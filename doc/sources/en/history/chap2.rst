Documentation: `doc fr <../../fr/history/chap2.html>`__



From 2000 to 2019
=================

Au début des années 2000, les projets **LOVE** du `CEA-EA/DAM <www.cea.fr>`__ et **ParaView** de
`Kitware <www.kitware.com>`__ sont lancés.
Tous deux se basent sur cette même boîte à outils, `VTK <vtk.org>`__.

C’est fort de l'expérience passée dans la réalisation d’outils intéractifs de visualisation
(tout particulièrement en 2D), que le logiciel **LOVE** atteint, plus rapidement que **ParaView**, la
maturité nécessaire pour répondre aux besoins métiers ciblés par les utilisateurs du `CEA-EA/DAM <www.cea.fr>`__.

Ses principales fonctionnalités sont les suivantes :

* une Interface Utilisateur Graphique (GUI) dédiée ;

* le scripting Python des commandes ;

* l'écriture d'un script Python en fonction des interactions utilisateurs avec la GUI dénommée *session* ;

* une calculatrice permettant de générer de nouvelles grandeurs ;

* des filtres spécifiques `VTK <vtk.org>`__, tout particulièrement des lecteurs de données propre au `CEA-EA/DAM <www.cea.fr>`__ ;

* etc.

A tel point, que certains de ces choix fait dans **LOVE** se retrouveront plus tard dans **ParaView**.

A contrario, peu à peu, **LOVE** se met à intégrer des fonctionnalités, d'abord internes, de **ParaView**
en commençant par le remplacement du lourd mécanisme du client/serveurs ce qui en améliorera la maintenance.

Les évolutions des besoins utilisateurs nécessitent que **LOVE** exposent de plus en plus de fonctionnalités
encombrants l'interface.
C'est tout particulièrement le cas avec cette nouvelle représentation autour des maillages à raffinement
adaptatif basés sur des arbres de décomposition (TB-AMR, Tree-based Adaptative Mesh Refinement) dont le
`CEA-EA <www.cea.fr>`__ est le principal maître d'oeuvre.

Au contraire, **ParaView** évolue afin d'offrir plusieurs axes de personnalisation de la GUI alors que
**LOVE** ne cesse de se complexifier sans pour autant proposer toutes les fonctionnalités de **ParaView**.

**ParaView** allait bientôt imposer le passage à Python 3 (2008-) alors que **LOVE** n'avait pas encore
commencé cette démarche.
