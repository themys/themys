Documentation: `doc fr <../../fr/filters/CEAAnnotateTime.html>`__

CEA Annotate Time filter
========================

Example:
++++++++

The main purpose of this filter is to give the user a very simple way to display, in the render view, the name of the source
(only if it is an `HerculeServicesReader`), the value of the currently loaded time and the timestep index.

.. figure:: ../../img/filters/ceaannotatetime_name_mus_index.png
   :align: center
   :width: 100%

   Example of ``CEAAnnotateTime`` filter usage where the name of the source, the time value and the timestep index are displayed.

Usage:
++++++

In the pipeline, select the source for which the time information should be displayed and click `<ctrl> + <space>`, then enter `CEAAnnotateTime` in the search bar.

By default, the filter will display:

- the registration name of the source, if possible;
- the word 'Temps' followed by the value of the scalar field variable `vtkFixedTimeValue`, if it is available. This variable holds the value of the current time.
  The value is expressed, by default, in microseconds.
- the word 'Indice' followed by the value of the scalar field variable `vtkFixedTimeStep`, if it is available. This variable holds the current timestep index.

If `vtkFixedTimeStep` or `vtkFixedTimeValue` are not available, it is possible to select anoter scalar field variable for representing time value and timestep index.

Properties:
+++++++++++

The available properties of this filter are:

- `Desired Time Unit`: a choice between:
    - `MicroSecond`;
    - `NanoSecond`;
    - `PicoSecond`.
  This property configures the unit used to display the time value. Default value: `MicroSecond`.
- `Time Variable`: a choice between all scalar field values.
  This property selects the variable that represent the time value. Default value: `vtkFixedTimeValue`.
- `Time Index Variable`: a choice between all scalar field values.
  This property selects the variable that represent the timestep index. Default value: `vtkFixedTimeStep`.
- `Use Registration Name`: a boolean that, if true, display the registration name of the source, if possible (i.e an `HerculeServicesReader` source).
  If false, the registration name is not displayed even if it is available. Checked by default.

.. figure:: ../../img/filters/ceaannotatetime_properties.png
   :align: center
   :width: 60%

   Properties of ``CEAAnnotateTime`` filter.
