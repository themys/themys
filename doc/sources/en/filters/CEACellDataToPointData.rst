Documentation: `doc fr <../../fr/filters/CEACellDataToPointData.html>`__

CEA CellDataToPointData Filter
==============================

Usage example
-------------

One of the purposes of this filter is to build an interface between 2 materials or meshes having
a cell field representing their respective volumic fraction.

To build this interface, it is necessary to apply the ``IsoVolume`` filter on a node field
representing the volumic fraction.

This node field is obtained by projection of the corresponding
cell field that is furnished by the code.

This projection is done thanks to the ``CEA CellDataToPointData`` filter.

For example, the following ``Pipeline`` is built:

- opening a database that holds a structured mesh with mixed cells

- loading the cell field representing the volumic fraction

- applying the ``CEA CellDataToPointData`` filter to project the ``volumic fraction``
  variable from cells to nodes with following options:

  - ``WeightCellOption`` = ``Quad``;
  - ``BoundaryConditionPoint`` = ``Y Axis``;
  - ``AxisAlignmentBoundaryConditionPoint`` = 0;
  - ``AbsoluteErrorEpsilonOnAxisAlignment`` keep the default value.

- applying the ``IsoVolume`` filter on this new ``volumic fraction`` node field

- to finish, applying the ``Transform`` filter with a 180° rotation around Y and Z axes

You will get the following result:

.. figure:: ../../img/filters/celldatatopointdata_quad_projection_and_boundary_with_symetry_intro.png
   :align: center
   :width: 100%

   Example of interface reconstruction using the ``CEACellDataToPointData`` filter to project the volume fraction
   from cell to nodes

On the left there is the corresponding ``Pipeline Browser``.

Just under, in the ``Properties`` panel, there are the ``IsoVolume`` filter parameters.
Logically, the maximum value of the ``Threshold`` parameter should be equal to 1. However
if is necessary to set a value slightly higher than 1, ideally 1.0001. This is due to the
fact that the projection process introduces some rounding errors.

On the render view, there are 2 representations:

- the left representation is the full result obtained through the pipeline.
  The color map represents the ``Milieu:Density`` variable.
  It is worthwile to note that:

  - external boundaries are not straight. This is due to the fact that the
    ``BoundaryConditionPoint`` handled the ``Y axis`` boundary but cannot handle
    more than one axis boundary.

  - holes appear in the centre of the mesh. This is because the whole procedure is
    valid to compute boundaries between two materials but not more. In the centre there may
    be more than 2 materials in each cell.

- the right representation is the same result as in the left but 2 of 4 materials are not displayed, there
  respective entries are unselected in the ``MultiBlock Inspector`` view. This way, we clearly see that the
  interface has been smoothed.

Description
-----------

The generic VTK filter ``CellDataToPointData`` project a field of values described
at the cell centers towards the points.
After application of this filter, a new field of values, named as the original one, appears at the points.
Without further consideration, this way of doing things is accurate and consistent with what is expected.
This filter can project several cell fields at the same time.
The calculation of the point value is just an average of the values in the cells that hold the point.

When dealing with more than one material inside a global structured mesh,
the vtk description of each material is unstructured even if the underlying global mesh is structured.
In fact the unstructured material description is just a part of the structured mesh.

.. figure:: ../../img/filters/celldatatopointdata_mixed_materials.png
   :align: center
   :width: 100%

   Two materials (red and blue) in a global structured mesh. Mixed zone is pink colored.

.. figure:: ../../img/filters/celldatatopointdata_mixed_materials_matA.png
   :align: center
   :width: 100%

   Blue material unstructured representation

.. figure:: ../../img/filters/celldatatopointdata_mixed_materials_matB.png
   :align: center
   :width: 100%

   Red material unstructured representation

In this case, the ``CEACellDataToPointData`` filter is able to project every cell variable of the
material mesh into corresponding points as if the material mesh was fully structured.
Thus it makes the hypothesis that in the cell where the material does not exist, its volumic fraction is null.

.. figure:: ../../img/filters/celldatatopointdata_mixed_materials_matA_proj.png
   :align: center
   :width: 100%

   ``CEACellDataToPointData`` filter will project any variable of the blue material as if it was part
   of a true structured mesh in which empty cells have a null volumic fraction

``WeightCellOption`` option
---------------------------

This option proposes three values: ``Standard``, ``Quad`` et ``Hexahedron``.

Purpose
~~~~~~~

This option is particularly used when projecting a volumic fraction field.

If the underlying mesh is a regular structured mesh
comprising only ``Quad`` or ``Hexahedron`` then the
value of this fraction projected at the nodes will have a consistent value
comprised between 0 and slightly above 1.
This slight overshoot is due to numerical rounding errors.

This new field can then be used to apply 2D iso-Contour or 3D iso-Volume filter
in order to build a unique interface between two materials.

Mecanism
~~~~~~~~

The ``WeightCellOption`` allow the modification of the weight of each cell
during the projection process.
The availabe values are:

- ``Standard``: produces same result as the ``CellDataToPointData`` filter;

- ``Quad``: use weight that mimics a 2D regular structures mesh made of ``Quad`` cells;

- ``Hexahedron``: use weight that mimics a 3D regular strcutured mesh made of ``Hexahedron`` cells.

Choosing ``Quad``, respectively ``Hexahedron``, will divide the contribution of each cell
by 4, respectively 8, during the projection computations.

Example
~~~~~~~

The following image illustrate the problem that arise when using the original
``CellDataToPointData`` filter, or the ``CEACellDataToPointData`` filter with ``WeightCellOption``
equal to ``Standard``, to project the volumic fraction before applying an ``IsoVolume`` filter to reconstruct
interface between materials.

The material in the middle is displayed in ``Wireframe`` mode, so that it is possible to
distinguish its interaction with surrounding materials.

.. figure:: ../../img/filters/celldatatopointdata_standard_projection.png
   :align: center
   :width: 100%

   Interface reconstruction using volumic fraction projection with classical
   projection settings

You can clearly see that the border of the middle material is inside the surrounding materials.

Instead, if the ``CEACellDataToPointData`` filter is used, with ``WeightCellOption`` equal to ``Quad``,
then the following figure is obtained:

.. figure:: ../../img/filters/celldatatopointdata_quad_projection.png
   :align: center
   :width: 100%

   Interface reconstruction using volumic fraction projection with enhanced
   projection settings

This option corrects the drawbacks of the standard filter.
Moreover, we can see that the interface is more smoothed.

The holes in the center of the image are due to the fact that this interface reconstruction
procedure is able to produce only one interface. Thus it cannot handle mixed cells with more
than 3 materials.

.. figure:: ../../img/filters/celldatatopointdata_quad_projection_abovetwo.png
   :align: center
   :width: 100%

   Procedure cannot restitute interface position in mixed cells with
   more than two materials


Boundary conditions options
---------------------------

The following options play a role on boundary conditions used in the projection algorithm:

- ``BoundaryConditionPoint``;
- ``AxisAlignmentBoundaryConditionPoint``;
- ``AbsoluteErrorEpsilonOnAxisAlignment``.

Purpose
~~~~~~~

The algorithm used, when previous option ``WeightCellOption`` has a value different from ``Standard``,
makes the hypothesis that the underlying regular structured mesh is infinite.

Thus it may lead to discontinuities in the vicinity of the boundaries of the mesh.

Mecanism
~~~~~~~~~

The idea behind the use of ``BoundaryConditionPoint`` option is to get back to standard projection process
for nodes that are on a boundary condition.

Four values are available for the ``BoundaryConditionPoint`` option:

- ``None``: do not consider any boundaries;

- ``X Axis``: do consider the X axis as a boundary;

- ``Y Axis``: do consider the Y axis as a boundary;

- ``Z Axis``:  do consider the Z axis as a boundary.

The ``AxisAlignmentBoundaryConditionPoint`` option awaits a floating point number
that will act as an offset from the selected axis to determine the boundary position.
The default value is 0.

The ``AbsoluteErrorEpsilonOnAxisAlignment`` option awaits a floating point number
that represent an absolute error on the boundary nodes position.

Example
~~~~~~~

The following image presents the results of the application of standard projection algorithm
(with ``CEACellDataToPointData`` and option ``WeightCellOption=Standard``)
followed by the application of ``IsoVolume`` filter.

.. figure:: ../../img/filters/celldatatopointdata_standard_projection_without_symetry.png
   :align: center
   :width: 100%

   Effect of ``Standard`` value of the ``WeigthCellOption`` option.

Next image is the same except that the ``WeightCellOption`` has now ``Quad`` value.

.. figure:: ../../img/filters/celldatatopointdata_quad_projection_without_symetry.png
   :align: center
   :width: 100%

   Effect of ``Quad`` value of the ``WeigthCellOption`` option.

Here again we can see that this last mode softens more the interface and both material limits match better.

However we do remark:

- a piece of material has disappeared in the ``Quad`` mode. It is due
  to the fact that the volumic fraction value is below 0.5;

- on the lower boundary the color map represent a much lower value in the ``Quad``
  mode than in the ``Standard`` mode. This is the consequence of the infinite underlying mesh hypothesis
  exposed previously

Those two drawbacks are even better illustrated in the following figure where a ``Reflect`` filter
is applied after the ``IsoVolume`` one.

.. figure:: ../../img/filters/celldatatopointdata_quad_projection_with_symetry.png
   :align: center
   :width: 100%

   Illustration of two drawbacks of the ``Quad`` value of the ``WeightCellOption`` option

Those two drawbacks may be corrected by setting the option ``BoundaryConditionPoint`` to ``Y Axis`` and
``AxisAlignmentBoundaryConditionPoint`` to 0.

.. figure:: ../../img/filters/celldatatopointdata_quad_projection_and_boundary_without_symetry.png
   :align: center
   :width: 100%

   ``BoundaryConditionPoint`` option fixes the drawbacks of ``Quad`` mode

This is even more visible when applying ``Reflect`` filter, as shown in the figure below.

.. figure:: ../../img/filters/celldatatopointdata_quad_projection_and_boundary_with_symetry.png
   :align: center
   :width: 100%

   ``BoundaryConditionPoint`` option fixes the drawbacks of ``Quad`` mode

With this option we obtained a continuous field even near the boundary condition.
