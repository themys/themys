configure_file(${CMAKE_CURRENT_LIST_DIR}/conf.py.in
               ${CMAKE_CURRENT_LIST_DIR}/conf.py @ONLY)

add_subdirectory(filters)
add_subdirectory(gettingstarted)
add_subdirectory(history)
add_subdirectory(procedures)
add_subdirectory(readers)
add_subdirectory(tutos)
add_subdirectory(writers)

set(DOC_FILES_EN
    ${DOC_FILES_EN} ${CMAKE_CURRENT_LIST_DIR}/index.rst
    ${CMAKE_CURRENT_LIST_DIR}/conf.py.in
    PARENT_SCOPE)
