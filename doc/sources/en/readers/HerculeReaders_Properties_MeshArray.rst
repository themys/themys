Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_MeshArray.html>`__


Mesh Array
^^^^^^^^^^

The **Mesh Array** property lists the meshes present in the database. The user may select the meshes
that he wants to visualize.

Thoses meshes may represent different aspects of the simulation. They can be true "finite elements" meshes,
cut planes, surfaces, curves, particles or laser beams.

Contrary to what used to be in LOVE, all the content of the database is exposed. There are no hidden contents.

Usually this selection should be done before the `materials <HerculeReaders_Properties_MaterialArray.html>`_ and the `cells and nodes fields <HerculeReaders_Properties_DataArray.html>`_. selections

Of course, those choices should be coherent. It's only after all these selections done that the user may confirm his choice and triggers the loading
by clicking on **Apply**.

At any time the user may change the selections made.

.. warning::
   It is strongly advised to only load the minimum, because it is what will be loaded when the timestep changes
   or during the application of a filter that works on all timesteps (**Plot Data Over Time** for example).
