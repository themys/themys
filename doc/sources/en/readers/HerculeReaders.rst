Documentation: `doc fr <../../fr/readers/HerculeReaders.html>`__


Hercule readers
===============

Two Hercule database readers are available: **Hercule Services Reader** and **Hercule HIc Reader**.

.. warning::
   The **Hercule Services Reader** has to be used to load any **non HTG/AMR-TB** databases.

   The **Hercule HIc Reader** has to be used to load any **HTG/AMR-TB** databases.

.. toctree::
   :maxdepth: 1

   HerculeReaders_Output_Description
   HerculeReaders_Differences_With_Love
   HerculeReaders_Properties
   HerculeReaders_Configuration
