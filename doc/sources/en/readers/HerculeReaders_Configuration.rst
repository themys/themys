Documentation: `doc fr <../../fr/readers/HerculeReaders_Configuration.html>`__


Hercule readers properties configuration
----------------------------------------

It is possible to pre-define objects lists to be default selected.

This configuration is written in the user file ``~/.config/CEA/themys-UserSettings.json``:

| {
|   "HerculeReader":
|   {
|     "DefaultSelectedMeshes" : ["meshNS", "meshNSYZ"],
|     "DefaultSelectedMaterials" : ["MatB", "global_meshNSYZ"],
|     "DefaultSelectedNodeFields" : ["Noeud:node_densite"],
|     "DefaultSelectedCellFields" : ["Milieu:mil_densite", "Maillage:cell_vecteurA1"]
|   }
| }

During the loading of a database, if no default value is specified in the configuration file, then the
reader will select:

- the first alphabetically sorted mesh;
- all of the materials execept those which names start with *global_*;
- **vtkPointId** for the node fields;
- **vtkCellId** et **vtkDomainId** for the cells fields.
