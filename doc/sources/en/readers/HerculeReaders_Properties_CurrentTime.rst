Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties_CurrentTime.html>`__


Current Time
^^^^^^^^^^^^

This property, available only with advanced display (using the gear button), gives the value of the currently loaded timestep.

This information is shared among:

- the **Information** tab under the **Data Statistics** section
- the global field values: **vtkFixedTimeStep** / **vtkFixedTimeValue**

When a timestep is chosen by the user, the timestep that will indeed be loaded is, by order of precedence:

* the exact timestep if it exists in the database,
* the closest lower timestep in the database,
* the closest upper timestep in the database.

Other Hercule readers properties may influence the currently loaded timestep:

* the **Fixed Time** property allow to pin a specific timestep of the database. When set, the timestep won't change
  anymore even if the user change the time in the **Themys GUI**. In this case, the value of the **Current Time** property may be
  different from the value of the time in the **Themys GUI**.
* the **Time Shift** property allow to define a shift between the timestep chosen in the **Themys GUI** and the one
  effectively loaded from the database.
