Documentation: `doc fr <../../fr/readers/HerculeReaders_Differences_With_Love.html>`__



The big differences with LOVE
-----------------------------

With former vizualisation tool, **LOVE**, the protocol used when changing the variable currently visualized,
was to unload from memory the current variable before loading the new one.

In **Themys** the logic is dissociated in two steps:

* first, the user, thanks to the reader **Properties**, load in memory the whole set of variable he wants to visualize;
* second, thanks to the **Themys** GUI, the user choses among the previously loaded variable the one he wants to visualize.

This way, once the loading is done, the change of variable is faster than what was experienced with **LOVE**.

During the loading phase, the user may select the **meshes**, **materials** and cell or node **variables**
that he needs for his analysis.
This choice can be changed at any time to add or remove an element. This choice should then be validated
by clicking on the **Apply** button.

.. warning::
   In order to limit the loading time and enhance the interactive experience, it is strongly advised
   to select only the needed **meshes**, **materials** and cell/node **variables**.
   Those selection choices will be kept during a change of timestep or if a filter needs to walk across the timesteps.
