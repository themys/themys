Documentation: `doc fr <../../fr/readers/HerculeReaders_Properties.html>`__

Hercule readers properties
--------------------------

The **Hercule Services Reader** uses the **API HS** of  Hercule, also known as **Hercule Services** API.

This Hercule API, dedicated to visualization and analysis, interprets the database content and
gives access to hidden informations.
In this mode, the different subdomains present in the database are gathered and distributed to the visualization servers.
Thus each server holds only one **piece** and the number of **pieces** is equal to the number of visulization servers.

**It is strongly advised to use this reader to load a non HTG/TB-AMR Hercule database**
**This reader must not be used to load HTG/TB-AMR Hercule databases**

The **Hercule HIc Reader** reader use the **API HIC** of Hercule, which is the one used by CFD codes to write their databases.

With this reader, the loading of a TB-AMR is done through a **vtkHyperTreeGrid**, which is a VTK specific representation
that handles this type of mesh. Different filters are associated to this representation. Some of them
apply local algorithms that need neighboring cells informations. This is, for example, the case an iso-contour or a gradient filter.

In this mode, for a visualization server:

   - in the case of a **vtkHyperTreeGrid** representation, the subdomains of the database, that are attributed
     to this server, are gathered into only one **piece**. Thus the number of **pieces** is equal to
     the number of visualization servers.

   - for the other representations, the number of **pieces** is equal to the number of subdomains present in the database.
     Those **pieces** are then distributed to the different visualization servers.

**It is strongly advised to use this reader to load only HTG/TB-AMR Hercule databases**


Common properties of **Hercule Services Reader** and **Hercule HIc Reader** readers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   HerculeReaders_Properties_Version
   HerculeReaders_Properties_CurrentTime
   HerculeReaders_Properties_FixedTime
   HerculeReaders_Properties_TimeShift
   HerculeReaders_Properties_MeshArray
   HerculeReaders_Properties_MaterialArray
   HerculeReaders_Properties_DataArray
   HerculeReaders_Properties_ExtremasSpectralForSummation
   HerculeReaders_Properties_MemoryEfficient
   HerculeReaders_Properties_Extruder1D

.. figure:: ../../img/readers/properties_common.png
   :width: 100%
   :align: center

   Common properties of the **Hercule Services Reader** and **Hercule Hic Reader** readers.


Specific properties of the **Hercule HIc Reader** reader
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   HerculeReaders_Properties_TB-AMR

.. figure:: ../../img/readers/properties_hic.png
   :width: 100%
   :align: center

   Specific property of the **Hercule HIc Reader**.
