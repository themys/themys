Documentation: `doc fr <../../fr/procedures/PlotOverLineCustom.html>`__


Custom Extraction of Points on Lines
====================================

Description
-----------


It is possible to extract information from a mesh at locations defined by a set of polylines
(equivalent to a broken line) that traverse the mesh. This is what the
``Plot Over Line Custom`` filter allows (the non-custom version only applies
to a straight line).

This filter produces curves with the linear position along the polylines from
their origin on the x-axis and the value for one of the data fields on the
y-axis. The linear position, named **arc_length**, corresponds to the distance
covered while traversing the polyline from the start.


Several options are available:

- ``Sample Uniformly`` : extracts points evenly distributed along each of the
  polylines. The ``Resolution`` property allows you to vary the number of points
  per polyline.
- ``Sample At Cell Boundaries`` : extracts points at each cell boundary. The
  result clearly expresses the geometry of the mesh.
- ``Sample At Segment Centers`` : extracts the centers of segments that
  traverse cells along each of the polylines.

The results are sorted according to the increasing linear position per polyline.


Creation of the PlotOverLineCustom Filter
-----------------------------------------

The ``Plot Over Line Custom`` filter takes as its first input the mesh on
which the filter will be applied.

A dialog window will then open, asking for the value of each of the two ports:

-  The first port, named ``Input``, is already filled with the active mesh.
-  The second port, named ``Source``, describes the multi-polylines to apply.

Convenient options are available to automatically create multi-polylines of types:

-  Peacock or hedgehog : ``SpheresLineSource`` ;
-  Beam : ``BoxLinesSource`` ;
-  Broom : ``CylinderLineSource``.

Of course, it is possible to create your own multi-polyline source.


Creation of Custom Multi-Polyline Sources
-----------------------------------------

The first step is to create a polyline using the ``PolyLines`` Source. You can
add points manually in the table or modify the polylines interactively.

The next step is to group and then merge these polylines. To do this, select
the polylines and apply the ``Group DataSets`` filter. Then, apply the
``Merge Blocks`` filter, which will define a new unstructured mesh where each
block is a polyline cell.

This result can serve as a source for the ``PlotOverLineCustom`` filter.
