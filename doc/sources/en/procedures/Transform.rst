Documentation: `doc fr <../../fr/procedures/Transform.html>`__



*Transform* Filter
=========

Description
-----------

The ``Transform`` filter allows for the application of certain types
of geometric operations:

- Translation
- Rotation
- Scaling

Only one operation of each type can be applied, and the order of application
is scaling first (if enabled), followed by rotation (if enabled), and finally
translation (if enabled).


Each application of the Transform filter creates a copy of the mesh with new
coordinates, which incurs a memory cost. Therefore, it is advisable not to
overuse this filter on large meshes.

Using the Filter
----------------

If you want to perform a translation (Translate) followed by scaling (Scale),
there are two options:

- The first, more intuitive option is to apply two ``Transform`` filters,
  each for one geometric transformation.
- The second, more efficient but more difficult option is to apply both
  operations in the same ``Transform`` filter.


Please note that the first option consumes twice as much memory as the second.

The second option is generally more challenging to use because you need to
consider the order of operations within the filter: since translation is
applied after scaling, the values associated with the translation must
be affected by this transformation.


Exemple
-------

The goal of this example is to modify the mesh provided by the file
``SainteHelens.dem`` so that the origin is at the origin of the coordinate
system and the width of the mesh along the X-axis is set to 1.



|image0|
  Left side, case number 1 with two ``Transform`` filters,
  Right side, case number 2 with only one ``Transform`` filter.

The values for the operations to be applied are determined by looking at the
``Information`` panel and are as follows:
::

   Translate -557945; -5.1079915e6; -682
   Scale  0.000102249; 0.000102249; 1


Case number 1 with two ``Transform`` filters:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first filter applied here relates to the translation and should be filled
as shown below.

|image1|

The second filter relates to the scaling and should be filled as shown below.

|image2|


Case number 2 with only one ``Transform`` filter:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As said before, the translation is applied after the scaling, and the values
associated to the translation must take into account this transformation.

Instead of entering a translation as previously:
::

   Translate -557945; -5.1079915e6; -682

you must apply the scaling:
::

   Translate -57.0493; -522.287; -682

This gives the parameters for the ``Transform`` filter shown below.


.. |image0| image:: ../../img/procedures/Transform.png
.. |image1| image:: ../../img/procedures/Transform_option1_a.png
.. |image2| image:: ../../img/procedures/Transform_option1_b.png
.. |image3| image:: ../../img/procedures/Transform_option2.png
