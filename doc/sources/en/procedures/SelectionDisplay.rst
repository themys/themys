Documentation: `doc fr <../../fr/procedures/SelectionDisplay.html>`__



Display information about a selection
=====================================

Description
-----------

Navigating a data set is easier after selecting elements for analysis.
One way to examine these elements is through a spreadsheet table.

Procedure using the GUI
-----------------------

1. Information display by hovering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This type of information display is useful to get a quick look at the
data set and does not actually requires the definition of a selection,
but is worth mentioning for its simplicity and convenience. Click on the
``Hover Cells/Points On`` button in the toolbar located above the
visualization window as shown below. Then simply move the cursor over
any cell or point to display a tooltip containing information about the
element.

|image0|

2. Information display using the find data panel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Once the selection is achieved, it is possible to display informations about
the selected cells (or points) with the ``Find Data`` panel.
Note that the selection may also be done through this panel.

|image6|

The panel shows all loaded variables for each cells (or points) selected.
It is possible to choose the displayed variables using the
**Column visibility** button (shown in red in the image below).

|image7|


3. Information display using a spreadsheet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The spreadsheet view offers an exhaustive way of displaying information.
To create this view, first activate the ``Advanced Mode`` by clicking on
the wheel button in the toolbar.

|image1|

Then split the current view by clicking on the corresponding button
shown below.

|image2|

A new window appears next to the current one, in which different view
options are available. Click on the ``SpreadSheet View``.

|image3|

This view consists in a table containing information such as IDs,
coordinates for points, cell types, data arrays, etc. Make sure to
select the desired data set in the ``Showing`` combo box. You can choose
whether to display information on cells or points with the ``Attribute``
combo box. Lastly, interacting with the rows by clicking on them
highlights the corresponding elements in the render view to easily
identify them.

|image4|

To reduce the amount of information to only a selection, click on the
``Show only selected elements`` button.

|image5|

Instead, if you wish to extract the selection to manipulate a subset of
the data set, see `this
section <./SelectionByGlobalID.html#extract-a-selection>`__.

Procedure using Python scripting
--------------------------------

1. Information display by hovering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This feature is not available in ``paraview.simple``.


2. Information display using the find data panel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This feature is not available in ``paraview.simple``.


3. Information display using a spreadsheet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'blow.vtk' according to the desired data set
   mySource = FindSource('blow.vtk')

   # Get layout
   layout = GetLayout()

   # Create a new 'SpreadSheet View'
   sheetView = CreateView('SpreadSheetView')

   # Show data set in spreadsheet
   sourceDisplay = Show(mySource, sheetView)

   # Add spreadsheet view to the layout
   AssignViewToLayout(sheetView, layout)

   # Show only selected elements
   sheetView.SelectionOnly = 1

   # Show cells rather than points
   sheetView.FieldAssociation = 'Cell Data'

.. |image0| image:: ../../img/procedures/08ElementHover.png
.. |image1| image:: ../../img/procedures/08AdvancedMode.png
.. |image2| image:: ../../img/procedures/08ViewSplit.png
.. |image3| image:: ../../img/procedures/08ViewSelection.png
.. |image4| image:: ../../img/procedures/08Spreadsheet.png
.. |image5| image:: ../../img/procedures/08SpreadsheetOnlySelected.png
.. |image6| image:: ../../img/procedures/find_data_click.png
.. |image7| image:: ../../img/procedures/find_data_panel.png
