Documentation: `doc fr <../../fr/procedures/axes.html>`__


Axes
====


ParaView offers different rendering modes for axes.

In the ``Properties panel``, simply type ``axis`` in the search field, ensuring
that the advanced mode is activated by clicking the gear icon to the right of
the search field.

Data Axes Grid
--------------

In the ``Display`` category, you can enable the ``Data Axes Grid``, which will
construct the axis representation on the bounding box of the active source in
the ``Pipeline Browser``. An ``Edit`` button is associated with it to change
various properties of this object.

Axes Grid
---------

In the ``View (Render View)`` category, you can enable the ``Axes Grid``,
which will construct the axis representation taking into account all instances
of objects visualized in the ``Pipeline Browser``. An instance is visualized
if the eye icon to its left is checked. An ``Edit`` button is associated with
it to change various properties of this object.

Polar Axes
----------

In the ``Annotations`` category, you can enable the ``Polar Axes`` with an
associated ``Edit`` button to access the properties of this object, which
will construct the polar representation on the X-Y axis.

It is possible to set the dimensions of the polar grid, the number of arcs
or radial axes displayed, and the delta between each.

One of the parameters allows you to define a ``Ratio`` in the
``Aspect Control`` category to achieve an elliptical aspect.

Moreover, the ``Screen Size`` option allows you to manage the overall
rendering of the texts.

|image0|

.. note::
  Lots of options are available and some of them could be unclear.
  This picture may help you to have better understanding of the meaning of these options.

Legend Grid
-----------

In the ``Orientation Axes`` category, you can enable the ``Legend Grid`` with
an associated ``Edit`` button to access the properties of this object. This
object is grayed out by default; to activate it, you must check the preceding
``Camera Parallel Projection`` box.

Once activated, a frame (resizable in the properties) appears in the render window. This frame is fixed, and only the graduations move during zoom or camera movement.

Polar Grid
----------

In the ``Orientation Axes`` category, you can enable the ``Polar Grid`` with
an associated ``Edit`` button to access the properties of this object. This
object is grayed out by default; to activate it, you must check the preceding
``Camera Parallel Projection`` box.

Once activated, a polar grid (resizable in the properties) appears in the
render window. This grid is fixed, and only the graduations move during zoom
or camera movement.


.. |image0| image:: ../../img/procedures/polar_axes.png
