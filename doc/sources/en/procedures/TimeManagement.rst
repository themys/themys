Documentation: `doc fr <../../fr/procedures/TimeManagement.html>`__


Time management
===============

Disclaimer
----------

This documentation is a copy/paste of the `blog post by  Nicolas Vuaille, Sonia Ayme and Francois Mazen <https://www.kitware.com/a-new-time-manager-is-landing-in-paraview/>`__


Introduction
------------
In ParaView, you can load a temporal dataset and navigate through its timesteps.

You can also create an animated scene where analysis and visualization
parameters can change at each timestep. The features are more than useful
to understand your data and communicate about your results!

And, as always in ParaView, we want you to have a fine control of what happens
through a dedicated interface.
The time controls used to be split into the ``Animation View`` and the
``Time Inspector``. As both of them had a similar interface and some
redundancy, we recently removed them in favor of the brand new
``Time Manager`` panel.


Inspect times
-------------

The tracks are organized into two sections. The first one, labeled with the
**Time** title, displays information about time-varying data present in the
pipeline. Each row matches one temporal pipeline source, and its associated
timesteps.

|image0|

.. raw:: html

    <video width="640" height="360" controls>
      <source src="NavigateTimesteps.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>

Use it to navigate in complex temporal pipelines!

.. raw:: html

    <video width="640" height="360" controls>
      <source src="ComplexTime.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>


Animate properties and camera
-----------------------------

In the second part come the animation tracks, labeled with the **Animations**
title. Create animation tracks to interpolate visualization pipeline properties
for each timestep. Increase the number of frames to get something smoother.

|image1|

.. raw:: html

    <video width="640" height="360" controls>
      <source src="PropertyAnim-1.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>

You can also configure a camera path around your data!

.. raw:: html

    <video width="640" height="360" controls>
      <source src="AnimationShort.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>


.. |image0| image:: ../../img/procedures/TimeManagerSequence.png
.. |image1| image:: ../../img/procedures/TimeManagerAnimations.png
