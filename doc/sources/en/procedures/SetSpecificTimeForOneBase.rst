Documentation: `doc fr <../../fr/procedures/SetSpecificTimeForOneBase.html>`__

Documentation: `doc fr <../../fr/procedures/FixATimeForOneBase.html>`__


Set a specific time for a base
========================

Description
-----------

Themys/ParaView allows you to open several database with different
simulation times.

By default, when a new database is opened, all its timesteps are merged into
the list of times offered in ParaView.

Selecting another simulation time prompts the readers to load the
simulation time closest to this value (from below).

Set a specific time for a base
-----------------------

In order to freeze the time of a database, it is possible to right-click on the database, in the ``Pipeline Browser``, and toggle the
``Ignore Time`` checkbox. Any new choice of a simulation time in the GUI will
have no effect on this database as long as this checkbox is checked.

Note that all time values for this database are no longer visible in the GUI.

Uncheck the checkbox to be able to change the base simulation time.

In the ``Properties`` panel for a database opened using the Hercule Readers,
an option named ``Fixed Time`` is available. By default, this option is set
to an empty slot. Choose the desired time and click on ``Apply`` to lock this
database to this time.
This has the same effect as clicking on ``Ignore Time``.
To restore the default behavior, i.e. give the time control back to the GUI, set the ``Fixed Time`` variable to an empty slot.
