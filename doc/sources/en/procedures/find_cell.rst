

Trouver une cellule
===================

Description
-----------

Pour trouver une cellule à partir de l'identifiant global fourni par le code, vous devez :

# avoir chargé le champ de valeurs **vtkCellId**, si ce n'est pas le cas :

    # en cliquant sur le lecteur de base Hercule dans l'explorateur du pipeline (**Pipeline Browser**) ;

    # en allant dans **Properties**, pour cliquer sur **vtkCellId** ;

    # puis en cliquant toujours  dans **Properties** sur le bouton **Apply** ;

# ouvrir la vue **FindData**, dans le menu **View** ; généralement cette vue s'ouvre à droite à côté de la fenêtre **Color Map Editor**, l'onglet se trouve en bas ;

# sélectionner le bon **Data Produceur** ; si vous avez plusieurs bases dans le pipeline, n'hésitez pas à renommer les objets en double cliquant dessus ;

# sélectionner dans **Element Type** la valeur **Cell** ;

# définisser la régle : **vtkCellId** **is** **#votre identifiant global#** ;

# puis cliquer sur le bouton **FindData** un peu plus bas ;

# vous voyez le résultat de la sélection dans la fenêtre de rendu mais aussi dans l'onglet **FindData** section **SelectedData** en changeant l'attribut, vous verrez des informations appliquées sur des éléments de géométries différents.
