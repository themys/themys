Documentation: `doc fr <../../fr/procedures/array_renaming.html>`__

Automatic Renaming for Field Array
==================================

Renaming Settings
^^^^^^^^^^^^^^^^^

In the ``Settings`` menu, under the ``Themys`` tab, it is possible to unify
the naming of elements coming from different databases.

|image1|

Here is an example of the section ``Matching table for array renaming`` which
allows you to permanently associate one or more names present in different
databases (on the right, such as *vtkInterfaceFraction* and *Milieu*)
with a unified name for visualization (on the left, here *frac*).

The interface enables adding, deleting, or modifying an association.

Creating a new association requires:

- writing a new name on the left;
- writing the list of old names on the right, which will be replaced by the new name in the interface.
  This list of old names has to be semicolon separated.


.. note::
It is up to the user to assign a name different from those already existing in the databases.

.. |image1| image:: ../../img/procedures/array_renaming.png
