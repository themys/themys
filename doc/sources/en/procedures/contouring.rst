Documentation: `doc fr <../../fr/procedures/contouring.html>`__



Contour data to obtain isosurfaces and isolines with common or different opacities
==================================================================================

Description
-----------

The contouring operation extracts surfaces or lines that have the same
scalar value in a given data set. A well known example is that of
isolines used in cartography.

Procedure to contour data using the GUI
---------------------------------------

Start by selecting the data set to contour from the ``Pipeline Browser``
panel by clicking on it. Then click on the ``Contour`` filter in the
filter toolbar as shown below.

|image0|

Select the scalar array by which to contour using the combo box
``Contour By`` in the ``Properties`` panel. By default, the contouring
value is set to the average value of the scalar data range, visible in a
list in the ``Isosurfaces`` section.

|image1|

That list can contain as many contouring values as desired. Various
options are available to add or remove values, detailed below.

|image2|

1. **``+`` button** : Adds a new value to the list below the currently
   selected value.

2. **``-`` button** : Deletes the selected value from the list.

3. **Automatic values generator button** : Opens a window offering
   options to generate contouring values distributed in different
   manners (linearly, logarithmically, etc.) in a given range.

   |image3|

4. **Delete button** : Deletes all values from the list.

5. **Scaling button** : Divides or multiplies all values by 2.

6. **Reset button** : Resets the list to its original state.

To modify a value, simply click on it and enter a new value.

Procedure to modify opacity using the GUI
-----------------------------------------

1. Common opacity
~~~~~~~~~~~~~~~~~

To apply the same opacity level to all isosurfaces, adjust the opacity
value (``Opacity``) in the ``Display`` section as shown below.

|image4|

2. Different opacities
~~~~~~~~~~~~~~~~~~~~~~

To obtain different opacity levels for each isosurface instead, first
open the ``Color Map Editor`` from the toolbar as seen below.

|image5|

Tick the ``Enable Opacity Mapping For Surfaces`` option to modify the
opacity mapping. The black curve superimposed to the color map indicates
the opacity level associated with the corresponding color (scalar
value). The image below illustrates the default opacity curve, where the
maximum value in red is completely opaque, whereas the minimum value in
blue is completely transparent.

|image6|

Click inside of the color and opacity graph to add new opacity values to
the curve. Existing points can then be dragged to adjust their values.

|image7|

Procedure using Python scripting
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set to contour
   mySource = FindSource('Wavelet1')

   # Create a new 'Contour' filter
   contour = Contour(Input=mySource)

   # If needed, change scalar data array
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'RTData' with a valid scalar array
   contour.ContourBy = ['POINTS', 'RTData']

   # Define as many isocontour values as needed
   contour.Isosurfaces = [90.0, 150.0]

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   contourDisplay = Show(contour, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   contourDisplay.SetScalarBarVisibility(renderView, True)

.. _common-opacity-1:

1. Common opacity
~~~~~~~~~~~~~~~~~

.. code:: py

   # Set common opacity
   contourDisplay.Opacity = 0.5

.. _different-opacities-1:

2. Different opacities
~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Get color map for 'RTData'
   colorMap = GetColorTransferFunction('RTData')

   # Enable opacity mapping
   colorMap.EnableOpacityMapping = 1

   # Get opacity map for 'RTData'
   opacityMap = GetOpacityTransferFunction('RTData')

   # Define points on the opacity map curve
   # Each point is defined as [scalar data value, opacity level, midpoint, sharpness]
   # For a piecewise linear function, midpoint = 0.5 and sharpness = 0.0
   opacityMap.Points = [50, 1.0, 0.5, 0.0, 100, 0.7, 0.5, 0.0, 200, 0.0, 0.5, 0.0]

.. |image0| image:: ../../img/procedures/04ContourFilterSelection.png
.. |image1| image:: ../../img/procedures/04ContourFilterAppearance.png
.. |image2| image:: ../../img/procedures/04ContourFilterOptions.png
.. |image3| image:: ../../img/procedures/04ContourFilterValueGenerator.png
.. |image4| image:: ../../img/procedures/04ContourFilterOpacityCommon.png
.. |image5| image:: ../../img/procedures/04EditColorMap.png
.. |image6| image:: ../../img/procedures/04ContourFilterOpacityLinear.png
.. |image7| image:: ../../img/procedures/04ContourFilterOpacityCustom.png
