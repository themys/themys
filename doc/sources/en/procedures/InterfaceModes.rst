Documentation: `doc fr <../../fr/procedures/InterfaceModes.html>`__



Add an interface mode
=====================

Context
-------

Themys has a setting to choose between different interface modes. Interface modes change the layout of the app, showing different toolbars, widgets and menus depending on the workflow.
Interface mode can be changed using the combobox on the bottom left of the toolbars.

Currently, Themys has three different interface modes :

* The default "Themys" one, with the Themys toolbar and simplified menus.
* "TB-AMR", used for Hyper Tree Grid related workflows. It exposes an "HTG" toolbar containing all useful filters for HTG usage.
* The "Advanced" one, corresponding to the Paraview base interface.

Interface mode JSON configuration file
--------------------------------------

Interface modes are configured using a JSON description file `interface_modes.json` which can be found in `client/resources` in the Themys repository.
This file is copied to the build directory by CMake at configure time, which means the configure step has to be run again for the changes to take effect in the app.
It is parsed on application startup.

Configuration file specification
--------------------------------

The JSON file has two top-level elements : `custom_menus` and `interfaces`.
`custom_menus` is an object that describes the elements of custom menus used in the Themys interface mode (and possibly others).
It defines 4 lists of string elements : `file`, `edit`, `tools`, `help`. Each list describes the actions that should be shown in each menu of the same name when custom menus are used. All unlisted actions will not appear in those menus.

`interfaces` is a list of objects, each describing a different interface mode. Each interface mode can have the following properties:

* `name` (string): The interface mode display name, used as the label of the comobobox.
* `hidden_widgets` (list of string): The names of the dock widgets to hide.
* `partial_toolbars` (list of objects): Toolbars to show partially. Each object on this list must define the following properties:

 * `name` (string) : the name of the toolbar to show partially.
 * `show` (list of string): The name of the actions to show on the toolbar. Actions not mentionned in the list will be hidden.

* `use_custom_menus` (bool): when true, use the simplified menus described in `custom_menus`. Otherwise, use the advanced Paraview menus.
* `sources` (list of string): Name of the categories to show in the sources menu.
* `partial_sources` (list of objects): Categories to show partially in the Sources menu. Follows the same semantics as `partial_toolbars`. Partial source menus do not need to be present in the `sources` list.
* `filters` (list of string): Name of the categories to show in the filters menu.
* `partial_filters` (list of objects): Categories to show partially in the Filters menu. Follows the same semantics as `partial_toolbars`. Partial filter menus do not need to be present in the `filters` list`.

All properties are optional, except for `name` and `use_custom_menus`. Their order in the mode description do not matter.

Settings update
----------------

Change in the chosen interface mode is persistent between sessions. Interface mode can also be changed in the settings of the application. When adding a new interface mode in the json configuration file,
one must also update the `ThemysSettings.xml` file, adding an entry to the `EnumerationDomain` of the `InterfaceMode` property. Be careful to follow the same order in the `EnumerationDomain` than in the `interfaces` list of the JSON file.
