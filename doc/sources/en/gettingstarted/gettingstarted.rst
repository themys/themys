Documentation: `doc fr <../../fr/gettingstarted/gettingstarted.html>`__

Getting started
***************

.. toctree::
   :maxdepth: 1

   chap1
   chap2
   chap3
   chap4
