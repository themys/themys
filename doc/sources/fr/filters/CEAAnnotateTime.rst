Documentation: `doc en <../../en/filters/CEAAnnotateTime.html>`__

CEA Annotate Time filter
========================

Exemple:
++++++++

Le principal intérêt de ce filtre est de donner à l'utilisateur un moyen très simple d'afficher, dans la fenêtre de rendu, le nom de la source
(seulement si il s'agit d'un `HerculeServicesReader`), la valeur du temps courant et le numéro du pas de temps.

.. figure:: ../../img/filters/ceaannotatetime_name_mus_index.png
   :align: center
   :width: 100%

   Exemple de l'application du filtre ``CEAAnnotateTime`` où le nom de la source, la valeur du temps courant et le numéro du pas de temps sont affichés.

Utilisation:
++++++++++++

Dans le pipeline, sélectionner la source pour laquelle les informations temporelles doivent être affichées et cliquer sur `<ctrl> + <space>`, puis entrer `CEAAnnotateTime` dans la barre de recherche.

Par défaut, le filter affiche:

- le `Registration Name` de la source, si possible ;
- le mot 'Temps' suivi de la valeur de la variable scalaire globale `vtkFixedTimeValue`, si elle est disponible. Cette variable contient la valeur du temps courant.
  La valeur est exprimée, par défaut, en microsecondes.
- le mot 'Indice' suivi de la valeur de la variable scalaire globale `vtkFixedTimeStep`, si elle est disponible. Cette variable contient le numéro du pas de temps courant.

Si `vtkFixedTimeStep` ou `vtkFixedTimeValue` ne sont pas disponibles, il est possible de sélectionner une autre variable scalaire globale pour représenter la valeur du temps courant et le numéro du pas de temps.

Propriétés:
+++++++++++

Les propriétés disponibles pour ce filtre sont :

- `Desired Time Unit` : un choix entre :
    - `MicroSecond`;
    - `NanoSecond`;
    - `PicoSecond`.
  Cette propriété configure l'unité utilisée pour afficher la valeur du temps. Valeur par défaut : `MicroSecond`.
- `Time Variable` : un choix entre toutes les variables scalaires globale.
  Cette propriété sélectionne la variable qui représente la valeur du temps. Valeur par défaut : `vtkFixedTimeValue`.
- `Time Index Variable` : un choix entre toutes les variables scalaires globale.
  Cette propriété sélectionne la variable qui représente le numéro du pas de temps. Valeur par défaut : `vtkFixedTimeStep`.
- `Use Registration Name`: un booléen qui, si il est vrai, affiche le `Registration Name` de la source, si possible (i.e. avec une source de type `HerculeServicesReader`).
  Si il est faux, le `Registration Name` n'est pas affiché même si il est disponible. Coché par défaut.

.. figure:: ../../img/filters/ceaannotatetime_properties.png
   :align: center
   :width: 60%

   Propriétés du filtre ``CEAAnnotateTime``.
