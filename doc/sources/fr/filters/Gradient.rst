Filtre Gradient
===============

Description
-----------

Le filtre ``Gradient`` permet de calculer le gradient d’un champ de
données sur des cellules ou des points. Le résultat est une grandeur
vectorielle qui indique de quelle façon la grandeur physique choisie
varie dans l’espace. Ce filtre est applicable à tout type de
``vtkDataSet``.

Pour des maillages non structurés, le gradient pour des données
cellulaires correspond aux dérivées sur chaque cellule. Pour des données
sur les points, le gradient à un point donné correspond à la moyenne des
dérivées calculées sur les cellules auxquelles appartient ce point.

Pour des maillages structurés, la méthode de différence centrée est
utilisée pour calculer le gradient, sauf sur les bords où la méthode de
différence avant ou arrière est utilisée.

La méthode de différence centrée correspond à

::

   [f(x+h) - f(x-h)]/2h,

tandis que la méthode de différence avant et arrière correspond à

::

   [f(x+h) - f(x)]/h

et

::

   [f(x) - f(x-h)]/h,

respectivement, où ``h`` désigne l’espacement entre deux cellules.

Option
------

Méthodes de traitement des bords
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour les maillages structurés réguliers (``vtkImageData``), il est
également possible d’utiliser une implémentation optimisée de la méthode
de différence centrée. Elle propose de dupliquer la valeur aux bords
pour mener le calcul sur toutes les cellules, ce qui entraîne un effet
de lissage sur les bords. Cette option est accessible en spécifiant la
méthode de traitement des bords ‘lissée’ (``Smoothed``) au lieu de ‘non
lissée’ (``Non-Smoothed``) comme illustré ci-dessous.

|image0|

Choix de la méthode ‘lissée’ avec différence centrée sur les bords.

|image1|

Choix de la méthode ‘non lissée’ avec différence avant/arrière sur les
bords.

Divergence, Vorticity, QCriterion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce filtre permet également de calculer la divergence, la turbulence
(vecteur tourbillon ou ``Vorticity``), ainsi que le critère Q
(``QCriterion``), en cochant les options correspondantes. Pour cela, des
données vectorielles (3 composantes) sont requises en entrée. Par
défaut, seul le gradient est calculé.

Boundary Method
~~~~~~~~~~~~~~~

L’option ``Boundary Method`` est disponible pour les maillages
structurés réguliers.

Indique quelle méthode utiliser sur les bords (lissée ou non). Voir la
description ci-dessus.

Dimensionality
~~~~~~~~~~~~~~

L’option ``Dimensionality`` est disponible pour les maillages structurés
réguliers lorsque la méthode ``Smoothed`` est sélectionnée.

Indique si le gradient doit être calculé en deux ou trois dimensions. En
deux dimensions, seules les dimensions X et Y sont prises en compte.

Faster Approximation
~~~~~~~~~~~~~~~~~~~~

L’option ``Faster Approximation`` active l’application du filtre ayant
un algorithme plus rapide (moins de calculs), mais moins précis.
L’erreur contient un lissage des données en sortie et potentiellement
des erreurs sur les bords. N’a aucun effet pour des données cellulaires
ou des maillages structurés.

Contributing Cell Option
~~~~~~~~~~~~~~~~~~~~~~~~

L’option ``Contributing Cell Option``, pour des données sur des points,
indique quelle dimension de cellule (i.e. nombre de sommets) contribue
au calcul du gradient. Par exemple, en sélectionnant ``All``, toutes les
cellules sont prises en compte. Lorsque ``Patch`` est sélectionné, la
plus grande dimension de cellule parmi les voisins d’un point donné est
utilisée. Enfin, ``Dataset Max`` utilise la plus grande dimension de
cellule de tout le jeu de données. N’a aucun effet pour des données
cellulaires.

Replacement Value Option
~~~~~~~~~~~~~~~~~~~~~~~~

L’option ``Replacement Value Option``, comme son nom, indique quelle
valeur par défaut utiliser lorsque le gradient à un point ou cellule
donné ne peut pas être calculé.

.. |image0| image:: ../../img/filters/WaveletGradientSmoothed.png
.. |image1| image:: ../../img/filters/WaveletGradientNonSmoothed.png
