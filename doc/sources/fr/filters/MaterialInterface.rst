Filtre Material Interface
=========================

Introduction / glossaire
------------------------

Usage
~~~~~

Selectionner la source sur laquelle appliquer le filtre:

|image0|

Selectionner le filtre en tapant, par exemple, simultanément sur les
touches Ctrl et Space puis en entrant au clavier “interface”. Dans la
fenêtre qui s’ouvre, sélectionner le filtre “Material Interface (LOVE)”

|image1|

Normalement, les champs “Mode”, “Order Array”, “Normal Array” et
“Distance Array” sont pré-remplis avec les variables adéquates. Si
besoin, corriger les valeurs par défaut en sélectionnant les champs
issus du code de calcul.

|image2|

Cliquer sur “Apply” et les interfaces doivent apparaître:

|image3|

Si le mode de représentation est “Surface With Edges”, l’affichage
semble “pollué” par des arètes en surnombre:

|image4|

C’est un phénomène normal. Lorsqu’une maille est coupée par une
interface, il se peut que l’une des parties soit délimitée par cinq
points. Au lieu de créer une maille à cinq sommets, VTK préfère créer
trois mailles triangulaires. Cela explique l’aspect “en pointe” de
l’interface.

Notion de matériau, cellule pure, cellule mixte, zone
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nos simulations comporte ``un maillage géométrique global`` qui sert de
support pour définir différents sous-maillages ``matériau``. Cette
définition se fait à travers une *sélection des cellules* du maillage
global.

Ainsi, une cellule de *ce maillage global* peut alors être : - dite
``cellule pure``, si elle est décrite dans un seul des sous-maillages
``matériau`` ; ou - dite ``cellule mixte``, si elle est décrite dans au
moins deux sous-maillages ``matériau``.

Du point de vue du maillage *global*, une cellule comporte : - une seule
``zone`` dans le cas de la ``cellule pure`` ; ou - une zone par matériau
dans le cas de la ``cellule mixte``, chaque *zone* étant séparée par une
``interface`` et l’aire/le volume cumulé des zones couvre exactement la
cellule.

Suivant cette convention, les *matériaux* ne se mélangent pas.

La notion de ``constituant`` définit les éléments consituants un
``matériau``, ces derniers pouvant être à leur tour constitués de
``fluide``. Un modèle de mélange sera bien souvent précisé lors de la
définition des *constituants* d’un *matériau* (resp. des *fuildes* d’un
*constituant*). Ces notions n’interviennent pas au niveau de la
définition des maillages mais plutôt au niveau d’un préfixe dans le
nommage de champ de valeurs au niveau des maillages, *global* ou
*matériau*.

Pour ne rien arranger, la notion de ``milieu`` est parfois employée en
place de ``matériau``.

Notion d’interface
~~~~~~~~~~~~~~~~~~

Dans nos simulations, une ``interface`` entre deux matériaux est décrite
par un plan. Cette description peut être fournie à ce filtre : - par un
lecteur à travers ce qu’a stocker le code de simulation, ou - par un
autre procédé dans le pipeline de dépouillement, par exemple, en
exploitant le champ de valeurs de *fraction de présence volumique* de
chacun des *matériaux* dans les cellules du mailage *global* (méthode de
Youngs).

Il faut être conscient que cette information sub-cellule permet
d’obtenir une représentaion plus fine mais pas nécessairement plus
exacte.

La description d’une ``interface plane`` comporte : - une ``normale`` du
plan représentant un flux sortant de l’intérieur de la *zone* **(nx, ny,
nz)** ; et - une ``distance`` du plan à l’origine **d**.

Ces deux informations définissent alors l’équation du plan suivant la
formule classique : **nx \* x + ny \* y + nz \* z + d = 0**.

|image5| Fig 1. Définition d’une interface plane, ici dans une cellule
mixte binaire.

Notion d’attributs
~~~~~~~~~~~~~~~~~~

Des champs de valeurs sont associés à ces différents maillages, *global*
ou *matériau*.

Le nommage de ces champs peut être pré-fixé par *matériau*/*milieu*,
*constituant* et *fluide*. On les affuble généralement du terme *champ
de valeurs partiel*.

Les champs de valeurs définis au niveau des maillages *matériaux* sont
des champs de valeurs partiels à partir du moment qu’il y a plus d’un
matériau et que la simulation décrit des *cellules mixtes*.

Description et pré-requis
-------------------------

Le filtre ``MaterialInterface`` ne permet pas de construire ces
*interfaces plans* mais d’en exploiter la définition faite au niveau des
maillages *matériaux* afin d’en modifier leur représentation qui
jusqu’alors n’est qu’une sélection des cellules du maillage *global*.

A l’issu de ce filtre, chacune des *zones* décrivant un *matériau* est
alors révélée.

   **ATTENTION** Le fait de ne pas appliquer ce filtre dans ce type de
   simulation produit un résultat visuel fâcheux qui peut être
   interpréter comme faux. En effet, seule une des représentations de
   cette cellule mixte issue d’un des maillages *matériau* sera rendu
   visible. Par ailleurs, ce choix aléatoire généralement fait par la
   carte graphique est fait pour chaque cellule. Pour obtenir une bonne
   délimitation d’un maillage *matériau* dans ce contexte, il faut
   l’afficher de façon isolé des autres maillages. Naturellement, le
   *picking* (l’action d’obtenir l’information d’une cellule) est
   d’autant limité si elle n’est pas visible.

Ce filtre : - ne génère pas les interfaces, - ne gère pas d’autres types
d’interface que les interfaces planes, - au mieux, ce filtre prend en
compte au niveau d’une *zone* d’une *cellule mixte* associè à un
*matériau* qu’au plus deux interfaces planes qui ne peuvent pas être
sécantes dans cette cellule.

Le fait qu’un maillage *matériau* comporte au moins une cellule mixte
nécessite la définition de nouveaux champs de valeurs définis au niveau
de toutes les cellules. Ce filtre exploitant alors ces champs de
valeurs.

Ce filtre ne fonctionne que sur les maillages *matériaux* définis en non
structuré (2D ou 3D).

C’est le cas de nos maillages matériaux issus de simulation de maillage
structuré global, la sélection produisant un maillage non structuré.

|image6| Fig 2. Exemple d’une cellule mixte comportant 3 zones, une par
matériau ici dénommé A, B et C. On constate qu’il y a deux interfaces
qui sont à décrire une rouge et une bleue. Chaque interface sera à
décrire du point de vue de chacun des matériaux qu’elle sépare. Si les
normales de ces deux interfaces sont les mêmes on parle d’\ *interfaces
en pelure d’oignon*.

Utilisation
-----------

L’utilisation de ce filtre a pour conséquence de produire pour chacun
des maillages *matériaux* un nouveau maillage auxquels est associé de
nouveaux champs de valeurs. Il est donc important de limiter les champs
de valeurs aux données utiles.

L’aspect masquage
-----------------

Ce filtre comporte une première propriété *popup* ``Mask`` qui permet de
révéler ou non, au niveau de la GUI Themys/ParaView, deux autres
propriétés concernant la définition d’un masque : - la propriété
``Mask Array Name``. et - la propriété
``Negative Value Order Filter On``.

La propriété ``Mask Array Name`` permet de définir un masque à partir
d’un champ de valeur scalaire défini sur chacune des cellules. Par
défaut, la valeur 0 active le masquage de la cellule.

La propriété ``Negative Value Order Filter On`` permet de changer cette
valeur par défaut en considérant comme masquée une cellule qui aurait
une valeur strictement négative.

Ce champ de valeurs étant défini par cellule pour chacun des maillages
*matériau*, il est aisé de masquer tout ou partie du maillage.

Le mode de découpage
--------------------

Si la propriété ``Set Fill Material On`` est activée, le découpage pour
chaque *cellule mixte* d’un maillage *matériau* se fera en mode ``Clip``
ou plein (fill), c’est-à-dire que cette *cellule mixte* sera découpée
afin de ne conserver que l’aire décrite par la *zone* associée à ce
*matériau*.

Par défaut, le découpage de cette cellule se fera en mode ``Cut`` ou
contour, c’est-à-dire que cette *cellule mixte* d’un maillage *matériau*
sera découpée afin de ne conserver que la représentaion des interfaces
de la *zone* associée à ce *matériau*.

Différents modes de description des interfaces
----------------------------------------------

Ce filtre comporte une seconde propriété *popup* ``Mode`` qui propose
trois modes de description d’interfaces : - le mode
``With an outgoing interface``, - le mode
``With two outgoing interface``, et - le mode
``Old-school CEA interfaces``.

Le mode ``With an outgoing interface``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le mode ``With an outgoing interface`` permet de définir au plus une
interface par matériau. Cela couvre le cas des *cellules pures* et
*cellules mixtes binaires*.

Cette description nécessite la définition d’un champ de valeurs
vectoriel ``Normal Array Name`` orienté vers l’extérieur de la zone du
matériau et d’un champ de valeur scalaire ``Distance Array Name``, la
distance du point à l’origine (0.,0.,0.) du plan. Ces champs de valeurs
sont définis au niveau des cellules et pour chacun des maillages
matériaux.

Dans le cas d’une *cellule pure*, les valeurs de ces champs sont à 0.

Contrairement au mode ``Old-school CEA interfaces`` qui impose le
chargement de plusieurs maillages *matériaux* et des champs de valeurs
associés, ce mode permet le chargement et l’application de ce filtre que
sur un unique matériau.

Le mode ``With two outgoing interface``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le mode ``With two outgoing interface`` permet de définir au plus deux
interfaces par matériau. Cela couvre le cas des *cellules pures* mais
aussi tous les cas des *cellules mixtes* où un matériau comprend au plux
deux interfaces. Pour une cellule et un matériau, si les normales des
interfaces sont opposées nous sommes dans le cas d’une description de
type *pelures d’oignon*.

Cette description nécessite la double définition d’une interface.

Une première définition d’un champ de valeurs vectoriel
``Normal Array Name`` orienté vers l’extérieur de la zone du matériau et
d’un champ de valeur scalaire ``Distance Array Name``, la distance du
point à l’origine (0.,0.,0.) du plan. Ces champs de valeurs sont définis
au niveau des cellules et pour chacun des maillages matériaux.

Puis d’une seconde définition d’un champ de valeurs vectoriel
``Normal Array Name 2`` et d’un champ de valeur scalaire
``Distance Array Name 2``.

|image7| Fig 3. A gauche, la cellule mixte avec ses 3 matériaux et 3
interfaces. A droite, la décomposition pour chaque zone, chaque matériau
et la description nécessaire des interfaces.

Dans le cas d’une *cellule pure*, les valeurs de ces quatre champs sont
à 0.

Dans le cas d’une *cellule mixte binaire*, les valeurs des champs de la
seconde interface sont soit nulles soit identiques à la première.

Contrairement au mode ``Old-school CEA interfaces`` qui impose le
chargement de plusieurs maillages *matériaux* et des champs de valeurs
associés, ce mode permet le chargement et l’application de ce filtre que
sur un unique matériau.

Le mode ``Old-school CEA interfaces``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le mode ``Old-school CEA interfaces`` permet de définir des *cellules
pures* et *cellules mixtes avec interfaces en pelures d’oignon*.

La particularité de ce mode de description compacte se base sur une
notion d’ordre des *zones* dans une *cellule mixte*.

L’interface de la première *zone* décrit une *normale* sortante de cette
*zone*. C’est cette valeur de normale qui est décrite et utilisée pour
toutes les *zones* de la *cellule mixte*. On comprend alors la
dénomination *cellules mixtes avec interfaces en pelures d’oignon*, au
contraire du mode ``With two outgoing interfaces`` qui permet aussi de
décrire deux interfaces avec des normales distinctes mais non sécantes
dans la cellule (dont ces interfaces en pelures d’oignon).

La valeur de la *distance* au point d’origine (0.,0.,0.) de l’interface
est donnée, pour toutes les zones sauf la dernière, par rapport au plan
d’interface correspondant à l’interface dont la valeur de la normale est
sortante. Pour la dernière zone, cette valeur de distance correspond au
seul plan d’interface dont la définition de la normale est opposée à
celle de l’interface réellement décrite.

La propriété ``Order Array Name`` permet de définir un champ de valeurs
entières pour décrire cette notion d’ordonnancement des matériaux dans
chacune des cellules.

Ensuite, un champ de valeurs vectoriel ``Normal Array Name`` décrit
cettenormale de l’interface et un champ de valeur scalaire
``Distance Array Name``, la distance du point à l’origine (0.,0.,0.) du
plan. Ces champs de valeurs sont définis au niveau des cellules et pour
chacun des maillages matériaux.

A faire remarquer qu’au lieu d’utiliser 4 valeurs, en plus de la valeur
d’ordre par cellule, on pourrait se limiter à 3 valeurs en divisant la
norme par la distance (ce qui revient à fixer la valeur de d dans
l’équation de droite à 1) avec une sémantique encore moins naturelle.
Pour faire, cela il faut juste s’assurer de ne pas faire passer le plan
d’interface par ce point d’origine.

Cette sémantique n’est pas très naturelle car la description des
interfaces d’une zone dépend de la zone précédente ! Ce qui nous amène à
un inconvénient important de ce mode, c’est de ne pas permettre le
chargement et l’affichage des interfaces que d’un ou plusieurs matériaux
chargés.

Coût mémoire de cette description
---------------------------------

Avant 2022
~~~~~~~~~~

Jusqu’en 2021, cette description était relativement compacte puisqu’elle
nécessitait la définition d’une normale, d’une distance et d’un petit
entier pour l’ordonnancement des zones dans la cellule mixte afin de
construire toutes les interfaces.

Cette description se limitait à une représentation de type *pelures
d’oignon* pour plus de deux matériaux dans une cellule mixte.

L’utilisation de cette description nécessitait pour construire les
interfaces d’un matériau d’en avoir chargé les matériaux voisins (ceux
qui précédent dans l’ordre des zones au moins dans une cellule mixte).

En 2022
~~~~~~~

Une nouvelle description a été mise en place, plus lisible et
n’obligeant plus à charger les matériaux voisins (voir point précédent).

Elle ne se limite plus au mode *pelure d’oignon* puisqu’elle permet que
les normales des interfaces d’un matériau ne soient plus opposées, par
contre, cette nouvelle représentation impose que les interfaces ne se
croisent pas dans une cellule mixte.

Le sur-coût mémoire pour cette nouvelle représentation est quasiment
double (x2).

A partir de 2023
~~~~~~~~~~~~~~~~

Mais tout ceci c’est sans compter sur des développements en 2022 qui
vont permettre de définir des champs de valeurs basés sur des tableaux
virtuels avec indexation interne. Une expérimentation non complétement
intégré dans VTK de tels tableaux avait déjà été réalisée avec succès en
interne au CEA en 2017.

Au lieu d’avoir 8 tableaux de valeurs, nous en avons 9 afin de décrire
une indexation interne.

L’utilisation d’une indexation interne permet de définir une valeur
*puit* pour décrire une *cellule pure* puisque la normale est à nulle et
la distance aussi. L’index interne pointe alors toujours au même
endroit. Si le maillage ne comprend que des cellules pures, le gain va
tendre vers 9 pour cette nouvelle description plus compréhensible (/9 à
comparer au surcoût de x9/4.25, soit un gain de 4.25).

Un autre aspect d’optimisation mémoire concerne la symétrisation des
valeurs d’une interface vu par chacun des matériaux de part et d’autres
de cette interface. Une indexation exploitant l’espace des valeurs
négatives pourrait aisément exploiter la même description d’une
interface et de son opposé… faut-il encore partager les valeurs de la
description de la première et de la seconde interface. Le gain serait
alors d’un facteur 6/10 (ce qui compense presque le surcoût de x9/4.25).

D’autres pistes d’optimisation peuvent être aisément mises en place
comme le passage en valeurs simple précision au lieu des double
précision… le passage en valeurs entières étant plus délicates à moins
de considérer la localisation de la cellule concernée pour chaque
interface (cette dernière optimisation nécessite de modifier la valeur
de la distance qui elle doit rester flottante).

Cette optimisation mémoire sera : - soit du fait du filtre source ou de
lecture des données qui définiront les tableaux virtuels adéquats, -
soit le fait de l’application d’une méthode adaptée d’auto-compaction a
appelé dans ces filtres après la définition des champs de valeurs.

Naturellement, nous préconisons la seconde dans l’esprit de mise en
commun.

Perspectives
------------

En dehors du fait d’utiliser la description d’interface fournie par le
code, les filtres de reconstruction d’interface doivent être soit
modernisés soit engagés une nouvelle réflexion afin d’en définir de
nouveaux.

Il faut être conscient que la reconstruction d’interface d’un matériau
nécessite, comme le mode ``Old-school CEA interfaces``, une information
de voisinage concernant les autres matériaux. C’est relativement aisé
sur le maillage global de la simulation mais beaucoup moins une fois le
découpage en plusieurs maillages matériaux réalisés.

Filtre de Youngs
~~~~~~~~~~~~~~~~

Le filtre de Youngs est un filtre disponible dans VTK à la faveur du
CEA. Un travail équivalent de renovation et validation doit être réalisé
sur le filtre de Youngs avec pour objet que ce dernier produise les
champs de valeurs nécessaires pour nourrir le filtre de
MaterialInterface alors appelé en interne de ce filtre, avant de libérer
ensuite ces champs de valeurs.

Ce travail sera probablement engagé en 2023.

Filtre Meredith
~~~~~~~~~~~~~~~

D’autres méthodes de reconstruction d’interfaces ne se basant plus sur
des interfaces planes peuvent être envisagées.

La méthode Meredith dont les premiers papiers sont sorties en 2010 est
une approche qui nous semble très intéressante afin de faire des
interfaces plus propres. L’idée est de décomposer la cellule en un
certain nombre d’éléments puis d’attribuer chacun de ces éléments à un
matériau de la cellule mixte.

Avec une qualité d’implémentation élevée avec cette logique et une
possibilité de paramètrage avancée pourraient apporter des résultats
extrêmements probants.

*Visualization and Analysis-Oriented Reconstruction of Material
Interfaces Jeremy S. Meredith,Hank Childs*

Ce travail envisagé depuis les années 2012 pourrait peut être voir le
jour en 2024.

.. |image0| image:: ../../img/filters/MaterialInterfaceSelectSource.png
.. |image1| image:: ../../img/filters/MaterialInterfaceSelectFilter.png
.. |image2| image:: ../../img/filters/MaterialInterfaceFieldsSelection.png
.. |image3| image:: ../../img/filters/MaterialInterfaceResult0.png
.. |image4| image:: ../../img/filters/MaterialInterfaceTriangleArtefact.png
.. |image5| image:: ../../img/filters/MaterialInterfaceCelluleMixteInterface.png
.. |image6| image:: ../../img/filters/MaterialInterfaceCelluleMixteZones.png
.. |image7| image:: ../../img/filters/MaterialInterfaceCelluleMixteInterfaceS.png
