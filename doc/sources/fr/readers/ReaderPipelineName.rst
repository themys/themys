Documentation: `doc en <../../en/readers/ReaderPipelineName.html>`__



Configuration du nom dans le pipeline
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Par défaut, **Themys** utilise le nom du fichier comme nom de l'instance du lecteur à créer
dans l'explorateur du pipeline (**Pipeline Browser**). Ceci se fait dès la création de l'objet
dans le pipeline et ne sera, par la suite, plus modifiable par cette instance.

Bien souvent, ce choix n'est pas le meilleur, c'est pourquoi il est possible
dans **Themys** de modifier le nom de tout élément du pipeline en double-cliquant
dessus ou en utilisant le menu contextuel. Il est ainsi possible de changer le nom de
l'instance d'un lecteur.

Une évolution récente dans **Themys** permet lors de la création de l'instance du lecteur
d'utilsier un nom qui est retourné par le lecteur lui-même. Cela se fait à travers
une propriété de type *String* nommée **RegistrationName** auquelle
sont associés les services éponymes d'accès préfixé *Get* et de positionnement préfixé *Set*.
Cette propritété est automatiquemnt appelée après la création de l'instance du lecteur
afin d'être utilisé pour nommer l'objet récemment créé dans l'explorateur du
pipeline. A charge au lecteur de proposer un meilleur nommage.

Si cette propriété n'existe pas, on revient dans le mode opératoire initial qui exploite
le nom du fichier.

Une implémentation correspondant au fonctionnement par défaut donnerai ceci :

.. code-block:: xml

   <StringVectorProperty
      label="Registration Name"
      name="RegistrationName"
      number_of_elements="1"
      command="GetFileName"
      panel_visibility="never"
      information_only="1">
   </StringVectorProperty>

L'important est le nom de cette propriété **RegistrationName**, le nom de la méthode pouvant être
modifié en modifiant l'attribut *command*.

Un exemple de définition et d'utilisation de cette propriété est faite dans le lecteur Hercule.

.. note::
   A retenir, la propriété **RegistrationName** est forcément un *String*. La commande doit retourner
   un pointeur sur un *String* dont l'existence doit perdurer à l'appel au niveau du lecteur.
