Documentation: `doc en <../../en/readers/HerculeReaders_Properties_Version.html>`__


Version
^^^^^^^

La propriété **Version** n'est disponible qu'en affichage avancé (grâce au bouton engrenage), elle permet d'afficher la version du lecteur Hercule.
