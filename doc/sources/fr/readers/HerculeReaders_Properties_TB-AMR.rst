Documentation: `doc en <../../en/readers/HerculeReaders_Properties_TB-AMR.html>`__


HTG up to level max
^^^^^^^^^^^^^^^^^^^

Cette propriété fixe le niveau de profondeur maximal que l'on doit charger et conserver en mémoire.

Une valeur nulle ne conserve que le premier niveau.

Dans le même esprit, il est aussi possible d'appliquer un filtre **vtkHyperTreeGridDepthLimiter**.
Celui-ci ne réduira pas le coût mémoire (contrairement à cette option du lecteur) mais il pourra accélérer
l'obtention d'un résultat en limitant les niveaux qui seront pris en compte dans l'application des filtres suivants.
