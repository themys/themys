Documentation: `doc en <../../en/readers/HerculeReaders_Properties.html>`__

Propriétés des lecteurs Hercule
-------------------------------

Le lecteur **Hercule Services Reader** utilise l' **API HS** d'Hercule, aussi appelée **Hercule Services**.

Cette API Hercule spécifique à l'analyse et la visualisation intègre différentes
manipulations et permet d'accèder à des informations cachées de la base.
Dans ce mode, au niveau d'un serveur, une reconstruction est faite afin d’assembler les sous-domaines de calcul de la
simulation qui lui a été attribué. Ainsi, chaque serveur ne décrira qu’une **pièce**, le nombre de **pièces**
correspondra au nombre de serveurs **Themys** en cours d'exécution.

**Il est fortement conseillé d'utiliser ce lecteur pour charger une bande Hercule non HTG/TB-AMR**
**Il ne faut pas utiliser ce lecteur pour charger des bandes HTG/TB-AMR!**

Le lecteur **Hercule HIc Reader** utilise l'**API HIC** d'Hercule, qui est celle employée par les codes de calcul pour écrire leurs bandes.

Avec ce lecteur, le chargement de maillage TB-AMR se fait en **vtkHyperTreeGrid**, cette représentation spécifique de VTK
gère nominalement ce type de maillage. A cette représentation est associée des filtres spécifiques dont
certains appliquent des traitements locaux à une cellule nécessitant les informations des cellules voisines
comme c'est le cas du calcul d'iso-contour ou de gradient.

Dans ce mode, au niveau d'un serveur :

   - pour la représentation **vtkHyperTreeGrid**, une reconstruction est faite afin d’assembler les
     sous-domaines de calcul de la simulation qui lui a été attribué. Ainsi, chaque serveur ne décrira
     qu’une **pièce**, le nombre de **pièces** correspondra au nombre de serveurs **Themys** en cours d'exécution.

   - pour les autres représentations, le nombre de **pièces** correspondra au nombre de sous-domaine de calcul
     de la simulation avec une distribution de ces **pièces** entre les serveurs **themys**.

**Il est fortement déconseillée d'utiliser ce lecteur pour charger tout autre bande Hercule que les bandes HTG/TB-AMR!**


Propriétés communes aux lecteurs **Hercule Services Reader** et **Hercule HIc Reader**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   HerculeReaders_Properties_Version
   HerculeReaders_Properties_CurrentTime
   HerculeReaders_Properties_FixedTime
   HerculeReaders_Properties_TimeShift
   HerculeReaders_Properties_MeshArray
   HerculeReaders_Properties_MaterialArray
   HerculeReaders_Properties_DataArray
   HerculeReaders_Properties_ExtremasSpectralForSummation
   HerculeReaders_Properties_MemoryEfficient
   HerculeReaders_Properties_Extruder1D

.. figure:: ../../img/readers/properties_common.png
   :width: 100%
   :align: center

   Un exemple d'affichage des propriétés communes aux lecteurs **Hercule Services Reader** et **Hercule Hic Reader**.


Propriétés spécifiques au lecteur **Hercule HIc Reader**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   HerculeReaders_Properties_TB-AMR

.. figure:: ../../img/readers/properties_hic.png
   :width: 100%
   :align: center

   Un exemple d'affichage des propriétés du lecteur **Hercule Hic Reader**.
