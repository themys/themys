Documentation: `doc en <../../en/readers/HerculeReaders.html>`__


Lecteurs Hercule
================

Deux lecteurs de bandes Hercule sont disponibles : **Hercule Services Reader** et **Hercule HIc Reader**.

.. warning::
   Le lecteur **Hercule Services Reader** est à utiliser pour charger toute bande Hercule **qui n'est pas de type HTG/AMR-TB**.

   Le lecteur **Hercule HIc Reader** est à utiliser pour charger une bande Hercule **de type HTG/AMR-TB**.

.. toctree::
   :maxdepth: 1

   HerculeReaders_Output_Description
   HerculeReaders_Differences_With_Love
   HerculeReaders_Properties
   HerculeReaders_Configuration
