Documentation: `doc en <../../en/readers/HerculeReaders_Properties_ExtremasSpectralForSummation.html>`__


Extremas spectral for summation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le chargement d'un champ de valeurs spectrales est très coûteux puisque pour chaque cellule on
peut avoir plusieurs centaines de valeurs.

C'est pourquoi chaque grandeur spectrale est proposée sous trois formes :

* sa **version complète vectorielle**, par exemple : *Milieu:Spectrum* ;
* sa **version partielle vecorielle**, par exemple : *Milieu:vtkPart_Spectrum* ;
* sa **version sommée partielle scalaire**, par exemple : *Milieu:vtkSum_Spectrum*.

Le premier est le champ de valeurs complétement décrit tel qu'il est stocké dans la base Hercule.

Les deux suivants permettent de réaliser une réduction au chargement d'un champ spectral.

Pour cela, les valeurs de la propriété **Extremas spectral for summation** sont exploitées afin de déterminer les
indices qui seront retenus :

* soit pour l'extraction d'une partie du champ spectral, *vtkPart_* ;
* soit pour réaliser la somme (partielle), *vtkSum_*.

La première valeur indique de cette propriété indique le premier indice qui sera pris en compte.
La valeur -1 est équivalente à 0.

La seconde valeur indique le dernier indice retenu.
S'il vaut -1, la valeur de l'indice correspondant au dernier élément du champ spectral concerné sera pris en compte.
