Documentation: `doc en <../../en/readers/HerculeReaders_Properties_CurrentTime.html>`__


Current Time
^^^^^^^^^^^^

Cette propriété, disponbile uniquement en affichage avancé (grâce au bouton engrenage), donne la valeur du temps de simulation actuellement chargé pour cette instance
du lecteur Hercule.

Cette information est, aujourd'hui, redondante avec celle donnée au niveau de l'onglet **Information** dans
la partie **Data Statistics** ou sous la forme de deux champs de valeurs globaux (**FieldData**)
**vtkFixedTimeStep** et **vtkFixedTimeValue**,
que ce soit pour cette instance du lecteur ou pour une instance d'un filtre exploitant les données retournées
en **output** (sortie).

Ce temps de simulation peut être différent de celui qui a été explicitement demandé au niveau de la **GUI Themys**
(Interface Utilisateur Graphique).

Lorsqu'un temps est demandé à l'instance d'un lecteur Hercule, le temps effectivement chargé sera choisi
par ordre de priorité :

* le temps demandé s'il est présent dans la base ; sinon
* le temps le plus proche en-dessous de la valeur du temps demandée ; sinon
* le temps le plus proche par au-dessus de la valeur du temps demandée.

D'autres propriétés du lecteur Hercule peuvent aussi influencer le temps chargé :

* la propriété **Fixed Time** permet de fixer un temps au niveau de cette instance de lecteur, à partir de là,
  ce lecteur ne tient plus en compte des requêtes relatives à la **GUI Themys**, ce choix est fait en sélectionnant
  explicitement un des temps de simulation disponible pour cette base ;
* la propriété **Time Shift**" permet de définir un décalage à appliquer au temps demandé au niveau de la
  **GUI Themys**, ensuite le choix du temps finalement retenu rentre dans le modus operanti défini précédemment.
