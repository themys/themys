Documentation: `doc en <../../en/readers/HerculeReaders_Properties_FixedTime.html>`__


Fixed Time
^^^^^^^^^^

Cette propriété propose de maîtriser le choix du temps de simulation à charger pour une instance du lecteur Hercule.

Le choix se fait à travers le positionnement dans un menu déroulant :

* commençant par une *zone vide*, puis
* comprenant la liste des temps de simulation qui sont disponibles dans cette base.

Le positionnement sur un temps de simulation va fixer de **façon immuable** le temps de
chargement pour cette instance de base. Les événements liès à un choix au niveau de la **GUI Themys** n'auront
alors **plus aucun effet**. La propriété `Time Shift <HerculeReaders_Properties_TimeShift.html>`_ sera ignorée.

Le positionnement sur la *zone vide* permet de désactiver cette propriété, la **GUI Themys** retrouve son rôle
initial.
