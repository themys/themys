Documentation: `doc en <../../en/readers/DatReader.html>`__



Lecteur DAT
===========

Description
-----------

Le filtre ``CEAReaderDat`` permet de lire un fichier de type ``.dat``.

Le format de fichier .dat
-------------------------

Le format de fichier ``.dat`` est la description d’une liste de poly-lignes dans un fichier ``ASCII``.
Une poly-ligne est décrite par une liste de points de l’espace reliés entre eux.

Voici un exemple de fichier ``.dat`` décrivant deux poly-lignes.

::

   -1.0000000000 0.0000000000
   0.0000000000 1.0000000000
   1.0000000000 0.0000000000
   0.0000000000 -1.0000000000
   &
   -2.0000000000 0.0000000000
   0.0000000000 2.0000000000
   2.0000000000 0.0000000000
   &

Chaque ligne décrit :

* soit un point de l’espace par un tuple de deux ou trois valeurs flottantes séparées par un espace ;

* soit une ligne séparatrice ``&`` entre deux poly-lignes. Le fichier se termine avec  ce genre de ligne séparatrice.

Option
------

Z Epsilon
~~~~~~~~~

Le ``Z Epsilon`` permet de fixer une translation dans la direction ``Z``. Par défaut, la valeur est à 0.01.

Dans le cas de multi poly-lignes planes dans l'espace 3D, la valeur du ``Z Epsilon`` doit être positionnée à 0.

Possible développement
----------------------

Optionnellement, dans ce filtre de lecture juste aprés le chargement des données, les valeurs de ``Z`` des points
devraient être positionnée à 0 si tous les points du fichier ``.dat`` décrits dans l’espace 3D ont une même valeur
de ``Z`` non nulle.
