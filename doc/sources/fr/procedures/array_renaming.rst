Documentation: `doc en <../../en/procedures/array_renaming.html>`__

Renommage automatique des grandeurs
===================================

Paramètres de renommage
^^^^^^^^^^^^^^^^^^^^^^^

Dans le menu ``Settings`` onglet ``Themys``, il est possible d'unifier les nommages d'éléments venant de
différentes bases.

|image1|

Ici, un exemple de la section ``Matching table for array renaming`` qui permet durablement de faire l'association
entre un ou plusieurs nommages présents dans différentes bases (à droite, ici *vtkInterfaceFraction*
et *Milieu:fracpres*) et un nommage pour la visualisation (à gauche, ici *frac*).

L'interface permet d'ajouter, supprimer ou modifier une association.

La création d'une nouvelle association nécessite :

- d'écrire à gauche, un *nouveau nom* ;

- d'écrire à droite la liste des *anciens noms* qui seront remplacés dans l'interface par le *nouveau nom*.
  La saisie de cette liste de nouveaux noms se fait en les séparant par des points virgules.

.. note::
   C'est à l'utilisateur d'attribuer un nommage différent de celui déjà présent dans la base.

.. |image1| image:: ../../img/procedures/array_renaming.png
