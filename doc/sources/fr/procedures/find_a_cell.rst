

Trouver une cellule
===================

Description
-----------

L'objet ici est de trouver une cellule à travers son identifiant global donné par le
code de simulation. Cet identifiant est remonté à l'utilisateur sous la dénomination
**vtkCellId** au niveau des cellules et **vtkNodeId** au niveau des points.

Via l'interface graphique
-------------------------

Pour trouver une cellule à partir de l'identifiant global fourni par le code, vous devez :

* avoir chargé le champ de valeurs **vtkCellId**, si ce n'est pas le cas :

  * en cliquant sur le lecteur de base Hercule dans l'explorateur du pipeline (**Pipeline Browser**) ;

  * puis en allant dans **Properties** (propriétés), pour cliquer sur **vtkCellId** ;

  * sans oubier de cliquer sur le bouton **Apply** toujours dans **Properties** ;

* ouvrir la vue **FindData** (trouver un élément de maillage par rapport à une valeur),
  dans le menu **View** (vue) ; généralement cette vue s'ouvre à droite à côté de la
  fenêtre d'édition de la map de couleur (**Color Map Editor**), l'onglet se trouve en bas ;

* sélectionner le bon **Data Produceur** ; si vous avez plusieurs bases dans le pipeline,
  n'hésitez pas à renommer les objets en double cliquant dessus à la façon dont on ferait
  dans un navigateur de fichiers classiques ;

* sélectionner dans **Element Type** la valeur **Cell** ;

* définisser la régle : **vtkCellId** **is** puis vous saisissez dans l'éditeur
  de ligne la valeur de l'identifiant global correspondant à la cellule que vous
  cherchez ;

* puis cliquer sur le bouton **FindData** un peu plus bas ;

* vous voyez le résultat de la sélection dans la fenêtre de rendu mais aussi dans
  l'onglet **FindData** section **SelectedData** en changeant l'attribut,
  vous verrez des informations appliquées sur des éléments de géométries différents.

.. note::
   A noter que la procédure est similaire pour trouver un point à partir de son
   identifiant global défini par le code de simulation.
