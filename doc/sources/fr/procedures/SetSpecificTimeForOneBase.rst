Documentation: `doc en <../../en/procedures/SetSpecificTimeForOneBase.html>`__

Documentation: `doc en <../../en/procedures/FixATimeForOneBase.html>`__

Fixer un temps spécifique pour une base
============================

Description
-----------

Themys/ParaView permet d'ouvrir plusieurs données avec différents temps
de simulation.

Par défaut, lorsqu'une nouvelle base de données est ouverte, tous ses pas de temps sont fusionnés dans la liste des temps proposés dans ParaView.

Sélectionner un autre temps de simulation impose aux lecteurs de charger le temps de simulation le plus proche de cette nouvelle valeur (par le bas).


Fixer un temps spécifique pour une base
---------------------------------------

Dans le ``Pipeline Browser``, faites un clic droit sur la base de données et
cochez la case ``Ignore Time``. Tout nouveau choix d'un temps de simulation
dans l'interface graphique n'aura aucun effet sur cette base tant que cette
case est cochée.

Notez que toutes les valeurs de temps pour cette base de données ne sont plus
visibles dans l'interface graphique.

Décochez la case pour pouvoir changer, à nouveau, le temps de simulation de la base.

Dans le panneau ``Properties`` pour une base de données ouverte en utilisant
les lecteurs *Hercule*, une option nommée ``Fixed Time`` est disponible. Par
défaut, cette option est définie avec un champ vide. Choisissez le temps
souhaité et cliquez sur ``Apply`` pour verrouiller cette base de données
sur ce temps. Cela a le même effet que de cliquer sur ``Ignore Time``.

Pour restaurer le comportement par défaut, définissez la variable
``Fixed Time`` sur un champ vide.
