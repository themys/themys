Documentation: `doc en <../../en/procedures/Slicing.html>`__



Filtre *Slice*
================

Description
-----------

L'opération de tranchage (*Slicing*) extrait l'intersection entre une géométrie
donnée et un ensemble de donnée.

Procédure avec l'interface graphique (GUI)
-----------------------

Commencez par sélectionner la donnée à trancher dans le panneau
``Pipeline Browser`` en cliquant dessus. Ensuite cliquez sur le filtre
``Slice`` dans la barre d'outil, comme montré ci-dessous.

|image0|

La géométrie de découpe par défaut est le plan, qui apparaît en rouge
dans la fenêtre de visualisation. Vous pouvez interagir directement avec
ce plan de la manière suivante :

- En cliquant sur le plan lui-même pour le déplacer le long de son axe.
- En cliquant sur l'axe pour ajuster son orientation.
- En cliquant sur la boule au centre de l'axe pour déplacer l'origine du
  plan (non visible sur l'image ci-dessous).

|image1|

Pour définir ces paramètres avec plus de précision, les coordonnées de la
position et de la normale du plan de coupe peuvent également être
spécifiées dans le panneau ``Properties``. Ensuite, cliquez sur ``Apply``
pour exécuter le filtre.

Pour permettre une interaction plus facile avec l'ensemble de données,
décochez l'option ``Show Plane`` pour masquer le plan de découpe comme
indiqué ci-dessous.

|image2|

Il est possible de produire plus d'une tranche. Pour ce faire, activez d'abord
le mode avancé en cliquant sur la *roue dentée* illustré ci-dessous. Une liste
contenant les décalages de position par rapport au plan de découpe actuel
apparaît. Vous pouvez définir autant de tranches que vous le souhaitez.

|image3|

Diverses options sont disponibles pour modifier la liste des tranches,
 détaillées ci-dessous.

|image4|

1. **Bouton +** : Ajoute une nouvelle valeur à la liste en dessous de la
   valeur sélectionné actuellement.
2. **Bouton -** : Supprime de la liste la valeur sélectionnée.

3. **Bouton de génération automatique de valeurs** : Ouvre une fenêtre
   proposant des options pour générer des positions distribuées de
   différentes manières (linéaire, logarithmique, etc.) dans un intervalle
   donné.

   |image5|

4. **Bouton de suppression** : Supprime toutes les valeurs de la liste.

5. **Bouton de redimensionnement** : Divise ou multiplie par 2 toutes les valeurs.

6. **Bouton de réinitialisation** : Réinitialise la liste à son état d'origine.

Pour modifier une valeur, cliquez simplement sur l'élément et entrez une
nouvelle valeur.

D'autres géométries sont disponibles, comme la boite, le cylindre ou la sphère.

.. note::
  Pour des données HTG , le tranchage multiple est disponible uniquement
  selon un plan en utilisant le filtre ``Axis-aligned slice``.


|image6|

|image7|

|image8|

Procedure using Python scripting
--------------------------------

1. Plane
~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set to slice
   mySource = FindSource('Wavelet1')

   # Create a new 'Slice' filter
   slice = Slice(Input=mySource)

   # If needed, change the origin and normal of the slicing plane
   slice.SliceType.Origin = [3.0, 0.0, 0.0]
   slice.SliceType.Normal = [1.0, 1.0, 0.0]

   # Optionally add more slices along the defined Normal (0.0 corresponds to the slice defined with Origin)
   slice.SliceOffsetValues = [0.0, 5.0, -5.0]

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   sliceDisplay = Show(slice, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   sliceDisplay.SetScalarBarVisibility(renderView, True)

2. Box
~~~~~~

.. code:: py

   # Change slicing geometry to a box
   # Note that rotation is expressed in degrees
   slice.SliceType = 'Box'
   slice.SliceType.Position = [0.0, 0.0, 1.0]
   slice.SliceType.Rotation = [0.0, 45.0, 0.0]
   slice.SliceType.Length = [5.0, 5.0, 10.0]

3. Cylinder
~~~~~~~~~~~

.. code:: py

   # Change slicing geometry to a cylinder
   slice.SliceType = 'Cylinder'
   slice.SliceType.Center = [0.0, 0.0, -3.0]
   slice.SliceType.Axis = [0.5, 1.0, 0.5]
   slice.SliceType.Radius = 3.0

4. Sphere
~~~~~~~~~

.. code:: py

   # Change slicing geometry to a sphere
   slice.SliceType = 'Sphere'
   slice.SliceType.Center = [1.0, 0.0, 0.0]
   slice.SliceType.Radius = 5.0

.. |image0| image:: ../../img/procedures/03SliceFilterSelection.png
.. |image1| image:: ../../img/procedures/03SliceFilterAppearance.png
.. |image2| image:: ../../img/procedures/03SliceFilterHidePlane.png
.. |image3| image:: ../../img/procedures/03SliceFilterMultiple.png
.. |image4| image:: ../../img/procedures/03SliceFilterOptions.png
.. |image5| image:: ../../img/procedures/03SliceFilterValueGenerator.png
.. |image6| image:: ../../img/procedures/03SliceBox.png
.. |image7| image:: ../../img/procedures/03SliceCylinder.png
.. |image8| image:: ../../img/procedures/03SliceSphere.png
