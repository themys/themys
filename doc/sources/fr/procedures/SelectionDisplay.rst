Documentation: `doc en <../../en/procedures/SelectionDisplay.html>`__



Afficher des informations sur une sélection
===========================================

Description
-----------

Parcourir un ensemble de données est plus aisé après avoir sélectionné
les élements à analyser. Une façon d'examiner ces élements est à travers
la ``Spreadsheet View``.

Procédure avec l'interface graphique (GUI)
------------------------------------------


1. Informations affichées par l'outil *hover*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce type d'affichage d'informations est utile pour obtenir un aperçu rapide
de l'ensemble de données et ne nécessite pas réellement de définir une
sélection, mais il mérite d'être mentionné pour sa simplicité et sa commodité.
Cliquez sur le bouton ``Hover Cells/Points On`` dans la barre d'outils située
au-dessus de la fenêtre de visualisation, comme indiqué ci-dessous.
Ensuite, déplacez simplement le curseur sur une cellule ou un point pour
afficher une infobulle contenant des informations sur l'élément.

|image0|

2. Informations affichées via le panneau *Find Data*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Une fois la sélection faite, il est possible d'afficher les informations
concernant les cellules (ou points) sélectionnés via le panneau ``Find Data``.
Il faut noter que la sélection peut aussi se faire par ce panneau.

|image6|

Ce panneau affiche toutes les variables chargées pour chaque cellule
(ou point) contenue dans la sélection. Il est possible de choisir
les variables visibles grâce au boutton **Column visibility**
(encadré en rouge dans l'image ci-dessous).

|image7|


3. Informations affichées dans la *Spreadsheet View*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


La vue ``Spreadsheet View`` permet un affichage exhaustif des informations.
Pour créer cette vue, chiossissez d'abord la déclinaison d'interface ``Advanced``.

|image1|

Ensuite, séparez la vue actuelle en cliquant sur le bouton correspondant
indiqué ci-dessous.

|image2|

Une nouvelle fenêtre apparait à côté de la fenêtre courante, plusieurs
options sont disponible, choisissez ``SpreadSheet View``.

|image3|


Cette vue se compose d'un tableau contenant des informations telles que les ID,
les coordonnées des points, les types de cellules, les tableaux de données,
etc. Assurez-vous de sélectionner l'ensemble de données souhaité dans la liste
déroulante ``Showing``. Vous pouvez choisir d'afficher des informations sur les
cellules ou les points avec la liste déroulante ``Attribute``. Enfin, en
interagissant avec les lignes en cliquant dessus, les éléments correspondants
sont mis en surbrillance dans la vue de rendu pour les identifier facilement.

|image4|

Pour réduire l'affichage uniquement à la sélection, cochez la case
``Show only selected elements``.

|image5|

Ou alors, si vous souhaitez extraire la sélection pour manipuler un
sous-ensemble de la donnée, voir `cette section
<./SelectionByGlobalID.html#extract-a-selection>`__.


Procédure avec le scripting Python
-----------------------------------

1. Informations affichées par l'outil *hover*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This feature is not available in ``paraview.simple``.

2. Informations affichées via le panneau *Find Data*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This feature is not available in ``paraview.simple``.

3. Informations affichées dans la *Spreadsheet View*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'blow.vtk' according to the desired data set
   mySource = FindSource('blow.vtk')

   # Get layout
   layout = GetLayout()

   # Create a new 'SpreadSheet View'
   sheetView = CreateView('SpreadSheetView')

   # Show data set in spreadsheet
   sourceDisplay = Show(mySource, sheetView)

   # Add spreadsheet view to the layout
   AssignViewToLayout(sheetView, layout)

   # Show only selected elements
   sheetView.SelectionOnly = 1

   # Show cells rather than points
   sheetView.FieldAssociation = 'Cell Data'

.. |image0| image:: ../../img/procedures/08ElementHover.png
.. |image1| image:: ../../img/procedures/08AdvancedMode.png
.. |image2| image:: ../../img/procedures/08ViewSplit.png
.. |image3| image:: ../../img/procedures/08ViewSelection.png
.. |image4| image:: ../../img/procedures/08Spreadsheet.png
.. |image5| image:: ../../img/procedures/08SpreadsheetOnlySelected.png
.. |image6| image:: ../../img/procedures/find_data_click.png
.. |image7| image:: ../../img/procedures/find_data_panel.png
