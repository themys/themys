Documentation: `doc en <../../en/procedures/Thresholding.html>`__


Appliquer un seuil à un ensemble de données
===========================================

Description
-----------


L'opération de seuil sur un ensemble de données extrait les cellules dont les
valeurs d'une des variables sont inférieures ou supérieures à une valeur seuil spécifiée.

Procédure avec l'interface graphique (GUI)
------------------------------------------

Commencez par sélectionner l'ensemble de données à seuiller dans le panneau
``Pipeline Browser`` en cliquant dessus. Ensuite, cliquez sur le filtre
``Threshold`` dans la barre d'outils des filtres comme indiqué ci-dessous.

|image0|

Dans le panneau ``Properties``, choisissez la grandeur scalaire à utiliser
pour le seuil à l'aide de la liste déroulante ``Scalars``.

Ensuite, sélectionnez le type de seuil à appliquer à l'aide de la liste
déroulante ``Threshold Method`` :

-  ``Between`` extrait les cellules dont les valeurs sont comprises
   entre la limite inférieure et supérieure.

-  ``Below Lower Threshold`` extrait les cellules dont les valeurs
   sont plus faibles que la limite inférieure.

-  ``Above Upper Threshold`` extrait les cellules dont les valeurs
   sont plus grandes que la limite supérieure.

Ensuite, les valeurs de seuil (``Lower Threshold`` et ``Upper Threshold``)
peuvent être ajustées avec les curseurs correspondants ou spécifiées directement.

|image1|

Pour inverser le résultat de l'opération de seuil, cochez l'option
``Invert`` comme indiqué ci-dessous.

|image2|

Procédure avec le scripting Python
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set to threshold
   mySource = FindSource('Wavelet1')

   # Create a new 'Threshold' filter
   threshold = Threshold(Input=mySource)

   # If needed, change scalar data array
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'RTData' with a valid scalar array
   threshold.Scalars = ['POINTS', 'RTData']

   # If needed, change threshold type (default is 'Between')
   threshold.ThresholdMethod = 'Below Lower Threshold'
   # or
   threshold.ThresholdMethod = 'Above Upper Threshold'
   # or
   threshold.ThresholdMethod = 'Between'

   # Set threshold values
   threshold.LowerThreshold = 70.0
   threshold.UpperThreshold = 250.0

   # Optionally invert the output data
   threshold.Invert = 1

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   thresholdDisplay = Show(threshold, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   thresholdDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/05ThresholdFilterSelection.png
.. |image1| image:: ../../img/procedures/05ThresholdFilterOptions.png
.. |image2| image:: ../../img/procedures/05ThresholdFilterInvert.png
