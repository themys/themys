Documentation: `doc en <../../en/procedures/ExportSelection.html>`__



Exporter une sélection interactive
==================================

Description
-----------

Une fois qu'une opération de sélection est effectuée, nous voulons
généralement l'extraire ou l'analyser. Nous verrons ici comment effectuer
une sélection en utilisant les outils interactifs,
puis l'exporter dans un fichier.

Procédure avec l'interface graphique (GUI)
-----------------------

1: Faire la sélection interactive
~~~~~~~~~~~~~~~~~

La barre d'outils située au-dessus de la fenêtre de visualisation contient
divers outils pour sélectionner des cellules et des points dans un ensemble
de données. Certains de ces outils seront détaillés ici, en commençant par
les boutons ``Select Cells/Points On``. Après avoir cliqué sur l'un de ces
boutons, cliquez simplement dans la fenêtre de visualisation et faites
glisser pour définir un rectangle de sélection.

|image0|

En relâchant, les points ou cellules à l'intérieur du rectangle sont
surlignés, indiquant qu'ils ont été sélectionnés.

|image1|

Notez que seuls les points et cellules les plus proches de la caméra (en
termes de profondeur) sont sélectionnés. Les éléments cachés par d'autres
ne sont pas sélectionnés. Pour sélectionner tous les points ou cellules qui
rentrent dans le rectangle, y compris ceux qui sont cachés, utilisez les
boutons ``Select Cells/Points Through``. Comme précédemment, cliquez et
faites glisser pour définir un rectangle de sélection, puis relâchez.

|image2|

Les éléments sont maintenant sélectionnés à travers l'ensemble de données
comme montré ci-dessous pour les cellules.

|image3|

Au lieu d'un rectangle, il est possible de dessiner une forme de manière libre
en utilisant les boutons ``Select Cells/Points With Polygon`` comme illustré
ci-dessous.

|image4|

|image5|

Il est également possible de sélectionner cellule par cellule ou point par
point de manière interactive. Pour cela, cliquez sur les boutons
``Interactive Select Cells/Points On``, puis cliquez sur les cellules ou les
points que vous souhaitez sélectionner. Un message apparaîtra pour vous
expliquer comment utiliser ce mode. Gardez à l'esprit que dans ce mode,
vous ne pouvez pas déplacer la caméra avec votre souris.

|image6|

Par défaut, chacune de ces opérations créera une nouvelle sélection. Si vous
souhaitez ajouter votre nouvelle sélection à votre sélection actuelle, vous
pouvez activer le mode ``Add Selection`` avec le bouton correspondant afin
que chaque sélection subséquente soit ajoutée à la précédente. Au lieu
d'utiliser le bouton, vous pouvez également maintenir la touche **Ctrl**
enfoncée pendant que vous faites votre sélection. De même, il existe un mode
``Substract Selection`` (raccourci **Shift**) et un mode ``Toggle Selection``
(raccourci **Ctrl+Shift**). La sélection interactive expliquée au-dessus est
une exception et ajoutera toujours la cellule ou le point nouvellement
sélectionné à la sélection actuelle par défaut, mais fonctionne toujours
avec les modes ``Substract`` et ``Toggle``.

|image7|

Il peut être utile d'ajouter les cellules ou points
voisins à une sélection existante. Pour ce faire, cliquez sur le bouton
dédié ``Grow Selection`` montré ci-dessous. Utilisez le bouton ``Shrink
Selection`` pour annuler cette opération.

|image8|

Enfin, vous pouvez effacer une sélection en cliquant sur le bouton correspondant.

|image9|

2: Exporter votre sélection
~~~~~~~~~~~~~~~~~~~~~~~~

Une fois la sélection effectuée, il est d'abord nécessaire de l'extraire
avant de l'exporter. Pour cela, créez un filtre ``Extract Selection`` et
cliquez sur ``Apply``. Si vous modifiez votre sélection après la création
du filtre, la sélection extraite ne sera pas mise à jour. En effet, ce filtre
fait une copie de la sélection actuelle lors de sa création. Si vous
souhaitez extraire une nouvelle sélection en utilisant le même filtre,
utilisez le bouton ``Copy Active Selection``. Voir l'image ci-dessous
pour un exemple où une sélection a été précédemment extraite mais
doit être modifiée.

|image10|

Une fois votre sélection extraite, assurez-vous que le filtre ``Extract
Selection`` est sélectionné et allez dans ``File - Save Data`` pour
l'exporter. Dans la première boîte de dialogue, vous pourrez choisir où
l'enregistrer et quel format utiliser. Une fois cette étape validée, une
seconde boîte de dialogue s'ouvrira avec les paramètres d'exportation
spécifiques au format de fichier que vous avez choisi.


.. |image0| image:: ../../img/procedures/06SelectionOn.png
.. |image1| image:: ../../img/procedures/06SelectionDone.png
.. |image2| image:: ../../img/procedures/06SelectionThrough.png
.. |image3| image:: ../../img/procedures/06SelectionThroughDone.png
.. |image4| image:: ../../img/procedures/06SelectionPolygon.png
.. |image5| image:: ../../img/procedures/06SelectionPolygonDone.png
.. |image6| image:: ../../img/procedures/06InteractiveSelection.png
.. |image7| image:: ../../img/procedures/06SelectionModeButtons.png
.. |image8| image:: ../../img/procedures/08GrowSelection.png
.. |image9| image:: ../../img/procedures/06SelectionClear.png
.. |image10| image:: ../../img/procedures/06ExtractSelectionFilter.png
