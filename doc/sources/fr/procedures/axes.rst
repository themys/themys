Documentation: `doc en <../../en/procedures/axes.html>`__



Axes
====

Paraview propose différents modes de rendu des axes.

Dans les propriétés (``Properties``), il vous suffit de saisir ``axe``
dans le champ de recherche en ayant bien activé le mode avancé en cliquant
sur la *roue dentée* à droite de ce champ de saisie.

Data Axes Grid
--------------

Dans la catégorie ``Display`` (affichage), nous trouvons la possibilité d'afficher le
``Data Axes Grid`` qui va construire la représentation des axes sur la boîte
englobante de la source active dans le ``Pipeline Browser``
(navigateur de pipeline). Un bouton ``Edit`` est associé afin de changer
différentes propriétés de cet objet.

Axes Grid
---------

Dans la catégorie ``View (Render View)``, nous trouvons la possibilité
d'afficher le ``Axes Grid`` qui va construire la représentation des axes
en tenant compte de toutes les instances d'objet visualisés dans le ``Pipeline
Browser``. Une instance est visualisée si l'oeil à sa gauche est coché.
Un bouton ``Edit`` est associé afin de changer différentes propriétés de cet objet.

Polar Axes
----------

Dans la catégorie ``Annotations``, nous trouvons la possibilité d'afficher le
``Polar Axes`` accompagné d'un bouton ``Edit`` pour accéder aux propriétés de
cet objet qui va construire la représentation polaire sur l'axe X-Y.

Il est possible de paramétrer les dimensions de la grille polaire, ainsi que
le nombre d'arcs ou d'axes radiaux affichés et le delta entre chaque.

Un des paramètres proposés permet de définir un ``Ratio`` au niveau
de la catégorie ``Aspect Control`` afin d'obtenir un aspect elliptique.

De plus, l'option ``Screen Size`` permet de gérer globalement le rendu des textes.


|image0|

.. note::
  Beaucoup de paramètres sont disponibles et certains peuvent paraître obscurs.
  Cette image permet une meilleure compréhension des notations utilisées.

Legend Grid
----------

Dans la catégorie ``Orientation Axes``, nous trouvons la possibilité d'afficher le
``Legend Grid`` accompagné d'un bouton ``Edit`` pour accéder aux propriétés de
cet objet. Cet objet est par défaut grisé, pour l'activer il faut cocher la case
précédente ``Camera Parallel Projection``.

Une fois activé, un cadre (pouvant être redimensionné dans les propriétés)
s'affiche dans la fenêtre de rendu. Ce cadre est fixe et seules les graduations
bougent lors de zoom ou de déplacement de la caméra.

Polar Grid
----------

Dans la catégorie ``Orientation Axes``, nous trouvons la possibilité d'afficher le
``Polar Grid`` accompagné d'un bouton ``Edit`` pour accéder aux propriétés de
cet objet. Cet objet est par défaut grisé, pour l'activer il faut cocher la case
précédente ``Camera Parallel Projection``.

Une fois activé, une grille polaire (pouvant être redimensionnée dans les propriétés)
s'affiche dans la fenêtre de rendu. Cette grille est fixe et seules les graduations
bougent lors de zoom ou de déplacement de la caméra.


.. |image0| image:: ../../img/procedures/polar_axes.png
