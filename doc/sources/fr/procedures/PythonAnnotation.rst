Documentation: `doc en <../../en/procedures/PythonAnnotation.html>`__



Créer une annotation
====================

Description
-----------

Les annotations sont utilisées pour afficher des informations textuelles à
l'écran, telles que le temps, les valeurs de données ou les commentaires.
Le filtre d'annotation décrit ici est le ``Python annotation``,
qui est très polyvalent.


Procedure avec l'interface graphique (GUI)
-----------------------

Commencez par activer le mode avancé en cliquant sur le bouton correspondant,
comme indiqué ci-dessous. Après avoir sélectionné l'ensemble de données dans
le panneau ``Pipeline Browser`` en cliquant dessus, allez dans ``Filtres >
Annotation > Python Annotation`` dans le menu.


|image0|

Dans le panneau ``Properties``, la liste déroulante ``Array Association``
spécifie le type de données qui peut être référencé dans l'annotation.
Ensuite, entrez l'annotation dans le champ de saisie ``Expression``.
L'exemple ci-dessous affiche l'expression suivante :
``"Current time step is %f $\mu$s" % (time_value * 1000000)``.

|image1|


Notez que ``time_value`` ci-dessus est une variable définie pour les
ensembles de données temporelles. Le formatage de base LaTeX est également
possible en encadrant le texte avec des symboles **$**.

D'autres exemples sont disponibles `ici <#annotation-examples>`__. Pour plus
de détails sur les différentes possibilités d'annotation, voir `cette
page <https://docs.paraview.org/en/latest/ReferenceManual/annotations.html?#python-annotation-filter>`__.



Des options pour déplacer l'annotation dans la fenêtre de visualisation et
pour changer les propriétés du texte sont disponibles dans la section
``Display`` du panneau ``Properties`` (encadrées en rouge et bleu ci-dessous,
respectivement).

Pour l'emplacement de l'annotation, vous pouvez soit choisir une position fixe
dans la fenêtre, soit choisir ``Use Coordinates`` pour cliquer directement
sur l'annotation et la déplacer à l'endroit souhaité.

|image2|

Exemples d'annotation
-------------------

Cette section contient plusieurs exemples d'annotations incluant des variables
et un formatage spécial. Pour les annotations plus longues, vous pouvez écrire
la chaîne de texte dans un éditeur de texte avant de la coller dans le champ
de saisie ``Expression``.


-  Texte et variable:
   ``"Current time step is %f ms" % (time_value * 1000)`` (ou t_value)

|image3|

-  Grandeur aux mailles pour un élément (2) de un bloc dans un multiblock (0):
   ``"ScalarData value for cell 3: %f" % ScalarData[2].Arrays[0]``

|image4|

-  Multiple lignes et colonnes: ``"A | B\nC | D"``

|image5|

-  Autres expressions:

   -  ``"Date %s\n" % (date.today())``
   -  ``"Summation RTData: %f\n" % (numpy.cumsum(inputs[0].PointData["RTData"])[-1])``
   -  ``"Summation RTData: %f\n" % (numpy.sum(inputs[0].PointData["RTData"]))``
   -  ``"Avg Density: %f\n" % (numpy.average(inputs[0].CellData["Milieu:Density"]))``

Vous pouvez combiner ces différentes syntaxes.

Pour définir une nouvelle ligne ou colonne de texte, utilisez le caractère
``\n`` ou ``|``, respectivement. Plusieurs options sont disponibles pour
modifier l'espacement entre les cellules ainsi que dessiner des bordures intérieures.

|image6|

Procédure avec du scripting Python
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'can.ex2' according to the data set to annotate
   mySource = FindSource('can.ex2')

   # Create a new 'PythonAnnotation' filter
   annotation = PythonAnnotation(Input=mySource)

   # Define annotation
   annotation.Expression = '"Current time step is %f ms" % (time_value * 1000)'

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   annotationDisplay = Show(annotation, renderView)

   # Change window location of annotation
   annotationDisplay.WindowLocation = 'Lower Center'

   # Manually set coordinates for the lower left corner of the annotation box
   # Position is changed only if the window location is 'Any Location'
   annotationDisplay.WindowLocation = 'Any Location'
   annotationDisplay.Position = [0.6, 0.05]

.. |image0| image:: ../../img/procedures/11PythonAnnotation.png
.. |image1| image:: ../../img/procedures/11ExampleAnnotation.png
.. |image2| image:: ../../img/procedures/11AnnotationParameters.png
.. |image3| image:: ../../img/procedures/11ExampleAnnotation1.png
.. |image4| image:: ../../img/procedures/11ExampleAnnotation2.png
.. |image5| image:: ../../img/procedures/11ExampleAnnotation3.png
.. |image6| image:: ../../img/procedures/11MultiCellOptions.png
