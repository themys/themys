Documentation: `doc en <../../en/procedures/InterfaceModes.html>`__



Ajout d'un mode d'interface
===========================

Contexte
--------

Themys possède un paramètre de configuration pour choisir entre plusieurs modes d'interface. Les modes d'interface modifient l'aspect des barres d'outils et des menus, pour s'adapter à différents cas d'usage de l'application.
Le mode d'interface peut être modifié en utilisant la liste déroulante située en bas à gauche des barres d'outils de Themys.

Actuellement, Themys possède 3 modes d'interface distincts:

* Le mode "Themys" par défaut, utilisant les barres d'outil de Themys et des menus simplifiés.
* "TB-AMR", utilisé pour le travail avec les "Hyper Tree Grid". Ce mode affiche une barre d'outil avec les filtres utilisés pour le traitement des HTG.
* Finalement, le mode "Advanced", qui correspond à l'interface par défaut de Paraview.

Fichier de configuration JSON des modes d'interface
---------------------------------------------------

Les modes d'interface sont configurés à l'aide d'un fichier de description JSON, `interface_modes.json`. Ce fichier se trouve dans `client/ressources` dans le dépôt de code de Themys.
Ce fichier est copié à l'étape de "Configuration" de CMake. Par conséquent, il est nécessaire de rejouer l'étape de configuration du projet pour mettre à jour les changements de ce fichiers.
Ce fichier est lu au lancement de l'application.

Spécification du fichier de configuration
-----------------------------------------

Le fichier JSON de configuration possède 2 éléments de premier niveau : `custom_menus` and `interfaces`.
`custom_menus` est un objet qui décrit les éléments des menus personnalisés utilisés dans le mode d'interface "Themys" (et possiblement d'autres).
Cet objet définit 4 listes de chaînes de caractères : `file`, `edit`, `tools`, `help`. Chaque liste décrit les actions présentes dans chaque menu lorsque les menus personnalisés sont utilisés. Les actions non listées n'apparaîtront pas dans le menu.

`interfaces` est une liste d'objets décrivant chacun un mode d'interface. Chaque mode d'interface peut posséder les propriétés suivantes:
* `name` (chaîne de caractères): Le nom d'affichage du mode, utilisé comme label de la liste déroulante.
* `hidden_widgets` (liste de chaînes de caractères): Les noms des barres d'outil dockables (dock widgets) à cacher.
* `partial_toolbars` (liste d'objects): Barres d'outils à montrer partiellement. Chaque objet de la liste doit définir les propriétés suivantes:

 * `name` (chaîne de caractères) : le nom de la barre d'outils à montrer partiellement.
 * `show` (liste de chaînes de caractères): Les actions à afficher sur la barrre d'outils. Les actions non présentes dans la liste seront cachées.

* `use_custom_menus` (bool): Lorsque cette valeur est `true`, le mode d'affichage utilise les menus simplifiés décrits dans `custom_menus`. Si `false`, utiliser les menus avancés de Paraview.
* `sources` (liste de chaînes de caractères): Le nom des catégories à afficher dans le menu "Sources".
* `partial_sources` (liste d'objets): Catégories à afficher partiellement dans le menu "Sources". Suit la même logique que `partial_toolbars`.
* `filters` (liste de chaînes de caractères): Le nom des catégories à afficher dans le menu "Filtres".
* `partial_filters` (liste d'objets): Catégories à afficher partiellement dans le menu "Filters". Suit la même logique que `partial_toolbars`.

Toutes les propriétés sus-mentionnées sont optionnelles, à l'exception de `name` et `use_custom_menus`. Leur ordre dans chaque objet d'`interfaces` n'a pas d'importance.

Mise à jour des paramètres
--------------------------

Le changement de mode d'interface persiste entre les sessions d'utilisation de Themys. Le mode d'interface peut également être changé dans les paramètres de l'application. Lors de l'ajout d'un mode d'interface,
il est nécessaire de mettre à jour le fichier `ThemysSettings.xml` en ajoutant une entrée de l'`EnumerationDomain` de la propriété `InterfaceMode`. Les items de l'`EnumerationDomain` doivent suivre le même ordre que la liste `interfaces`.
