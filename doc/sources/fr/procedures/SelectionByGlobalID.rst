Documentation: `doc en <../../en/procedures/SelectionByGlobalID.html>`__



Définir une sélection suivant des identifiants globaux (IDs)
============================================================

Description
-----------

L'opération de sélection met en évidence un sous-ensemble d'éléments dans
un ensemble de données, généralement des cellules ou des points, qui peuvent
ensuite être extraits ou analysés. En particulier, la sélection avec des
identifiants globaux (IDs) utilise l'ID d'une cellule ou d'un point,
attribué de manière unique dans un ensemble de données donné.


Procédure avec l'interface graphique (GUI)
------------------------------------------

Pour cette opération, commencez par activer le mode avancé de Themys
(``Advanced Mode``) en cliquant sur le bouton correspondant,
illustré ci-dessous.

|image0|

1. Générer les identifiants globaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Avant de procéder à la sélection, il est nécessaire de définir des IDs
globaux s'ils ne sont pas déjà présents. Pour ce faire, commencez par
sélectionner l'ensemble de données dans le panneau ``Pipeline Browser``
en cliquant dessus. Ensuite, allez dans le menu ``Filters > Search...``.

|image1|

Cela ouvre une fenêtre contextuelle dans laquelle vous pouvez taper.
Commencez à taper le nom du filtre ``Generate Global Ids`` et appuyez
sur ``Enter`` lorsqu'il est surligné.

|image2|

Cliquez sur  ``Apply`` dans le panneau ``Properties`` pour exécuter le filtre.
Cela crée de nouveaux tableaux de données contenant des IDs uniques pour
les cellules et les points, nommés respectivement
**GlobalCellIds**  et **GlobalPointIds**.

|image3|

2. Créer une sélection
~~~~~~~~~~~~~~~~~~~~~~


Ensuite, ouvrez le panneau de création de sélection, appelé ``Find Data``,
en cliquant sur son icône. Par défaut, la touche ``V`` est également
définie comme raccourci clavier pour ouvrir ce panneau.

|image4|

Dans ce nouveau panneau, sélectionnez l'ensemble de données à partir
duquel créer une sélection dans la section ``Selection Criteria`` grâce à
la liste déroulante ``Data Producer``. Ensuite, choisissez si vous souhaitez
sélectionner des cellules ou des points en utilisant la liste déroulante
``Element Type``. Pour créer des sélections, n'importe quel tableau de
données peut être utilisé. Cependant, dans ce contexte, nous utiliserons
les tableaux d'IDs globaux, que vous pouvez sélectionner comme illustré ci-dessous.

|image5|

.. note::
  Il arrive que le code produisant la données fournisse un identifiant global,
  il alors possible de passer l'étape de génération, choissisez ensuite cette
  grandeur dans le critère de sélection.


La ligne située sous ``Element Type`` est une condition de sélection qui peut
être choisie parmi les types suivants :

-  ``<array> is <value>`` : Sélectionne les éléments pour lesquels
   ``<array> = <value>``.

-  ``<array> is in range <min> and <max>`` : Sélectionne les éléments
   pour lesquels ``<min> < <array> < <max>``.

-  ``<array> is one of <value1>, <value2>, <value3>, ...`` : Sélectionne
   les éléments pour lesquels la valeur de ``<array>`` est égale à une des
   valeurs dans la liste (séparé par des virgules).

-  ``<array> is >= value`` : Sélectionne les éléments pour lesquels
   ``<array> >= <value>``.

-  ``<array> is <= value`` : Sélectionne les éléments pour lesquels
   ``<array> <= <value>``.

-  ``<array> is min`` : Sélectionne les éléments avec la plus petite
   valeur de ``<array>``.

-  ``<array> is max`` : Sélectionne les éléments avec la plus grande
   valeur de ``<array>``.

-  ``<array> is NaN`` : Sélectionne les éléments pour lesquels la valeur
   de ``<array>`` est ``NaN`` (Not a Number).

-  ``<array> is <= mean`` : Sélectionne les éléments pour lesquels la
   valeur de ``<array>`` est plus petite ou égale à la valeur moyenne
   de ``<array>``.

-  ``<array> is >= mean`` : Sélectionne les éléments pour lesquels la
   valeur de ``<array>`` est plus grande ou égale à la valeur moyenne
   de ``<array>``.

-  ``<array> is mean <value>`` : Sélectionne les éléments pour lesquels
   ``(m - <value>) <= <array> <= (m + <value>)``, où ``m`` est la valeur
   moyenne de ``<array>``.

appuyer sur le bouton ``+`` à droite de la condition permet d'en ajouter
une nouvelle qui est combinée avec les autres conditions en utilisant
l'opérateur logique **ET**. En d'autres termes, un élement doit satisfaire
toutes les conditions pour être sélectionné.

Une fois que toutes les conditions sont définies, appuyez sur le bouton
``Find Data`` pour créer la sélection. les éléments selectionnés
apparaissent dans une liste dans la section ``Selected Data``, ainsi que
surlignés dans la fenêtre de visualisation.

|image6|

Pour supprimer la sélection actuelle, appuyez sur le bouton ``Clear Selection``
en haut de la fenêtre de visualisation, comme illustré ci-dessous.

|image7|

3. Etendre une sélection
~~~~~~~~~~~~~~~~~~~~~~~~

Si vous souhaitez étendre une sélection avec un nouvel ensemble de critères,
appuyez sur le bouton ``Freeze``, comme indiqué ci-dessous, afin de sauvegarder
la sélection actuelle. Les critères de sélection sont ensuite réinitialisés,
permettant de définir de nouvelles conditions.

|image8|

4. Extraire une sélection
~~~~~~~~~~~~~~~~~~~~~~~~~

Pour extraire la sélection actuelle et obtenir un sous-ensemble de la
base de donnée originale, appuyez sur le bouton ``Extract``,
comme illustré ci-dessous. Cela va ajouter un filtre d'extraction dans le
panneau ``Pipeline Browser`` à gauche. Appuyez sur ``Apply`` dans le panneau
``Properties`` pour obtenir la sélection extraite.

|image9|

Procédure avec le scripting Python
-----------------------------------

.. _generate-global-ids-1_fr:

1. Générer des identifiants globaux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'blow.vtk' according to the desired data set
   mySource = FindSource('blow.vtk')

   # Create a new 'GenerateGlobalIds' filter
   generator = GenerateGlobalIds(Input=mySource)

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   generatorDisplay = Show(generator, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   generatorDisplay.SetScalarBarVisibility(renderView, True)

.. _create-a-selection-1_fr:

2. Créer une sélection
~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Create a query selection
   # Choose the query type according to your needs
   # Combine several queries using '&'

   # <array> is <value>
   QuerySelect('(GlobalCellIds == 10)', 'CELL')

   # <array> is in range <min> and <max>
   QuerySelect('(GlobalCellIds > 15) & (GlobalCellIds < 20)', 'CELL')

   # <array> is one of <value1>, <value2>, <value3>, ...
   QuerySelect('(in1d(GlobalPointIds, [10, 20, 30]))', 'POINT')

   # <array> is >= value
   QuerySelect('(GlobalCellIds >= 241)', 'CELL')

   # <array> is <= value
   QuerySelect('(GlobalCellIds <= 244)', 'CELL')

   # <array> is min
   QuerySelect('(GlobalPointIds == min(GlobalPointIds))', 'POINT')

   # <array> is max
   QuerySelect('(GlobalPointIds == max(GlobalPointIds))', 'POINT')

   # <array> is NaN
   QuerySelect('(isnan(GlobalCellIds))', 'CELL')

   # <array> is <= mean
   QuerySelect('(GlobalPointIds <= mean(GlobalPointIds))', 'POINT')

   # <array> is >= mean
   QuerySelect('(GlobalPointIds >= mean(GlobalPointIds))', 'POINT')

   # <array> is mean <value>
   QuerySelect('(abs(GlobalCellIds - mean(GlobalCellIds)) <= 10)', 'CELL')

   # Clear selection if needed
   ClearSelection()

.. _expand-a-selection-1_fr:

3. Etendre une sélection
~~~~~~~~~~~~~~~~~~~~~~~~

L'expension via le bouton ``freeze`` n'est pas disponible dans ``paraview.simple``.

.. _extract-a-selection-1_fr:

4. Extraire une sélection
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Create a new 'ExtractSelection' filter
   extractor = ExtractSelection(Input=generator)

   # Show output data
   extractorDisplay = Show(extractor, renderView)

   # Hide original source data
   Hide(generator, renderView)

   # Show color bar
   extractorDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/07AdvancedMode.png
.. |image1| image:: ../../img/procedures/07FiltersMenu.png
.. |image2| image:: ../../img/procedures/07FiltersSearch.png
.. |image3| image:: ../../img/procedures/07GenerateGlobalIdsArray.png
.. |image4| image:: ../../img/procedures/07FindData.png
.. |image5| image:: ../../img/procedures/07FindDataInitial.png
.. |image6| image:: ../../img/procedures/07FindDataApply.png
.. |image7| image:: ../../img/procedures/07FindDataClear.png
.. |image8| image:: ../../img/procedures/07FindDataFreeze.png
.. |image9| image:: ../../img/procedures/07FindDataExtract.png
