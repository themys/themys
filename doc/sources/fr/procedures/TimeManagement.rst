Documentation: `doc en <../../en/procedures/TimeManagement.html>`__


Gestion du temps
================

Disclaimer
----------

Cette documentation est une traduction du `blog écrit par  Nicolas Vuaille, Sonia Ayme and Francois Mazen <https://www.kitware.com/a-new-time-manager-is-landing-in-paraview/>`__


Introduction
------------
Dans ParaView, vous pouvez charger un ensemble de données temporelles
et naviguer à travers ses pas de temps.

Vous pouvez également créer une scène animée où les paramètres d'analyse
et de visualisation peuvent changer à chaque pas de temps. Ces
fonctionnalités sont plus qu'utiles pour comprendre vos données
et communiquer vos résultats !

Et, comme toujours dans ParaView, nous voulons vous offrir un contrôle
précis de ce qui se passe grâce à une interface dédiée. Les contrôles
temporels étaient auparavant répartis entre le panneau
``Animation View`` et le panneau ``Time Inspector``. Comme ces deux interfaces
étaient similaires et redondantes, nous les avons récemment supprimées en
faveur du tout nouveau panneau ``Time Manager``.


Inspecter les temps
-------------------

Les pistes sont organisées en deux sections. La première, intitulée **Time**,
affiche des informations sur les données temporelles présentes dans
le pipeline. Chaque ligne correspond à une source temporelle du pipeline
et à ses pas de temps associés.

|image0|

.. raw:: html

    <video width="640" height="360" controls>
      <source src="NavigateTimesteps.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>

Utilisez le pour naviguer dans des pipelines temporelles complexes!

.. raw:: html

    <video width="640" height="360" controls>
      <source src="ComplexTime.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>


Propriétés d'animation et camera
--------------------------------

Dans la deuxième partie se trouvent les pistes d'animation, intitulées
**Animations**. Créez des pistes d'animation pour interpoler les propriétés
du pipeline de visualisation à chaque pas de temps. Augmentez le nombre
d'images pour obtenir une transition plus fluide.

|image1|

.. raw:: html

    <video width="640" height="360" controls>
      <source src="PropertyAnim-1.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>

Vous pouvez aussi configurer une trajectiore de camera autour de votre donnée!

.. raw:: html

    <video width="640" height="360" controls>
      <source src="AnimationShort.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>


.. |image0| image:: ../../img/procedures/TimeManagerSequence.png
.. |image1| image:: ../../img/procedures/TimeManagerAnimations.png
