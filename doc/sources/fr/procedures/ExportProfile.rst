Documentation: `doc en <../../en/procedures/ExportProfile.html>`__



Exporter un profil dans un fichier CSV
==============================

Description
-----------

Générer un profil de données dans Themys est utile, mais on peut vouloir
l'exporter et le visualiser en dehors de Themys. Cela est possible en
utilisant le format CSV.

Procédure avec l'interface graphique (GUI)
-----------------------

-  Chargez vos données dans Themys et affichez-les.

-  Créez le profil en utilisant le filtre ``Plot Over Line``.
   -  Pour configurer la position de votre profil, modifiez les paramètres
   dans la section ``Properties``. Une explication plus détaillée sur
   la création de profil est disponible sur
   `cette page <PlotOverLineCustom.md>`__.


-  Cliquez sur ``Apply``. Lors de la première application, Themys créera
   automatiquement une ``Line Chart View`` pour visualiser le profil résultant.
   Vous pouvez modifier les paramètres du filtre jusqu'à ce que vous soyez
   satisfait du résultat de votre profil.

-  Assurez-vous que le mode avancé de Themys soit activé.

-  Une fois satisfait de votre profil, vous pouvez maintenant l'exporter.
   Il y a deux méthodes pour cela : en utilisant ``File - Save Data`` ou en
   utilisant la ``spreadsheet view``.

Avec *File - Save Data*
~~~~~~~~~~~~~~~~~~~~~~~~

-  Avec le filtre ``Plot Over Line`` sélectionné, allez dans ``File - Save Data``.

-  Dans la première boîte de dialogue, Themys vous demandera un emplacement
   pour enregistrer le fichier ainsi que le type de fichier que vous souhaitez
   écrire. Choisissez **Comma or  Tab Delimited Files** et cliquez sur ``Ok``.

-  Une deuxième boîte de dialogue s'ouvrira où vous pourrez définir certains paramètres :

   -  ``Choose array to Write`` : vous permet de choisir quelles données
      exporter dans votre profil CSV. Notez que ``arc_length`` doit toujours
      être exporté car il fait référence à la longueur de votre profil et est
      utilisé comme axe X.
   -  ``Précision`` : nombre de chiffres pour les nombres à virgule flottante.
   -  ``Field Association`` : doit toujours être **Point Data**.

-  Notez que toutes les coordonnées des points 3D du profil seront toujours
   exportées en utilisant cette méthode.


Avec la *spreadsheet view*
~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Dans Themys, ouvrez un nouvel onglet de visualisation en cliquant sur
   l'onglet ``+`` à côté de votre **layout** actuel.

-  Choisissez ``spreadsheet view``.

-  En haut de la vue, assurez-vous que l'affichage de votre filtre
   ``Plot Over Line`` est sélectionné. Vous devriez maintenant pouvoir
   voir votre profil dans la vue.

-  Choisissez quelle grandeur afficher et exporter en utilisant le bouton
   ``Toggle column visibility`` en haut de la vue.

-  À côté du bouton ``Toggle column visibility``, cliquez sur le bouton
   ``Export Spreadsheet``.

-  Dans la première boîte de dialogue, Themys vous demandera un emplacement
   pour enregistrer le fichier.

-  Une deuxième boîte de dialogue s'ouvrira où vous pourrez définir
   certains paramètres :

   -  ``Real Number Notation`` : comment écrire les nombres.
      Trois modes sont disponibles :

      -  ``Mixed`` : choisira automatiquement l'affichage le plus court.
      -  ``Fixed`` : utilise un nombre fixe de chiffres pour les nombres
         à virgule flottante.
      - ``Scientific`` : utilise la notation scientifique.
   -  ``Real Number Precision`` : nombre de chiffres pour les nombres à
      virgule flottante.


|image0|

Procédure avec le scripting Python
--------------------------------

.. code:: py

   from paraview.simple import *

   # Create your data. This can ever be by reading a file or creating a source.
   # Here for this example we create a Wavelet source
   data = Wavelet()

   # create a new 'Plot Over Line' filter that takes your data as input.
   profileFilter = PlotOverLine(Input=data)
   # you can customize the resolution of your profile, as well as its starting and ending point for example.
   profileFilter.Resolution = 100
   profileFilter.Point1 = [-5.0, -5.0, -10.0]
   profileFilter.Point2 = [5.0, 10.0, 10.0]

   # create a new 'SpreadSheet View' so we can export our data from there
   spreadSheet = CreateView('SpreadSheetView')

   # show the profile in the spreadsheet view
   Show(profileFilter, spreadSheet, 'SpreadSheetRepresentation')

   # hide unwanted data.
   # Below is an example of data we may not want to export when doing the profile of a wavelet
   spreadSheet.HiddenColumnLabels = ['Block Number', 'Point ID', 'Points', 'Points_Magnitude', 'vtkValidPointMask']

   # export the profile.
   # Note the parameters that allows us to custom floating point numbers writing.
   ExportView('/home/user/profile.csv', view=spreadSheet, RealNumberNotation='Fixed', RealNumberPrecision=7)

.. |image0| image:: ../../img/procedures/SpreadsheetViewButtons.png
