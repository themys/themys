Documentation: `doc en <../../en/procedures/Histogram.html>`__



Créer un histogramme
==================

Description
-----------

Un histogramme est un type de graphique à barres qui, en séparant la plage
du tableau de données en intervalles de taille égale, montre le nombre de
 cellules ou de points appartenant à chaque intervalle.

Procédure avec l'interface graphique (GUI)
-----------------------

Commencez par sélectionner l'ensemble de données à partir duquel créer un
histogramme dans le panneau ``Pipeline Browser`` en cliquant dessus. Ensuite,
activez le ``Advanced Mode`` en cliquant sur le bouton correspondant en forme
de roue, comme indiqué ci-dessous. Dans le menu, allez dans ``Filtres >
Display > Charts > Histogram``.

|image0|

Dans le panneau ``Properties``, choisissez le tableau de données à partir
duquel créer un histogramme avec la liste déroulante ``Select Input Array``.
Sélectionnez le nombre d'intervalles pour l'histogramme à l'aide du curseur
``Bin Count``, ou en entrant directement une valeur. La case ``Component``
est utilisée pour choisir un composant vectoriel lorsque le tableau de
données est vectoriel. Cliquez sur ``Apply`` pour produire l'histogramme,
qui apparaît dans une nouvelle ``Chart View``.


|image1|

Procédure avec le scripting Python
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set from which to produce a histogram
   mySource = FindSource('Wavelet1')

   # Create a new 'Histogram' filter
   histogram = Histogram(Input=mySource)

   # If needed, change scalar data array
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'RTData' with a valid scalar array
   histogram.SelectInputArray = ['POINTS', 'RTData']

   # If needed, change the number of bins
   histogram.BinCount = 15

   # Create a bar chart view
   barChartView = CreateView('XYBarChartView')

   # Show output data
   histogramDisplay = Show(histogram, barChartView)

   # Get layout
   layout = GetLayout()

   # Add bar chart view to the layout
   AssignViewToLayout(barChartView, layout)

   # If needed, create a new layout containing only the histogram
   layout2 = CreateLayout(name='Histogram')

   # Create a new 'Bar Chart View'
   barChartView2 = CreateView('XYBarChartView')

   # Show output data
   histogramDisplay2 = Show(histogram, barChartView2)

   # Add bar chart view to new layout
   AssignViewToLayout(barChartView2, layout2)

.. |image0| image:: ../../img/procedures/13HistogramFilter.png
.. |image1| image:: ../../img/procedures/13HistogramResult.png
