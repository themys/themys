Documentation: `doc en <../../en/procedures/clipping.html>`__



Découpage
=========

Description
-----------

L'opération de découpage supprime la partie d'un ensemble de données située d'un côté d'une géométrie
de découpage donnée.

Via l'interface graphique
-------------------------

L'application du filtre **Clip** nécessite de sélectionner une des
instances d'objets/sources du navigateur du pipeline (**Pipeline Browser**)
en cliquant dessus avant de créer une instance de ce filtre **Clip**.
Cette création peut se faire en cliquant sur un bouton.

|image0|

.. note::
   Cette création d'instance de filtre peut aussi se faire en tapant **CTRL+ESP**
   (équivalent à passer par le menu déroulant *Filters > Search*)
   ce qui aura pour effet d'ouvrir une fenêtre contextuelle dans laquelle
   vous pouvez commencer à taper le nom du filtre recherché (ici, **clip**).
   Ici, il se trouve que les deux premiers caractères du nom de ce filtre
   sont discriminants et vont mettre en surbrillance ce nom dans la liste
   des filtres disponibles. Il suffit donc de saisir **cli+ENTER**.

La géométrie de découpage par défaut est le plan qui apparaît en rouge dans
la fenêtre de visualisation.

Vous pouvez interagir directement avec ce plan de différentes manières
à travers ces objets en rouge :

*  en cliquant sur le plan lui-même pour le déplacer le long de son axe ;

*  en cliquant sur l'axe pour ajuster son orientation ;

*  en cliquant sur la boule au centre de l'axe pour déplacer l'origine du plan.

|image1|

Pour définir ces paramètres avec plus de précision, les coordonnées de
la position et de la normale du plan de détourage peuvent également être
spécifiées dans le panneau **Properties** (propriétés).

Pour valider ces choix, il ne faut pas oublier de cliquer sur
**Apply** (appliquer) pour exécuter le filtre.

Pour permettre une interaction plus facile avec l'ensemble de données,
vous pouvez décocher l'option **Show Plane** (afficher le plan) afin de
masquer le plan de détourage comme indiqué ci-dessous. Il suffit de
cocher cette option pour retrouver les interacteurs du plan.

|image2|

Pour inverser le résultat de l'opération de découpage et obtenir
la partie du jeu de données qui correspond à l'autre côté du plan
de découpage, cochez/décochez la case **Invert** (inverser).

|image3|

Si nécessaire, les filtres de découpe peuvent être enchaînés
pour découper un ensemble de données en utilisant plusieurs plans.

|image4|

Outre le plan, d'autres géométries sont disponibles, notamment
la boîte, le cylindre et la sphère.

Un mode **Scalar** supplémentaire coupe les ensembles de données
en fonction d'un tableau de données scalaire sélectionné,
similaire à une opération de seuillage mais avec un découpage de cellule.
Précisons que ce découpage a lieu pour un champ de valeurs aux noeuds
mais correspond à un threadshold si le champ de valeurs est aux cellules.

|image5|

|image6|

|image7|

|image8|

Via le scripting Python
-----------------------

1. Plane
~~~~~~~~

Script d'exemple pour un découpage par un plan (plane).

.. code:: py

   # Find the source data set with its name
   # Replace 'can.ex2' according to the data set to clip
   mySource = FindSource('can.ex2')

   # Create a new 'Clip' filter
   clip = Clip(Input=mySource)

   # If needed, change the origin and normal of the clipping plane
   clip.ClipType.Origin = [0.0, 0.0, 0.0]
   clip.ClipType.Normal = [0.0, 1.0, 0.0]

   # Optionally invert the output data
   clip.Invert = 0

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   clipDisplay = Show(clip, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   clipDisplay.SetScalarBarVisibility(renderView, True)

2. Box
~~~~~~

Script d'exemple pour un découpage par un boîte (box).

.. code:: py

   # Change clipping geometry to a box
   # Note that rotation is expressed in degrees
   clip.ClipType = 'Box'
   clip.ClipType.Position = [0.0, 0.0, 1.0]
   clip.ClipType.Rotation = [0.0, 45.0, 0.0]
   clip.ClipType.Length = [5.0, 5.0, 10.0]

3. Cylinder
~~~~~~~~~~~

Script d'exemple pour un découpage par un cylindre (cylinder).

.. code:: py

   # Change clipping geometry to a cylinder
   clip.ClipType = 'Cylinder'
   clip.ClipType.Center = [0.0, 0.0, -3.0]
   clip.ClipType.Axis = [0.5, 1.0, 0.5]
   clip.ClipType.Radius = 3.0

4. Sphere
~~~~~~~~~

Script d'exemple pour un découpage par une sphère (sphere).

.. code:: py

   # Change clipping geometry to a sphere
   clip.ClipType = 'Sphere'
   clip.ClipType.Center = [1.0, 0.0, 0.0]
   clip.ClipType.Radius = 5.0

5. Scalar
~~~~~~~~~

Script d'exemple pour un découpage par un champ de valeurs (scalar).

.. code:: py

   # Change clip type to scalar mode
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'ids' with a valid scalar array
   clip.ClipType = 'Scalar'
   clip.Scalars = ['POINTS', 'ids']
   clip.Value = 1234

.. |image0| image:: ../../img/procedures/02ClipFilterSelection.png
.. |image1| image:: ../../img/procedures/02ClipFilterAppearance.png
.. |image2| image:: ../../img/procedures/02ClipFilterHidePlane.png
.. |image3| image:: ../../img/procedures/02ClipFilterInvert.png
.. |image4| image:: ../../img/procedures/02ClipFilterMultiple.png
.. |image5| image:: ../../img/procedures/02ClipBox.png
.. |image6| image:: ../../img/procedures/02ClipCylinder.png
.. |image7| image:: ../../img/procedures/02ClipSphere.png
.. |image8| image:: ../../img/procedures/02ClipScalar.png
