Documentation: `doc en <../../en/procedures/Transform.html>`__



Filtre *Transform*
==================

Description
-----------

Le filtre ``Transform`` permet d’appliquer certains types d’opérations
géométriques :

- translation (``Translate``)
- rotation (``Rotate``)
- redimensionnement (``Scale``)

Un seul traitement par type d’opération est possible et l’ordre d’application
est d’abord le redimensionnement (si activé), puis la rotation (si
activé) et enfin la translation (si activé).


Chaque application du filtre ``Transform`` réalise une copie du maillage,
selon de nouvelles coordonnées, avec un coût mémoire. C’est pourquoi, sur des
gros maillages, il est conseillé de ne pas multiplier l’usage de ce filtre.


Utilisation du filtre
---------------------

Si l’on souhaite effectuer une translation (``Translate``) puis un
redimensionnement (``Scale``), deux options sont possibles :

- la première, plus instinctive, propose d’appliquer deux filtres
  ``Transform`` avec chacun une transformation géométrique.
- la seconde, plus efficace mais plus difficile à mettre en place,
  consiste à appliquer les deux opérations dans le même filtre ``Transform``.


Veuillez noter que la première proposition est deux fois plus consommatrice
en mémoire que la seconde.

La deuxième est en général moins facile à mettre en place car il faut penser
à l'ordre d'application des opération au sein du filtre: ici la translation
étant appliquée après le redimensionnement, les valeurs associées
à la translation doivent prendre en compte cette première transformation.


Exemple
-------

Le but de cet exemple est de modifier le maillage donné par
le ficher ``SainteHelens.dem`` afin que l'origine soit à l'origine du repère
et que la largeur du maillage suivant l'axe X ait pour valeur 1.


|image0|
  A gauche le cas 1 avec l’application de deux filtres ``Transform``,
  A droite le cas 2 avec l’application d’un seul filtre ``Transform``.


Les valeurs des opérations à appliquer sont déduites en regardant le
panneau ``Information`` et sont les suivantes:
::

   Translate -557945; -5.1079915e6; -682
   Scale  0.000102249; 0.000102249; 1


Cas 1 avec deux filtres ``Transform``:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le premier filtre appliqué ici correspond à la translation et
doit être rempli comme indiqué ci-dessous.

|image1|

Le second filtre correspond au redimenssonnement et doit être rempli comme
indiqué ci-dessous.

|image2|


Cas 2 avec un filtre ``Transform``:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Comme indiqué plus haut, la translation est appliquée après le
redimensionnement et les valeurs associées à la translation doivent subir
cette transformation.

Au lieu de saisir comme précédemment une translation de
::

   Translate -557945; -5.1079915e6; -682

il faut lui appliquer le redimensionnement :

::

   Translate -57.0493; -522.287; -682

Ce qui donne les paramètres du filtre ``Transform`` ci-dessous.

|image3|


.. |image0| image:: ../../img/procedures/Transform.png
.. |image1| image:: ../../img/procedures/Transform_option1_a.png
.. |image2| image:: ../../img/procedures/Transform_option1_b.png
.. |image3| image:: ../../img/procedures/Transform_option2.png
