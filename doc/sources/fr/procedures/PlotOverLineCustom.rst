Documentation: `doc en <../../en/procedures/PlotOverLineCustom.html>`__



Extraction personnalisée de points sur lignes
=============================================

Description
-----------

À partir d’un maillage, il est possible d’extraire une information
portée par un ensemble de polylines (équivalent d'une ligne brisée) qui
traverseraient ce maillage. C'est ce que propose de faire le ``Plot Over Line
Custom`` (la version non custom ne s'appliquant qu'à une ligne droite).


Ce filtre produit des courbes avec en abscisse la position linéaire sur les
polylines depuis leur origine et en ordonnée la valeur pour un des champs
de valeurs. La position linéaire, nommée **arc_length**, correspond à la
distance couverte en parcourant la polyline depuis le début.


Plusieurs options sont proposées :

- ``Sample Uniformly`` : extrait des points répartis uniformément sur
  chacune des polylines. La propriété ``Resolution`` permet de faire varier
  le nombre de point par polylines.
- ``Sample At Cell Boundaries`` : extrait des points à chaque extremité
  des cellules. Le résultat exprime clairement la géométrie du maillage.
- ``Sample At Segment Centers`` : extrait les centres des segments qui
  traversent des cellules sur chacune des polylines.


Les résultats sont triés selon la position linéaire croissante par polyline.


Création du filtre PlotOverLineCustom
------------------------------------

Le filtre ``Plot Over Line Custom`` prend en première entrée le maillage sur
lequel on veut l'appliquer.

Une fenêtre de dialoge s’ouvre alors et demande la valeur de chacun
des deux ports :

- le premier nommé ``Input`` est déjà renseigné, c’est le maillage actif ;
- le second nommé ``Source`` décrit le multi-polylines à apliquer ;


Des facilités sont proposées pour construire automatiquement des
multi-polylines de type :

- paon ou hérisson : ``SpheresLineSource`` ;
- faisceau : ``BoxLinesSource`` ;
- balayette : ``CylinderLineSource``.

Il est bien sur possible de créer sa propre source multi-polylines.


Création de source multi-polylines personnalisée
----------------------------------

La première étape est de créer une polyline avec la source ``PolyLines``.
Il est possible d'ajouter des points manuellement dans le tableau ou bien
modifier de façon intéractive la polylines.

L'étape suivante consiste à grouper puis fusionner ces polylines. Pour cela,
sélectionner les polylines et appliquer le filtre ``Group DataSets``.
Il faut ensuite appliquer le filtre ``Merge Blocks`` qui va définir un
nouveau maillage non structuré où chaque bloc est une cellule de type polyline.

Ce résultat peut servir de source au ``PlotOverLineCustom``.
