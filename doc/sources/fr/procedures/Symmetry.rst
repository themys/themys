Documentation: `doc en <../../en/procedures/Symmetry.html>`__



Appliquer une symétrie
==================

Description
-----------

Appliquer un filtre de symétrie sur un ensemble de données produit une copie
symétrique de celui-ci par rapport à un plan donné. Cette opération est
utile, par exemple, pour reconstruire un maillage basé sur la moitié ou le
quart d'une structure.

Procédure avec l'interface graphique (GUI)
-----------------------

Commencez par sélectionner l'ensemble de données, dont la réflexion est souhaitée, dans le panneau
``Pipeline Browser`` en cliquant dessus. Choisissez l'un des filtres de
symétrie dans la barre d'outils des filtres, nommés ``ReflectX``, ``ReflectY``
et ``ReflectZ``, en fonction de l'axe orthogonal au plan de symétrie.

|image0|

Cliquez sur le bouton ``Apply`` dans le panneau ``Properties`` pour exécuter
le filtre. Le résultat sera l'ensemble de données réfléchi par rapport au
plan de symétrie sélectionné.

|image1|


Pour conserver l'ensemble de données d'entrée en plus de sa réflexion,
cochez la case ``Copy Input``. Pour accéder à cette option avancée, cliquez
sur le bouton ``Advanced Properties`` (roue dentée) illustré ci-dessous.


|image2|

Enfin, le plan peut être déplacé le long de son axe en ajustant la valeur
``Position`` dans le panneau ``Properties``, comme montré ci-dessous.

|image3|

Procédure avec le scripting Python
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'blow.vtk' according to the data set to reflect
   mySource = FindSource('blow.vtk')

   # Create a new 'ReflectX' filter
   # Likewise for 'ReflectY' and 'ReflectZ'
   reflectX = ReflectX(Input=mySource)

   # Optionally enable CopyInput
   reflectX.CopyInput = 1

   # Optionally modify the reflection plane position
   reflectX.Position = 5.0

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   reflectXDisplay = Show(reflectX, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   reflectXDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/01BeforeReflection.png
.. |image1| image:: ../../img/procedures/01ApplyReflection.png
.. |image2| image:: ../../img/procedures/01ReflectionCopyInput.png
.. |image3| image:: ../../img/procedures/01ReflectionPosition.png
