Execute a Python script
=======================

Description
-----------

Python scripts can be executed in different ways in Themys: once by
loading it (in the menu or the Python shell), through a saved macro
button, or even without using the graphical user interface.

For illustration purposes, we will use the following simple Python
script that creates a sphere and changes the representation to show mesh
edges.

.. code:: py

   # Import the simple module from paraview
   from paraview.simple import *

   # Create a simple sphere
   sphere = Sphere()

   # Get active view
   view = GetActiveViewOrCreate('RenderView')

   # Show sphere
   display = Show(sphere, view)

   # Change representation type
   display.SetRepresentationType('Surface With Edges')

For more details on Python scripting, see `this
page <../tutos/scripting/ScriptingTutorial.html>`__.

Procedure using the GUI
-----------------------

1. Executing once only (Menu)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To execute a Python script once, go into ``File > Load State...`` as
shown below.

|image0|

Next, go to your script location, select it and click ``OK`` to load and
execute it.

|image1|

The result of the execution will then be directly available for further
processing.

|image2|

2. Executing once only (Python Shell)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First open the Python shell view by going into ``View > Python Shell``
and ticking the box shown below.

|image3|

Under the Python shell, click on the ``Run Script`` button.

|image4|

In the new dialog, go to your script location, select it and click
``OK`` to execute it.

|image5|

3. Saving as a reusable macro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Instead, the script can be permanently saved as a macro for convenient
reuse.

For that, go into ``Tools > Import new macro...`` (if the
``Advanced Mode`` is activated, go into ``Macros > Import new macro...``
instead).

|image6|

|image7|

Next, move to your script location, select it and click ``OK`` to load
the script as a macro.

Note that the Python script is copied and saved separately from the
original script. Therefore, changes made to one will not impact the
other.

|image8|

The macro then appears in the toolbar as a button named after the script
name. Clicking on it will execute the script.

|image9|

Alternatively, the macro can be accessed in the menu via
``Tools > YourScriptName``.

|image10|

An existing macro can be edited in
``Tools > Edit Macro > YourScriptName``, which opens a script editor
inside Themys.

|image11|

|image12|

Lastly, a macro can be deleted via
``Tools > Delete Macro > YourScriptName``.

Procedure without the GUI
-------------------------

Sometimes you may wish to execute a script without opening the
application, for instance to generate images. To illustrate this, we add
a command to save a screenshot of the render view to the script above.

.. code:: py

   # Optionally save a screenshot of the render view
   SaveScreenshot('/path/to/screenshot/Sphere.png', view, ImageResolution=[640, 360])

Python scripts can be executed either with the ``pvpython`` or the
``pvbatch`` executable. The main difference between them is that the
second can be run in distributed mode if built with MPI.

To proceed with the execution, open a terminal/command prompt and enter
the following commands.

::

   > cd path/to/Themys/bin
   > pvpython /path/to/Python/script/YourScriptName.py

or, for a distributed execution (the number 16 below is chosen
arbitrarily),

::

   > cd path/to/Themys/bin
   > mpirun -np 16 pvbatch /path/to/Python/script/YourScriptName.py

The script will then run and produce a screenshot of the view that would
appear in Themys.

|image13|

.. |image0| image:: 14LoadState.png
.. |image1| image:: 14LoadScript.png
.. |image2| image:: 14AfterLoad.png
.. |image3| image:: 14PythonShellView.png
.. |image4| image:: 14RunScript.png
.. |image5| image:: 14LoadScript.png
.. |image6| image:: 14ImportMacroMenu.png
.. |image7| image:: 14ImportMacroMenuAdvanced.png
.. |image8| image:: 14LoadMacroScript.png
.. |image9| image:: 14MacroInToolbar.png
.. |image10| image:: 14MacroInMenu.png
.. |image11| image:: 14EditMacro.png
.. |image12| image:: 14ScriptEditor.png
.. |image13| image:: 14SavedScreenshot.png
