ParaView Python Scripting Scenarios and documentation
=====================================================

-  `Introduction <#introduction>`__
-  `Creating a new constant value on points or cells data
   array <#creating-a-new-constant-value-on-points-or-cells-data-array>`__

   -  `Feature <#feature>`__
   -  `GUI <#gui>`__
   -  `Python Scripting <#python-scripting>`__
   -  `HyperTreeGrid status <#hypertreegrid-status>`__

-  `Creating a new growing value on point or cell data
   arrays <#creating-a-new-growing-value-on-point-or-cell-data-arrays>`__

   -  `Feature <#feature-1>`__
   -  `GUI <#gui-1>`__
   -  `Python Scripting <#python-scripting-1>`__
   -  `HyperTreeGrid Status <#hypertreegrid-status-1>`__

-  `Creating a new value computed from input point or cell data
   arrays <#creating-a-new-value-computed-from-input-point-or-cell-data-arrays>`__

   -  `Feature <#feature-2>`__
   -  `GUI <#gui-2>`__
   -  `Python Scripting <#python-scripting-2>`__
   -  `HyperTreeGrid status <#hypertreegrid-status-2>`__

-  `Computing a gradient <#computing-a-gradient>`__

   -  `Feature <#feature-3>`__
   -  `GUI <#gui-3>`__
   -  `Python Scripting <#python-scripting-3>`__
   -  `HyperTreeGrid status <#hypertreegrid-status-3>`__

-  `Computing custom normals on a
   dataset <#computing-custom-normals-on-a-dataset>`__

   -  `Feature <#feature-4>`__
   -  `GUI <#gui-4>`__
   -  `Python Scripting <#python-scripting-4>`__
   -  `HyperTreeGrid status <#hypertreegrid-status-4>`__
   -  `Related features <#related-features>`__

-  `Transforming point data into cell data and vice
   versa <#transforming-point-data-into-cell-data-and-vice-versa>`__

   -  `Feature <#feature-5>`__
   -  `GUI <#gui-5>`__
   -  `Python Scripting <#python-scripting-5>`__
   -  `HyperTreeGrid status <#hypertreegrid-status-5>`__

-  `Comparing mesh with identical
   topologies <#comparing-mesh-with-identical-topologies>`__

   -  `Feature <#feature-6>`__
   -  `GUI <#gui-6>`__
   -  `Python Scripting <#python-scripting-6>`__
   -  `HyperTreeGrid status <#hypertreegrid-status-6>`__

-  `Using an external Python
   module <#using-an-external-python-module>`__

   -  `Feature <#feature-7>`__
   -  `GUI <#gui-7>`__
   -  `Details <#details>`__

Introduction
------------

This document contains documentation around specific scenarios within
ParaView and their pvpython scripting equivalents. The scenarios were
done using the datasets created in the context of the task 1.1 of this
project. The python scripts assume that the data is already loaded, and
that the computation should happen on the active source of the pipeline.

Creating a new constant value on points or cells data array
-----------------------------------------------------------

Feature
~~~~~~~

This sheet explains how to add a constant value to the point data or
cell data arrays of a dataset. There are multiple ways to achieve this,
but we will show the most simple way to.

GUI
~~~

-  Open a new ParaView session
-  Open/Create your dataset
-  Filters -> ``Calculator``
-  Choose either point data or cell data
-  In the text edit, write the constant value needed
-  Apply

Python Scripting
~~~~~~~~~~~~~~~~

Here is the script python to use to achieve the same result:

.. code:: python

   """
   ParaView script to add a constant value as Point/Cell Data on a mesh\
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # Use the active pipeline source
   dataSet = GetActiveSource()

   # create a new 'Programmable Filter'
   programmableFilter1 = ProgrammableFilter(Input=dataSet)
   programmableFilter1.Script ="""
   start = 0
   # handle multiblock structure: iterate over blocks
   for mesh in output:
     # create a numpy array for the current block
     grow = numpy.arange(start, start + mesh.GetNumberOfPoints())
     mesh.PointData.append(grow, "Growing")
     # comment following lin to increment only per block
     start += mesh.GetNumberOfPoints()"""

   UpdatePipeline()

HyperTreeGrid status
~~~~~~~~~~~~~~~~~~~~

The Calculator filter does not support HyperTreeGrid for now.

Creating a new growing value on point or cell data arrays
---------------------------------------------------------

.. _feature-1:

Feature
~~~~~~~

This sheet explains how to add a linearly growing value to the point
data or cell data arrays of a dataset. There are multiple ways to
achieve this, but we will show the most generic way to. A programmable
filter will allow you to generate data in any way possible.

.. _gui-1:

GUI
~~~

-  Open a new ParaView session

-  Open / Create your dataset

-  Filters -> ``Programmable Filter``

-  Use this script:

   .. code:: python

      start = 0
      for mesh in output:
        # create a numpy array for the current block
        grow = numpy.arange(start, start + mesh.GetNumberOfPoints())
        mesh.PointData.append(grow, "Growing")
        # comment following lin to increment only per block
        start += mesh.GetNumberOfPoints()

-  Apply

.. _python-scripting-1:

Python Scripting
~~~~~~~~~~~~~~~~

Here is the script python to use to achieve the same result:

.. code:: python

   """
   ParaView script to generate a varying value as Point Data on a mesh
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # Use the active pipeline source
   dataSet = GetActiveSource()

   # create a new 'Calculator'
   calculator1 = Calculator(Input=dataSet)

   # By default, array is added on 'Point Data'
   # Uncomment the following line to switch to Cell Data value
   # calculator1.AttributeType = 'Cell Data'

   # Name your array
   calculator1.ResultArrayName = 'Constant'
   # Set the constant
   calculator1.Function = '42'

   UpdatePipeline()

.. _hypertreegrid-status-1:

HyperTreeGrid Status
~~~~~~~~~~~~~~~~~~~~

The Programmable Filtre supports HyperTreeGrid data. But the numpy
wrapping does not.

Here is the programable filter script to use with HyperTreeGrid:

.. code:: python

   start = 0
   output.ShallowCopy(inputs[0].VTKObject)
   for mesh in output:
       # create a numpy array for the current block
       grow = numpy.arange(start, start + mesh.GetNumberOfVertices())
       arr = dsa.VTKArray(grow)
       dataArr = dsa.numpyTovtkDataArray(arr, "Result")
       mesh.GetCellData().AddArray(dataArr)
       # comment following lin to increment only per block
       # start += mesh.GetNumberOfPoints()

Creating a new value computed from input point or cell data arrays
------------------------------------------------------------------

.. _feature-2:

Feature
~~~~~~~

This sheet explains how to add values to the point data or cell data
arrays of a dataset, based on existing values. There are multiple ways
to achieve this, but we will show the most simple way to.

.. _gui-2:

GUI
~~~

-  Open a new ParaView session
-  Open / Create your dataset
-  Filters -> ``Calculator``
-  Choose either point data or cell data
-  In the text edit, write the expression needed,
   e.g. ``sin(ScalarData)``

   -  Available variable name (i.e. existing arrays) can be accessed in
      either “Scalars” list or “Vectors” list

-  Apply

.. _python-scripting-2:

Python Scripting
~~~~~~~~~~~~~~~~

.. code:: python

   """
   ParaView script to add an array computed from an existing one.
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # Use the active pipeline source
   dataSet = GetActiveSource()

   # create a new 'Calculator'
   calculator1 = Calculator(Input=dataSet)

   # Set an expression.
   # Considering than "Scalar Data" is an array from the input.
   calculator1.Function = 'sin(ScalarData)'

   UpdatePipeline()

.. _hypertreegrid-status-2:

HyperTreeGrid status
~~~~~~~~~~~~~~~~~~~~

The Calculator filter does not support HyperTreeGrid for now.

Computing a gradient
--------------------

.. _feature-3:

Feature
~~~~~~~

This sheet explains how to compute the gradient of an existing variable.

.. _gui-3:

GUI
~~~

-  Open a new ParaView session
-  Open / Create your dataset
-  Filters -> ``Gradient``
-  Apply

.. _python-scripting-3:

Python Scripting
~~~~~~~~~~~~~~~~

.. code:: python

   """
   ParaView script to compute a gradient
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # Use the active pipeline source
   ds = GetActiveSource()

   # create a new 'Gradient'
   gradient1 = Gradient(Input=ds)
   # specify the input array
   gradient1.ScalarArray = ['POINTS', 'ScalarData']

   UpdatePipeline()

.. _hypertreegrid-status-3:

HyperTreeGrid status
~~~~~~~~~~~~~~~~~~~~

The Gradient filter does not support HyperTreeGrids for now.

Computing custom normals on a dataset
-------------------------------------

.. _feature-4:

Feature
~~~~~~~

This sheet explains how to compute custom normals for a specific dataset
using the calculator. Since a scalar array does not make sense to define
normals, one must take care of creating an array of 3 components.

.. _gui-4:

GUI
~~~

-  Open a new ParaView session
-  Open / Create your dataset
-  Filters -> ``Calculator``
-  Fill the formula with how you want to compute the normal
-  Check the box ``Result Normals``
-  Apply

.. _python-scripting-4:

Python Scripting
~~~~~~~~~~~~~~~~

.. code:: python

   """
   ParaView script to compute a custom normal
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # Use the active pipeline source
   dataset = GetActiveSource()

   # create a new 'Calculator'
   calculator = Calculator(Input=dataset)
   # specify the formula
   calculator.Function = "[...]"
   # specify that we want to use the output as normals
   calculator.ResultNormals = True

   UpdatePipeline()

.. _hypertreegrid-status-4:

HyperTreeGrid status
~~~~~~~~~~~~~~~~~~~~

The Calculator filter does not support HyperTreeGrid for now.

Related features
~~~~~~~~~~~~~~~~

-  To generate geometric normals automatically, see filter
   ``Generate Surface Normals``.
-  To set an existing data array as a normal array for display, go to
   section ``Display``->\ ``Lighting`` of your active source and check
   the property ``Normal array``

Transforming point data into cell data and vice versa
-----------------------------------------------------

.. _feature-5:

Feature
~~~~~~~

This sheet explains how to transform point associated data into cell
associated data, and vice versa.

.. _gui-5:

GUI
~~~

-  Open a new ParaView session
-  Open / create your data
-  Filters -> ``Point Data To Cell Data`` (resp.
   ``Cell Data To Point Data``)
-  Apply

.. _python-scripting-5:

Python Scripting
~~~~~~~~~~~~~~~~

.. code:: python

   """
   ParaView script to convert point data arrays to cell data arrays.
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # Use the active pipeline source
   dataSet = GetActiveSource()

   # create a new 'Point Data to Point Data'
   pointDatatoCellData1 = PointDatatoCellData(Input=dataSet)

   # Set to 1 to process all arrays
   pointDatatoCellData1.ProcessAllArrays = 0
   # If `ProcessAllArrays` is 0, one should manually set arrays to process
   pointDatatoCellData1.PointDataArraytoprocess = ['ScalarData']

   # Pass original point data to output
   pointDatatoCellData1.PassPointData = 1

   UpdatePipeline()

.. _hypertreegrid-status-5:

HyperTreeGrid status
~~~~~~~~~~~~~~~~~~~~

The HyperTreeGrid data model does not support point data..

Comparing mesh with identical topologies
----------------------------------------

.. _feature-6:

Feature
~~~~~~~

This sheet explains how to compare two datasets with identical
topologies.

.. _gui-6:

GUI
~~~

-  Open a new ParaView session

-  Open / Create both datasets

-  Select both

-  Filters -> ``Python Calculator``

   ``inputs[1].PointData["ScalarData\"] - inputs[0].PointData["ScalarData_Twin"]``

-  Apply

.. _python-scripting-6:

Python Scripting
~~~~~~~~~~~~~~~~

.. code:: python

   """
   ParaView script to compare point (respectively cell) arrays from two datasets.
   They should have the same number of points (repectively cells)
   """
   #### import the simple module from the paraview
   from paraview.simple import *

   # expect two datasets selected in the pipeline
   dataset1 = active_objects.get_selected_sources()[0]
   dataset2 = active_objects.get_selected_sources()[1]

   # create a new 'Python Calculator'
   pythonCalculator1 = PythonCalculator(registrationName='PythonCalculator1', Input=[dataset1, dataset2])
   # Compare Point Data 'ScalarData' to 'ScalarData_Twin'
   pythonCalculator1.Expression = 'inputs[1].PointData["ScalarData"] - inputs[0].PointData["ScalarData_Twin"]'
   pythonCalculator1.ArrayName = 'DiffScalarData'

   UpdatePipeline()

.. _hypertreegrid-status-6:

HyperTreeGrid status
~~~~~~~~~~~~~~~~~~~~

The Python Calculator filter does not support HyperTreeGrid for now.

Using an external Python module
-------------------------------

.. _feature-7:

Feature
~~~~~~~

This sheet explains how to use an external python module in a
programmable filter.

.. _gui-7:

GUI
~~~

-  Open a new ParaView session
-  Open / create your data
-  Filters -> ``'Programmable filter'``
-  Script: ``import your.module``
-  Apply

Details
~~~~~~~

Programmable filters have access to the python environment ParaView was
built with. With the official binaries distrution, it is not recommended
to import other modules than those provided (such as numpy), due to
compatibility issues.

With a local build of ParaView, the whole python environment used for
the build is available. So it is possible to install specific modules
and access them.

When building a superbuild aimed to be distributed, be aware to ship the
extra modules you want to access with the created binaries. Then the end
users have the same restriction than using the official binaries : only
shiped modules should be used.
