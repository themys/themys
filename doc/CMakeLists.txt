include(create_links)

# Sphinx is required to build the html files
find_program(
  SPHINX_EXE sphinx-build
  DOC "Sphinx executable to transform markdown files into html ones" REQUIRED)
if(SPHINX_EXE)
  message(STATUS "Found Sphinx: ${SPHINX_EXE}")
endif()

# DOC_FILES_<lang> will be filled by each CMakeLists.txt found under source
# directory It holds the path to each of the document files (.rst but also .css
# and .html) that is used in the doc. Every modification of a file of this list
# will trigger the documentation build.
set(DOC_FILES_FR "")
set(DOC_FILES_EN "")
# IMG_FILES will be filled by each CMakeLists.txt found under this directory if
# the subdirectory holds images managed by sphinx. It will be used just to
# declare that Sphinx targets depends on them so that a modification of an image
# triggers the Sphinx build
set(IMG_FILES "")
set(RAW_VID_FILES "")

# DOCUMENTATION_BUILD_DIR is the path toward the directory in which the
# documentation is generated. As it contains generated stuff it must not be part
# of the source tree
set(DOCUMENTATION_BUILD_DIR ${CMAKE_BINARY_DIR}/documentation)
set(DOCUMENTATION_BUILD_DIR_FR ${CMAKE_BINARY_DIR}/documentation/fr)
set(DOCUMENTATION_BUILD_DIR_EN ${CMAKE_BINARY_DIR}/documentation/en)

include(download_paraview_doc_site)
add_subdirectory(sources)

create_links("fr" "en" "${DOC_FILES_FR}")
create_links("en" "fr" "${DOC_FILES_EN}")

foreach(lang "fr" "en")
  string(TOUPPER ${lang} LANG)
  set(DOCUMENTATION_BUILD_DIR_LANG "DOCUMENTATION_BUILD_DIR_${LANG}")
  set(DOC_FILES_LANG "DOC_FILES_${LANG}")
  # Declares the command that will execute sphinx. It depends on the
  # DOC_FILES_<lang> list so that every modification of a file in this list
  # triggers the sphinx execution
  add_custom_command(
    OUTPUT ${${DOCUMENTATION_BUILD_DIR_LANG}}/index.html
    COMMAND ${SPHINX_EXE} ${CMAKE_CURRENT_LIST_DIR}/sources/${lang}
            ${${DOCUMENTATION_BUILD_DIR_LANG}}
    DEPENDS ${${DOC_FILES_LANG}} ${IMG_FILES}
    COMMENT "Sphinx is running for ${lang} ... ")
  # This target depends on the ${DOCUMENTATION_BUILD_DIR_<lang>}/index.html
  # which itself depends on the ${DOC_FILES_<lang>} files so that if one of
  # those files changes, then the html file is generated again. This target is a
  # dependency of the ThemysDocumentation target
  add_custom_target(
    SphinxRun_${lang}
    DEPENDS ${${DOCUMENTATION_BUILD_DIR_LANG}}/index.html
            MirrorFiles_images_${lang} MirrorFiles_vid_${lang}
    COMMENT "Running Sphinx for ${lang}")
endforeach()

add_dependencies(themys SphinxRun_fr SphinxRun_en GetParaViewDocSite)
# Do not remove trailing '/' after ${DOCUMENTATION_BUILD_DIR} otherwise the doc
# will be installed into /path/to/install/documentation/documentation (twice
# documentation)
install(DIRECTORY ${DOCUMENTATION_BUILD_DIR}/
        DESTINATION ${DOCUMENTATION_INSTALL_DIR})
