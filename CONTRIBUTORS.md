The Themys project is an open-source project funded by
CEA, DAM, DIF, F-91297 Arpajon, France.

# Contributors

We thank all the contributors to this project which could not be there without them.


- Eloïse Billa <eloise.billa@gmail.com> (Maintainer)
- Guillaume Peillex <guillaume.peillex@cea.fr> (Maintainer)
- Maxime Stauffert <maxime.stauffert@cea.fr> (Maintainer)
- Jacques-Bernard Lekien <jacques-bernard.lekien@cea.fr>
- Mathieu Westphal <mathieu.westphal@kitware.com>
- Nicolas Vuaille <nicolas.vuaille@kitware.com>
- Thomas Galland <thomas.galland@kitware.com>
- Tiffany Chhim <tiffany.chhim@kitware.com>
- Timothée Chabat <timothee.chabat@kitware.com>
- Louis Gombert <louis.gombert@kitware.com>
