
![Themys](client/resources/themys_logo.jpeg){width=300}

# Themys

[Themys](README.md) is a custom application based on the open-source data analysis and visualization application [ParaView](https://www.paraview.org/)
from [KitWare](www.kitware.com). It is a **ParaView's branded application**.

Themys/ParaView users can quickly visualize and analyze data sets. It can be used to build visualization pipelines to analyze data using qualitative and quantitative techniques. The data exploration can be done interactively in 3D or programmatically using batch processing.

Themys fits the ParaView's Graphical User Interface (GUI) to the usage of the [CEA DAM](http://www-dam.cea.fr/) scientists.

Themys is born as an alternative to the historical data analysis and visualization tool [LOVE](www-hpc.cea.fr/fr/red/opensource.htm) created in 2000 (like ParaView) to respond to a growing context of massive exploitation of large volumes of data from numerical simulation. Both ParaView and LOVE are based on the [VTK toolkit](vtk.org).

Themys first development was made by KitWare under a contract with the [CEA DAM](http://www-dam.cea.fr/). It is now an open-source project, maintained by CEA DAM with the help of Kitware thanks to subcontracts.

CEA, DAM, DIF, F-91297 Arpajon, France

## Dependencies

 - ParaView at a given commit (the commit needed is indicated in the [CHANGELOG](CHANGELOG.md) for each themys version) - can be found at https://gitlab.kitware.com/paraview/paraview/

## Documentation

Themys documentation can be found [here](https://themys.gitlab.io/themys).

# Building Themys

Be aware that some Themys features need specific commits from the master branch of Paraview, and you should have a Paraview installation which is at least at the specific commit given in the [CHANGELOG](CHANGELOG.md) file.

There are three methods to build Themys.

## From scratch

1) Install ParaView by following the

   a. [official instructions](https://gitlab.kitware.com/paraview/paraview/-/blob/master/Documentation/dev/build.md)

   b. or if you are building on Ubuntu 24.04 (should be very similar with other ubuntu or debian based distributions) please follow the instructions below. They reflect those in the [Dockerfile](https://gitlab.com/themys/dockerimagefactory/-/blob/master/Dockerfile?ref_type=heads) of the [DockerImageFactory repository](https://gitlab.com/themys/dockerimagefactory).

    ~~~bash
    # Download pre-requisites
    sudo apt-get update
    sudo apt-get install -y --no-install-recommends git cmake ninja-build autoconf automake libtool \
    curl make g++ unzip apt-transport-https ca-certificates \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev libpugixml-dev libdouble-conversion-dev \
    liblz4-dev liblzma-dev libjpeg-dev libpng-dev libtiff-dev libfreetype-dev libjsoncpp-dev libeigen3-dev \
    python3-mpi4py libxml2-dev libhdf5-dev libnetcdf-dev nlohmann-json3-dev libsqlite3-dev sqlite3 libproj-dev \
    libtheora-dev libprotobuf-dev catch2 protobuf-compiler && update-ca-certificates


    # You home is chosen as the working directory. Feel free to change
    # this
    WORKDIR=/home/$USER
    cd ${WORKDIR}

    # Clone paraview repo and its submodules
    git clone https://gitlab.kitware.com/paraview/paraview.git
    cd paraview
    git submodule update --init --recursive

    # Configure paraview build system
    cd ${WORKDIR}
    mkdir paraview_build
    cd paraview_build
    cmake -GNinja \
        -DCMAKE_INSTALL_PREFIX=/opt/paraview_install \
        -DCMAKE_SKIP_INSTALL_RPATH=TRUE \
        -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
        -Dqt_xmlpatterns_executable=/usr/lib/qt5/bin/xmlpatterns \
        -DOpenGL_GL_PREFERENCE:STRING=LEGACY \
        -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=ON \
        -DPARAVIEW_BUILD_EDITION:STRING=CANONICAL \
        -DPARAVIEW_USE_QT:BOOL=ON \
        -DPARAVIEW_BUILD_WITH_EXTERNAL=ON \
        -DPARAVIEW_ENABLE_EXAMPLES:BOOL=OFF \
        -DPARAVIEW_QT_VERSION=5 \
        -DPARAVIEW_USE_PYTHON:BOOL=ON \
        -DPARAVIEW_USE_MPI:BOOL=ON \
        -DPARAVIEW_BUILD_SHARED_LIBS:BOOL=ON \
        -DPARAVIEW_USE_CUDA:BOOL=OFF \
        -DPARAVIEW_BUILD_WITH_KITS:BOOL=OFF \
        -DBUILD_TESTING:BOOL=OFF \
        -DVTK_OPENGL_HAS_OSMESA:BOOL=OFF \
        -DVTK_USE_X:BOOL=ON \
        -DVTK_MODULE_USE_EXTERNAL_VTK_ioss:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_exprtk:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_ogg:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_fmt:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_cgns:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_glew:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_gl2ps:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_libharu:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_utf8:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_verdict:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_token:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_fast_float:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_pegtl:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_cli11:BOOL=OFF \
        ../paraview


    # Build and install ParaView
    cmake --build .
    cmake --install .
    ~~~

   c. or if you are building on AlamaLinux 9.5. It's mostly the same as the ones for Ubuntu 24.04

    ~~~bash
    # Download pre-requisites
    sudo dnf update
    sudo dnf install git cmake ninja-build autoconf automake libtool curl make g++ unzip apt-transport-https certificates
    sudo dnf groupinstall "Development Tools"
    sudo dnf install python3.12-devel python3.12-numpy openssl-devel bzip2-devel libffi-devel qt5-devel libglvnd-devel \
    openmpi-devel openmpi pugixml-devel double-conversion-devel lz4-devel xz-devel libjpeg-turbo-devel openjpeg2-devel \
    libpng-devel libtiff-devel freetype-devel jsoncpp-devel eigen3-devel python3-mpi4py-openmpi libxml2-devel hdf5-devel \
    netcdf-devel pybind11-json-devel libsqlite3x-devel proj-devel libtheora-devel protobuf-devel catch2-devel cli11-devel \
    protobuf-compiler PEGTL-devel expat-devel
    # Set PATH
    export PATH=/usr/lib64/openmpi/bin:$PATH
    # Then follow Ubuntu 24.04 instructions (start at `WORKDIR=...`).
    # WARNING : For ParaView configuration, set the correct path for `qt_xmlpatterns_executable` :
    # `-Dqt_xmlpatterns_executable=/usr/bin/xmlpatterns-qt5`
    # Replace sudo apt-get install by sudo dnf install if needed
    ~~~

2) Install Themys and ThemysServerPlugins

    If `git-lfs` is not yet installed, please install it with your package manager.
    For `ubuntu` or `debian` based distributions:

    ~~~bash
    sudo apt-get install git-lfs
    ~~~

    a) Install both projects

    ~~~bash
    cd ${WORKDIR}

    git clone https://gitlab.com/themys/themys.git

    # Fetch Plugins submodule
    cd themys
    git submodule update --init --recursive

    cd ${WORKDIR}

    # Configure Themys build system
    mkdir themys_build
    cd themys_build
    cmake -DCMAKE_PREFIX_PATH=/opt/paraview/install/ -DBUILD_TESTING=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/themys_install -DBUILD_DOCUMENTATION=ON ../themys

    # Build and install Themys
    cmake --build .
    cmake --install .
    ~~~

    An alternative is to install only Themys, if the plugins are not necessary (for example to contribute to the Themys documentation or to the GUI customization)

    b) Install only Themys

    ~~~bash
    cd ${WORKDIR}

    git clone https://gitlab.com/themys/themys.git

    # Configure Themys build system
    mkdir themys_build
    cd themys_build
    cmake -DCMAKE_PREFIX_PATH=/opt/paraview/install/ -DBUILD_TESTING=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/themys_install -DBUILD_DOCUMENTATION=ON -DUSE_SERVERPLUGINS=OFF ../themys

    # Build and install Themys
    cmake --build .
    cmake --install .
    ~~~

3) Notes on `clang-tidy` and AlmaLinux

   If `ENABLE_CLANG_TIDY` is `ON` for a subproject, it asks for `clang-tidy-15` explicitly.
   On Alma the version is 17 or 19. So you need to edit `${build}/_deps/common_tools-src/cmake/ClangTidy.cmake` file,
   line 10, to remove `-15` from `find_program(clangtidy_path clang-tidy-15)`.
   The clang-tidy rules are versions dependent, so you may have more warnings than expected.

4) Notes on client-server

   Sometimes on AlmaLinux laptop the hostname is weird (`UNXXXXXXXX-UNAL.xxx.xxx.xxx.fr`). With this hostname it's impossible to
   run `ParaView` or `themys` in a client-server mode. If you don't want to change the hostname, replace it with localhost.
   Otherwise, change it following these instructions (It's allow by our IT crowd):
   - In *Parametres* window (right top menu), in *A Propos* submenu, change 'Nom de l'appareil" (just keep `UNXXXXXXXX`)
   - Edit `/etc/hostname` to put the same name as previous (you must be root)


## Using a docker container

A docker image contains already all the dependencies required to build themys (with or without ThemysServerPlugins).

For this to work [docker](https://docs.docker.com/desktop/) should be installed on the build machine.

Once docker is installed, the following commands retrieve the necessary image:

~~~bash
docker pull registry.gitlab.com/themys_private/themysdocker:latest
~~~

Once the image is retrieved, it is necessary to run it:

~~~bash
xhost +local:docker
docker run --network=host -e DISPLAY=:0  --device=/dev/dri:/dev/dri -it registry.gitlab.com/themys_private/themysdocker:latest
~~~

Once inside the running container, install Themys and ThemysServerPlugins:

~~~bash
WORKDIR="/opt"
cd ${WORKDIR}

git clone https://gitlab.com/themys/themys.git

# Fetch Plugins submodule
cd themys
git submodule update --init --recursive

cd ${WORKDIR}

# Configure Themys build system
mkdir themys_build
cd themys_build
cmake -DCMAKE_PREFIX_PATH=/opt/paraview/install/ -DBUILD_TESTING=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/themys_install -DBUILD_DOCUMENTATION=ON ../themys

# Build and install Themys
cmake --build .
cmake --install .
~~~

or just Themys:

~~~bash
cd ${WORKDIR}

git clone https://gitlab.com/themys/themys.git

# Configure Themys build system
mkdir themys_build
cd themys_build
cmake -DCMAKE_PREFIX_PATH=/opt/paraview/install/ -DBUILD_TESTING=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/themys_install -DBUILD_DOCUMENTATION=ON -DUSE_SERVERPLUGINS=OFF ../themys

# Build and install Themys
cmake --build .
cmake --install .
~~~

## Using spack

We maintain a [repository](https://gitlab.com/themys/spackinstaller) with [Spack](https://github.com/spack/spack) installation facilities and themys recipe. This repository is for internal purpose and will not evolve for external needs, however it can help you in your installation if you want to use Spack.

# Further informations

## Contributing

If you want to contribute to the Themys project please follow [this guide](CONTRIBUTING.md).

## Licence

Themys is under BSD 3-Clause License. See the [LICENSE](LICENSE) file for details.

## Related Projects

In our production context, Themys is not used as a standalone application, a full visualization tool suit is deployed:

- [Themys](https://gitlab.com/themys/themys) as the client GUI on client side;
- [Themys Serverplugin](https://gitlab.com/themys/themysserverplugins) (which is a submodule of Themys) on the server side;
- [Readers plugin](https://gitlab.com/themys/readers) on both side. Most of the CEA DAM simulations are outputting their results in `Hercule` databases. `Hercule` is a CEA/DAM library that allows codes to manage their IO on HPC cluster. The readers plugin is a `VTK` reader that is able to turn the information from a `Hercule` database into a `VTK` structure.

All theses projects are installed using recipes in the [Spackinstaller repository](https://gitlab.com/themys/spackinstaller).
