# Changelog

All notable changes to this project will be documented in this file.
Changes from the ThemysServerPlugin submodule will not be listed here.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Guiding Principles:

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes:

- **Added** for new features.
- **Changed** for changes in existing functionality.
- **Deprecated** for soon-to-be removed features.
- **Removed** for now removed features.
- **Fixed** for any bug fixes.
- **Security** in case of vulnerabilities.

## Unreleased

### Fixed

### Added

## Version 1.1.3 - 2025-03-07 -- Paraview needed at commit b879efe3 or newer

### Fixed

### Added

## Version 1.1.2 - 2025-01-09 -- Paraview needed at commit c0b74f15 or newer

### Fixed

- Fix integer promotion check from unsigned to int

### Added

- Add doc for CEAAnnotateTime filter

- Add doc on AlmaLinux installation (and client-server mode on Alma)

### Changed

- Update GUI Themys interface with users feedbacks (Menus, filters,...)

- Use new commontools CI files (simplify iwyu and cppcheck jobs)

- Improves MainWindow code to be more similar to the ParaView one

- Use new CppCheck analysis tool from commontools. Fix cppcheck warnings.

- Adds IWYU analysis job in the CI

- Fixes wrong commontools branch name in .gitlab-ci.yml and CMakeLists.txt

- Treats clang-tidy warnings as errors and fixes those warnings/errors

- Update ParaView build instructions for Ubuntu 24.04

## Version 1.1.1 - 2024-09-11 -- Paraview needed at commit 1eb2dd73 or newer

### Fixed

  - Update image references due to changes in PV background and color scale default values

### Added

  - Update submodule

## Version 1.1.0 - 2024-09-06 -- Paraview needed at commit 1eb2dd73 or newer

### Fixed

- Removes deprecated items. It includes `AnimationView` that it is replaced by `TimeManager`.

### Added

- Configure Cmake to add Paraview embedded Doc (commit pv:91c6b5d5)

- Rework of Documentations: procedures Fr and En

### Changed

- Takes into account change in the way commontools handles `clang-tidy`.
  Now the `CXX_CLANG_TIDY` target property is used.

## Version 1.0.9 - 2024-06-28 -- Paraview needed at commit dd91a67a or newer

### Fixed

### Added

## Version 1.0.8 - 2024-06-20 -- Paraview needed at commit 73a99c7f or newer

### Fixed

- Assistant: fix user selected by default

### Added

- Update GUI filters and sources from the official paraview 5.12 release
- Use Assistant dialog when using the pipeline browser's context menu items "Change file" or "Open" instead of the default ParaView "Open file" dialog
- Update CMake logic to account for a change in Plugin submodule

### Changed

- Hide some useless toolbars in the GUI

- Adds english documentation for masking and ghosting, numpy interface and CEACellDataToPointData filter

- Update the reference image for InterfaceModes test on 4 servers (renamed InterfaceModes4servers)

- Add Louis Gombert as contributor

- Restructure the project. Rewrite README.md and adds CONTRIBUTING.md.

- Documentation of the readers has been updated and translated

- Removes use of c++17 features during documentation path search

- Changes the repository of the image used for CI

  Closes #66

- Fetching content of `commontools` repo is made for debug builds only

- The documentation is made relocatable

## Version 1.0.7 - 2024-04-02 -- Paraview needed at commit 8f4e48bc or newer

### Fixed

 - Fix a double open in assistant dialog

### Added

- The ParaView's Read the Docs web pages are embedded in Themys's documentation

- Uses common CMake script of the ThemysCommonTools repository.
  Adds cppcheck and clang-tidy jobs in the CI.
  Introduces code coverage measurement in the CI.

- Configuration files for pre-commit and clang-format are added.
  Clang-tidy configuration file is now identical to the one in themysserverplugins project.

### Changed

- Use common repo for storing and mutualizing CI across Themys repositories

## Version 1.0.6 - 2023-12-06

### Fixed

### Added
### Changed
 - Replace Advanced On/Off button by an interface mode combobox. Interface modes are described in a json file, which specifies which toolbars, widgets and menus should be shown or hidden by each different interface mode.

## Version 1.0.5 - 2023-10-02

### Fixed

### Added

## Version 1.0.4 - 2023-09-25

### Changed

- Adds CMake presets.

- Documentation about the `ContourWriter` filter has been updated.

  Closes #31

- The `TimeInspector` is replaced by new `ParaView`'s `TimeManager`.

  Closes #37

### Fixed

- Crash when opening a file has been fixed. It was due to a recent ParaView's assistant evolution.

  Closes #38

### Added

- Adds logging information when the `Assistant` is fetching storages settings.

- Documentation about the use of the "Glyph" filter to display arrows on a vector,
  has been added.

  Closes #36

- Logging mechanism have been added to the assistant and are now visible
in the LogViewer of ParaView.

## Version 1.0.3 - 2023-05-03

## Version 1.0.2 - 2023-02-20

### Changed

#### RegistrationName

- Use RegistrationName instead of PipelineName to rename proxies in the
  pipeline browser

#### Assistant rework

- The assistant has been separated from the file dialog changing sligthly
  its look and feel.

- It was also reworked to support the usecase of changing user to select
  the storage.

#### Storages as settings

- The assistant storages used to be setup using the favorites of the file dialog.
  This has been reworked to separates the concept of storages from the concept of favorites.
  Now, the settings are used to setup the storages.

## Version 1.0.1 - 2022-12-19

## Version 1.0.0 - 2022-11-25
