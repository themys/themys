# Contribution Guide

## Code of Conduct

Contributing in Themys or any other open source project should be a harassment-free experience for everyone, regardless of age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.
We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy project.

Please, be respectful of differing opinions, viewpoints, and experiences; give and gracefully accept constructive feedback; and demonstrate empathy toward other people.

## Find something to do

If you want to contribute to the Themys project, here is some hints of what you can do

- Open an issue

- Improve the documentation

- Reproduce bugs and confirm that issues are valid

- Investigate or debug complex issues

- Design or specify a solution

- Give your opinion on ongoing discussion

- Fix bugs and crashes

- Review pull requests

## The Themys workflow policy

Themys development process is made in accordance with the Git forking workflow.
In other words every contributor should fork the Themys project before starting its contribution.

Forking the project will prevent pollution of the Themys repositories with stall branches and adds security because only
the maintainers are able to modify the repository.

If not already done, create an account on [gitlab](https://.gitlab.com).

To fork Themys project go to the [project page](https://gitlab.com/themys/themys) and click on the `Fork` button in the upper right corner of the page.

![](images/themys_how_to_fork.png)
*Click the Fork button in the upper right corner (red rectangle)*

On the page that just opens select the namespace where to fork the project. Let other options with their default values and continue.

Now Themys project is forked into the namespace selected previously. For convenience reason, lets call this namespace "your_namespace".

## Setting up the development environment

### Cloning the project

It's time to clone the project. Open a terminal and type:

~~~bash
git clone https://gitlab.com/your_namespace/themys.git
~~~

If your gitlab account is configured to work with ssh protocol you can instead type:

~~~bash
git clone git@gitlab.com:your_namespace/themys.git
~~~

This cloned repository will be used whatever the following method you choose.

### Using docker to work in an environment "batteries included"

Themys development team maintains a docker image that contains all the dependencies necessary to develop easily.

Install docker by following those [instructions](https://docs.docker.com/engine/install/). Do not forget the [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/).

Once done, pull the image from the themys repository:

~~~bash
docker pull registry.gitlab.com/themys/themys_docker:latest
~~~

To use it:

~~~bash
xhost +local:docker
docker run --network=host -e DISPLAY=:0  --device=/dev/dri:/dev/dri -it -v /path/to/your/workspace/themys:/opt/themys registry.gitlab.com/themys/themysdocker:latest
~~~

After typing those commands, you are inside a docker container on a ubuntu based system.

The source of the project are located inside the `/opt/themys` directory. As this directory is mounted as a volume (`-v` option in the command above), every modification made inside this directory is also made in the `/path/to/your/workspace/themys` directory on the host system.

You can now start modifying `Themys` (see sections below).

### Using VScode with Dev Containers extension

We strongly encourage the contributor to use [VSCode](https://code.visualstudio.com/) and its [Dev Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) as it eases a lot the development process.
Note that Visual Studio and IntelliJ IDEA also support the Development Container Specification.

Launch VSCode, then open the themys directory. VSCode will suggest that you open the directory in a devcontainer. Just accept, wait for the image to be pulled and then you can work in VSCode as usual but all the jobs (configuration, compilation ...) will be done inside the container.

### Developing without docker

It is also possible to set your environement manually without requiring docker manipulations.

First of all, install `ParaView` and all of its dependencies as explained in the [README](./README.md).

You will also need the development dependencies. Assuming you are developing on a Debian/Ubuntu based machine, please type:

~~~bash
sudo apt-get install libtrompeloeil-cpp-dev cppcheck patch make catch2 gcovr clang-tidy-15 cmake ninja-build
~~~

Once done, follow the instructions below to start modifying the source code.

## Branch creation

The developing environment is now set up, the next step is to create a branch to work within. No modification should be made on the master branch.

If your contribution solves an issue then it is strongly advice to use the Gitlab web interface to create a branch from the issue page:

![](images/issue_create_branch_0.png)
*Click on Create Merge Request button to select Create Branch*

![](images/issue_create_branch_1.png)
*Click on Create Branch*

Once done, open a terminal in your `/path/to/your/workspace/themys` directory and fetch the modification made to your repository:

~~~bash
cd /path/to/your/workspace/themys
git fetch
git checkout your_new_branch_name
~~~

If your contribution is a new feature or something that is not linked to an issue then choose a name for your branch that is illustrative of the modification you are planning. Once chosen, create the branch locally in your workspace and checkout it at the same time:

~~~bash
cd /path/to/your/workspace/themys
git checkout -b your_new_branch_name
~~~

## Source code modification

Once on a freshly created branch, you can start modifying the source code.

For C++ files, Themys development team followed style that is stored into
the `.clang-format` configuration file. Most of the recent IDEs will be aware
of that file and their formatting tool will take it into account automatically.

For manually formatting the file please type:

~~~bash
clang-format --style=file -i /path/to/your/file
~~~

For Python files, please format them using [Black](https://github.com/psf/black).

~~~bash
black /path/to/your/file/or/directory
~~~

Or:

~~~bash
python -m black /path/to/your/file/or/directory
~~~

If modifying CMake files, please format them with `cmake-format`. A configuration file (`.cmake-format.yaml`) is located at the root of this repository.
Please run:

~~~bash
cmake-format -i /path/to/your/file
~~~

All those tedious formatting tasks can be handled by `pre-commit` tool. It is strongly adviced to use this tool.
Update your git configuration so that every commit you make is checked by `pre-commit`.

~~~bash
cd /path/to/your/themys/workspace
pre-commit install
~~~

Once done, you just have to modify the sources and to commit them. If one or more of those files do not conform to the style guides, `pre-commit` will warn you about it and prevent the commit to be done. Format correctly the code, then retry the commit.

`pre-commit` can also be run manually:

~~~bash
cd /path/to/your/themys/workspace
pre-commit run -a
~~~

## Testing

Every development should be accompanied with one (or more) test that check this development and will ensure its correctness in the future.
Themys test framework is derived from the ParaView/VTK test system.
Two types of tests exist, XML and Python tests. While Python tests are far more readable and easily modifiable they cannot tests GUI specificites. This role is devoted to XML tests.
For now, in Themys repository, only XML tests are set up. To see examples of Python tests, please have a look to the tests of [themysserverplugins](https://gitlab.com/themys/themysserverplugins) or [readers](https://gitlab.com/themys/readers) repositories.

### Saving the database

If you are using a database that is not already in the project, you have to add it under the `/path/to/your/themys/data/testing/` directory in one of the appropriate subdirectories, depending on the type of your database.

Please check that the freshly added database is handled through `git-lfs`.
To do that:

~~~bash
cd /path/to/your/themys
git lfs ls-files
~~~

Check that the database appears in the output.

If `git-lfs` is not yet installed (it may be the case if you do not use the docker image), please install it with your package manager.
For `ubuntu` or `debian` based distributions:

~~~bash
sudo apt-get install git-lfs
~~~

### Recording the test

To set up a test, the first step is to record it through the Themys GUI.
After having started Themys:

1) Click on `Tools`
1) Then choose `Record Test...` as shown below

    ![](images/record_test_0.png)

    *Select Record Test... to start the test recording*

1) Save the test file in the `/path/to/your/themys/testing/WithParaView/` with a descriptive name (for the following the name will be 'YourTest').

    ![](images/record_test_1.png)

    *Select where to save the test file*

1) An new window showing the test recording appears

    ![](images/record_test_2.png)

    *Test recording on its way*

1) Open your database (stored in `/path/to/your/themys/data/testing`) and do the appropriate manipulations

1) When done, click on `Stop Recording` in this last window

### Tweak the test file

The test file should begin with a comment indicating what this test is for and be relevant to verify the new feature.

In order for the test to be independant of your file system, please fix the path to the database. It should be something along `/path/to/your/themys/data/testing/appropriate_subdirectory/your_database`, replaces it with `$PARAVIEW_DATA_ROOT/testing/appropriate_subdirectory/your_database`.

### Register the test

In order for the test to be known to `ctest`, the tool used to launch the tests, please register it.

To do so, open the `/path/to/your/themys/testing/WithParaView/CMakeLists.txt` file and add the following lines at the end of the file:

~~~cmake
register_paraview_test(YourTest)
register_paraview_client_server_test(YourTest 4)
~~~

The first line register a simple ParaView test, while the second register a client/server test with 4 servers. Feel free to adapt the number of servers to your needs but not over 8 (due to physical limitations of the hardware used for CI).

### Running the tests

Before going toward the merge request, please ensure that all the tests end successfully.

After the build stage, please run `ctest` on all the tests:

~~~bash
cd /path/to/your/build/directory
ctest -j 20
~~~

Here `20` represents the number of tests that will be run concurrently. Adapt it to your hardware.

If some tests are failing, you can relaunch them and ask `ctest` to print the output:

~~~bash
ctest --rerun-failed --output-on-failure
~~~

Once every thing is ok you can move on to the next step.

## Adding documentation

Each development should be documented in the source files and in the test files to enable other contributors to understand the code. Themys uses `Sphinx` to build its documentation.
Thus the documentation should be written in [`reStructuredText`](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) format.

Your development should be accompanied with updated documentation to explain what is the new feature and how to use it, screenshots are welcome.

To add new files in the documentation, please add them under both `en` and `fr` directories in the `doc` folder.
Then `en` files should be written in english while `fr` files should be written in french. If you are not french speaker, there is off course no need to deal with `fr` files. The development team will translate your english files to generate the french ones.

Once your files are added, please do not forget to register them in the corresponding `CMakeLists.txt` file.
For example, it is necessary to add your filename into the `DOC_FILES_EN` (and maybe `DOC_FILES_FR` if in french) list:

~~~cmake
set(DOC_FILES_EN
    ${DOC_FILES_EN}
    ${CMAKE_CURRENT_LIST_DIR}/chap1.rst
    ...
    ${CMAKE_CURRENT_LIST_DIR}/YourFreshlyAddedDoc.rst
    PARENT_SCOPE)
~~~

Documentation is automatically generated when building the Themys project (if the CMake option `BUILD_DOCUMENTATION` is on).
To verify the final visual, launch Themys and hit the F1 key to open the documentation and look for your feature.

## Modify the CHANGELOG.md

Once everything is ok and before launching the Merge Request process, please modify the [CHANGELOG](CHANGELOG.md) file.

If you are not yet in there, you should also update the [CONTRIBUTORS](CONTRIBUTORS.md) file with your name and email.

## Creating a Merge Request

To create a Merge Request please open your web browser to your Themys's fork page: https://gitlab.com/your_namespace/themys

Then, on left column, click on `Code` then `Merge requests`:

![](images/merge_request_0.png)

On the next page click on the blue `New merge request` button:

![](images/merge_request_1.png)

On the next page in the `Select source branch` selection list, select the branch you have been working on, do not change the `Target branch`:

![](images/merge_request_2.png)

Then click on the blue `Compare branches and continue` button.

On the next page, fill in the `Title` and `Description` section but do not modify other items. If necessary, those items will be modified by maintainers.

![](images/merge_request_3.png)

Once done, click on the blue `Create merge request` button to finish the merge request submission process.

Please keep in mind that your MR will be reviewed by others contributors, so keep change small and coherent.

In order to ease the review process, if you want to implement different independent features please do so in different MR.

### Testing and Code Coverage

Your MR must pass Themys's unit tests and documentation tests, and must fit with our style rules. We enforce these guidelines with our CI process.

The following image present the different steps of the CI.

![](images/CI_0.png)

**Build stage** has 3 steps:

- *build_debug*: build in debug mode with standard gcc of the CI image
- *build_debug_legacy*: build in debug mode with gcc-8
- *build_release*: build in release mode with standard gcc of the CI image

**Code quality** stage has 4 steps:

- *check_changelog*: checks that the CHANGELOG.md file has been modified
- *clang-tidy*: statically analyzes the code with `clang-tidy` to detect common code smells. The results are shown in the `Code Quality` item in the MR front page
- *cppcheck*: statically analyzes the code with `cppcheck` to detect common code smells.
- *pre-commit*: launch `pre-commit` on the code to check that code styles are respected. `pre-commit` itself will launch `clang-format`, `black`, `cmake-format` and other linters.

**Tests** stage has 2 steps:

- *all_tests_debug*: run all the tests with the debug mode executable
- *all_tests_release*: run all the tests with the release mode executable

**Documentation** stage has 1 step:

- *documentation_generation*: check that *sphinx* is able to generate the documentation

To be merged, a MR should pass all CI steps (all is green in the CI pipeline) and be approved by at least one maintainer of the project.

Test coverage is evaluated at each MR, please, add as many tests as needed to obtain same or better test coverage percentage.
