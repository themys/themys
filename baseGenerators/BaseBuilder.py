import json
import os
import vtk

# Global base definition
timestepFolderName = "times"
timesteps_orig = {0.3333, 0.6666, 0.9999}
timesteps_twin = {0.3333, 0.7666, 1.3333}
materials = {"Material1", "Material2"}
domains = {"Domain1", "Domain2", "Domain3"}
simuAuthor = "Kitware"
distance_orig = 1
distance_twin = 2
plane_orig = 6
plane_twin = 7


def Build(path, baseName, simuName, builderFunction, isTwin=False):
    if isTwin:
        suffixTwin = "_Twin"
        timesteps = timesteps_twin
        distance = distance_twin
        plane = plane_twin
    else:
        suffixTwin = ""
        timesteps = timesteps_orig
        distance = distance_orig
        plane = plane_orig

    # Python object representing the vtm.series file
    vtkSeriesObject = {}
    vtkSeriesObject["file-series-version"] = "1.0"
    vtkSeriesObject["files"] = []

    # Create output directory storing each timesteps
    timestepFolder = os.path.join(path, timestepFolderName)
    if not os.path.exists(timestepFolder):
        os.makedirs(timestepFolder)

    # Prepare metadata arrays
    simuNameArray = vtk.vtkStringArray()
    simuNameArray.SetName("Simulation Name" + suffixTwin)
    simuNameArray.SetNumberOfValues(1)
    simuNameArray.SetValue(0, simuName)
    authorNameArray = vtk.vtkStringArray()
    authorNameArray.SetName("Author" + suffixTwin)
    authorNameArray.SetNumberOfValues(1)
    authorNameArray.SetValue(0, simuAuthor)
    timeValueArray = vtk.vtkDoubleArray()
    timeValueArray.SetName("TimeValue")
    timeValueArray.SetNumberOfValues(1)
    distanceArray = vtk.vtkIntArray()
    distanceArray.SetName("DistanceToOrigin")
    distanceArray.SetNumberOfValues(1)
    distanceArray.SetValue(0, distance)
    planeArray = vtk.vtkIntArray()
    planeArray.SetName("Plane")
    planeArray.SetNumberOfValues(1)
    planeArray.SetValue(0, plane)

    # For each timestep
    for timeIdx, time in enumerate(timesteps):
        filename = baseName + "_t" + str(timeIdx) + ".vtm"
        vtkSeriesObject["files"].append(
            {"name": os.path.join(timestepFolderName, filename), "time": time}
        )
        timeValueArray.SetValue(0, time)

        base = vtk.vtkMultiBlockDataSet()
        base.SetNumberOfBlocks(len(materials))

        # For each material
        for materialIdx, materialName in enumerate(materials):
            material = vtk.vtkMultiPieceDataSet()
            material.SetNumberOfPieces(len(domains))

            materialNameArray = vtk.vtkStringArray()
            materialNameArray.SetName("Material" + suffixTwin)
            materialNameArray.SetNumberOfValues(1)
            materialNameArray.SetValue(0, materialName)

            base.SetBlock(materialIdx, material)
            base.GetMetaData(materialIdx).Set(
                vtk.vtkCompositeDataSet.NAME(), materialName
            )

            # For each calculation domain
            for domainIdx, domainName in enumerate(domains):
                domain = builderFunction(timeIdx, materialIdx, domainIdx, isTwin)
                domain.GetFieldData().AddArray(simuNameArray)
                domain.GetFieldData().AddArray(authorNameArray)
                domain.GetFieldData().AddArray(timeValueArray)
                domain.GetFieldData().AddArray(materialNameArray)
                domain.GetFieldData().AddArray(distanceArray)
                domain.GetFieldData().AddArray(planeArray)

                material.SetPiece(domainIdx, domain)
                material.GetMetaData(domainIdx).Set(
                    vtk.vtkCompositeDataSet.NAME(), domainName
                )

        # Write curret base
        writer = vtk.vtkXMLMultiBlockDataWriter()
        writer.SetFileName(os.path.join(timestepFolder, filename))
        writer.SetInputData(base)
        writer.SetCompressorTypeToNone()
        writer.SetDataModeToAscii()
        writer.Write()

    # Write the vtp.series object as a JSON file
    fullPath = os.path.join(path, baseName + ".vtm.series")
    vtkSeriesString = json.dumps(vtkSeriesObject, sort_keys=True, indent=2)
    with open(fullPath, "w") as outFile:
        outFile.write(vtkSeriesString)
