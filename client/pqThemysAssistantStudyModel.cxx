#include "pqThemysAssistantStudyModel.h"

#include <QByteArray> // for QByteArray
#include <QDateTime>
#include <QSortFilterProxyModel>
#include <algorithm> // for any_of
#include <map>       // for operator!=, opera...
#include <string>
#include <utility> // for move

#include <vtk_nlohmannjson.h>
#include VTK_NLOHMANN_JSON(json.hpp)
// IWYU pragma: no_include <nlohmann/detail/iterators/iter_impl.hpp>

#include <pqServer.h>
#include <pqServerResource.h>
#include <vtkSMSettings.h>

#include "pqThemysAssistantBaseManager.h" // for Base, Study, pqTh...

// ----------------------------------------------------------------------------
pqThemysAssistantStudyModel::pqThemysAssistantStudyModel(
    pqServer* server, pqThemysAssistantBaseManager* manager, QObject* parent)
    : Superclass(parent), ProxyModel(new QSortFilterProxyModel(this)),
      BaseManager(manager), ActiveStudy{0}
{
  const pqServerResource resource =
      server != nullptr ? server->getResource() : pqServerResource("builtin:");
  this->ServerURI = resource.serverName().isEmpty() ? resource.toURI()
                                                    : resource.serverName();

  this->ProxyModel->setSourceModel(this);

  // Fetch stored studies from settings
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  const std::string settingStrKey =
      ("Assistant#" + this->ServerURI + STUDY_SETTING_KEY).toStdString();
  if (settings->HasSetting(settingStrKey.c_str()))
  {
    const std::string studiesString =
        settings->GetSettingAsString(settingStrKey.c_str(), "");
    if (!studiesString.empty())
    {
      auto studyArray = nlohmann::json::parse(studiesString);
      for (const auto& studyJson : studyArray)
      {
        assistant::Study study;
        if (pqThemysAssistantStudyModel::ParseStudy(studyJson, study))
        {
          this->Studies.push_back(std::move(study));
        }
      }
    }
  }
}

// ----------------------------------------------------------------------------
// NOLINTNEXTLINE(bugprone-exception-escape)
pqThemysAssistantStudyModel::~pqThemysAssistantStudyModel()
{
  // Store current studies in settings
  auto studyArray = nlohmann::json::array();
  for (const auto& study : this->Studies)
  {
    nlohmann::json studyJson =
        pqThemysAssistantStudyModel::SerializeStudy(study);
    studyArray.push_back(std::move(studyJson));
  }

  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  const std::string settingStrKey =
      ("Assistant#" + this->ServerURI + STUDY_SETTING_KEY).toStdString();
  settings->SetSetting(settingStrKey.c_str(), studyArray.dump());
}

// ----------------------------------------------------------------------------
int pqThemysAssistantStudyModel::columnCount(
    const QModelIndex& /*parent*/) const
{
  // Name, Date
  return 2;
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantStudyModel::data(const QModelIndex& idx,
                                           int role) const
{
  if (idx.row() >= this->Studies.size())
  {
    return {};
  }

  const assistant::Study& study = this->Studies[idx.row()];
  if (role == Qt::DisplayRole || role == Qt::EditRole)
  {
    switch (idx.column())
    {
    case 0:
      return study.Name;
    case 1:
      return study.Date;
    default:
      return {};
    }
  } else if (role == Qt::ToolTipRole)
  {
    return QString::number(study.Bases.size()) + " bases";
  } else if (role == Qt::UserRole)
  {
    return QVariant::fromValue<QVector<assistant::Base>>(study.Bases);
  }

  return {};
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantStudyModel::setData(const QModelIndex& idx,
                                          const QVariant& value, int role)
{
  const int row = idx.row();
  const QString asString = value.toString();
  if (row >= this->Studies.size() || idx.column() != 0 || asString.isEmpty())
  {
    return false;
  }

  if (this->Studies[row].Name != asString)
  {
    this->Studies[row].Name = asString;
    Q_EMIT this->dataChanged(idx, idx, {role});
    if (row == this->ActiveStudy)
    {
      Q_EMIT this->activeStudyRenamed(asString);
    }
    return true;
  }

  return false;
}

// ----------------------------------------------------------------------------
QModelIndex
pqThemysAssistantStudyModel::index(int row, int column,
                                   const QModelIndex& /*parent*/) const
{
  return this->createIndex(row, column);
}

// ----------------------------------------------------------------------------
QModelIndex
pqThemysAssistantStudyModel::parent(const QModelIndex& /*child*/) const
{
  return {};
}

// ----------------------------------------------------------------------------
int pqThemysAssistantStudyModel::rowCount(const QModelIndex& /*parent*/) const
{
  return this->Studies.size();
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantStudyModel::hasChildren(const QModelIndex& idx) const
{
  // Study will never display its children bases.
  return !idx.isValid();
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantStudyModel::headerData(
    int section, Qt::Orientation /*orientation*/, int role) const
{
  if (role == Qt::DisplayRole)
  {
    switch (section)
    {
    case 0:
      return tr("Study name");
    case 1:
      return tr("Date");
    default:
      return {};
    }
  }

  return {};
}

// ----------------------------------------------------------------------------
Qt::ItemFlags pqThemysAssistantStudyModel::flags(const QModelIndex& idx) const
{
  Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  if (idx.column() == 0)
  {
    flags |= Qt::ItemIsEditable;
  }
  return flags;
}

// ----------------------------------------------------------------------------
int pqThemysAssistantStudyModel::addStudy(const QString& name)
{
  const int idx = this->Studies.size();

  this->beginInsertRows(QModelIndex(), idx, idx);
  this->Studies.push_back({name, QDateTime::currentDateTime(), {}});
  this->endInsertRows();

  return idx;
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStudyModel::removeStudy(int row)
{
  this->beginRemoveRows(QModelIndex(), row, row);
  this->Studies.remove(row);
  this->endRemoveRows();
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStudyModel::clearStudies()
{
  this->beginResetModel();
  this->Studies.clear();
  this->endResetModel();
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStudyModel::addBaseToStudy(int studyIdx,
                                                 const assistant::Base& base)
{
  if (studyIdx >= this->Studies.size() || studyIdx < 0)
  {
    return;
  }

  // If already there, do not add
  const auto& existing_bases = this->Studies[studyIdx].Bases;
  if (std::any_of(existing_bases.cbegin(), existing_bases.cend(),
                  [&base](const auto& existing) { return existing == base; }))
  {
    return;
  }

  this->Studies[studyIdx].Bases.push_back(base);
  this->Studies[studyIdx].Date = QDateTime::currentDateTime();
  const QModelIndex idx1 = this->index(studyIdx, 0, QModelIndex());
  const QModelIndex idx2 = this->index(studyIdx, 1, QModelIndex());
  Q_EMIT this->dataChanged(idx1, idx2, {Qt::ToolTip, Qt::DisplayRole});
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStudyModel::removeBaseFromStudy(
    int studyIdx, const assistant::Base& base)
{
  if (studyIdx >= this->Studies.size() || studyIdx < 0)
  {
    return;
  }

  auto& bases = this->Studies[studyIdx].Bases;
  for (int i = 0; i < bases.size(); ++i)
  {
    if (bases[i] == base)
    {
      bases.removeAt(i);
      return;
    }
  }

  const QModelIndex idx1 = this->index(studyIdx, 0, QModelIndex());
  const QModelIndex idx2 = this->index(studyIdx, 1, QModelIndex());
  Q_EMIT this->dataChanged(idx1, idx2, {Qt::ToolTip});
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStudyModel::renameBaseInStudy(int studyIdx, int baseIdx,
                                                    const QString& newName)
{
  if (studyIdx >= this->Studies.size() || studyIdx < 0)
  {
    return;
  }

  this->Studies[studyIdx].Bases[baseIdx].Name = newName;
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantStudyModel::ParseStudy(const nlohmann::json& studyJson,
                                             assistant::Study& result)
{
  if (!studyJson.contains("name") || !studyJson.contains("date") ||
      !studyJson.contains("bases"))
  {
    return false;
  }

  result.Name = studyJson["name"].get<std::string>().c_str();
  result.Date = QDateTime::fromString(
      studyJson["date"].get<std::string>().c_str(), Qt::TextDate);

  for (const auto& baseJson : studyJson["bases"])
  {
    assistant::Base base;
    if (pqThemysAssistantBaseManager::ParseBase(baseJson, base))
    {
      result.Bases.push_back(std::move(base));
    }
  }

  return true;
}

// ----------------------------------------------------------------------------
nlohmann::json
pqThemysAssistantStudyModel::SerializeStudy(const assistant::Study& study)
{
  nlohmann::json result;
  result["name"] = study.Name.toUtf8().data();
  result["date"] = study.Date.toString(Qt::TextDate).toUtf8().data();

  nlohmann::json basesJson = nlohmann::json::array();
  for (const auto& base : study.Bases)
  {
    nlohmann::json currentBase =
        pqThemysAssistantBaseManager::SerializeBase(base);
    basesJson.push_back(std::move(currentBase));
  }
  result["bases"] = basesJson;

  return result;
}
