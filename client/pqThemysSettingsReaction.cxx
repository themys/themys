#include "pqThemysSettingsReaction.h"

#include <QPointer>    // for QPointer
#include <QStringList> // for QStringList

#include <pqCoreUtilities.h>
#include <pqSettingsDialog.h>

class QAction;

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
QPointer<pqSettingsDialog> pqThemysSettingsReaction::Dialog;

//-----------------------------------------------------------------------------
pqThemysSettingsReaction::pqThemysSettingsReaction(QAction* parentObject)
    : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
pqThemysSettingsReaction::~pqThemysSettingsReaction()
{
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  delete pqThemysSettingsReaction::Dialog;
}

//-----------------------------------------------------------------------------
void pqThemysSettingsReaction::showApplicationSettingsDialog(
    const QString& tabName)
{
  if (pqThemysSettingsReaction::Dialog == nullptr)
  {
    // NOLINTBEGIN(cppcoreguidelines-owning-memory)
    pqThemysSettingsReaction::Dialog =
        new pqSettingsDialog(pqCoreUtilities::mainWidget(), Qt::WindowFlags(),
                             QStringList("Themys"));
    // NOLINTEND(cppcoreguidelines-owning-memory)
    pqThemysSettingsReaction::Dialog->setObjectName("ApplicationSettings");
    pqThemysSettingsReaction::Dialog->setAttribute(Qt::WA_DeleteOnClose, true);
  }
  pqThemysSettingsReaction::Dialog->show();
  pqThemysSettingsReaction::Dialog->raise();
  pqThemysSettingsReaction::Dialog->showTab(tabName);
}
