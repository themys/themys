#include "pqThemysViewFrameActionsImplementation.h"

#include <QByteArray> // for QByteArray
#include <map>        // for map, operator!=
#include <string>     // for basic_string
#include <utility>    // for pair

#include <pqStandardViewFrameActionsImplementation.h> // for pqStandardView...
#include <vtkSMSettings.h>

#include "pqThemysSettingsInfo.h"

//-----------------------------------------------------------------------------
pqThemysViewFrameActionsImplementation::pqThemysViewFrameActionsImplementation(
    QObject* parent)
    : Superclass(parent)
{
}

//-----------------------------------------------------------------------------
QList<pqStandardViewFrameActionsImplementation::ViewType>
pqThemysViewFrameActionsImplementation::availableViewTypes()
{
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  const bool advanced =
      settings->GetSettingAsInt(QString(".settings." + SETTINGS_GROUP_NAME +
                                        "." + INTERFACE_MODE_NAME)
                                    .toUtf8()
                                    .data(),
                                0 /*false*/) != 0;

  if (advanced)
  {
    return this->Superclass::availableViewTypes();
    // https://gitlab.kitware.com/paraview/paraview/-/blob/master/Qt/ApplicationComponents/pqStandardViewFrameActionsImplementation.cxx
  }
  // https://gitlab.kitware.com/paraview/paraview/-/blob/master/Remoting/Views/Resources/views_and_representations.xml
  const std::map<std::string, std::string> option_view{
      {"RenderView", "Render View"},
      {"XYChartView", "Line Chart View"},
      {"SpreadSheetView", "SpreadSheet View"}};
  QList<pqStandardViewFrameActionsImplementation::ViewType> views;
  for (const auto& [key, value] : option_view)
  {
    pqStandardViewFrameActionsImplementation::ViewType info;
    info.Name = QString(key.c_str());
    info.Label = QString(value.c_str());
    views.push_back(info);
  }
  return views;
}
