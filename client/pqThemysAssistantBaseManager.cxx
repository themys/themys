#include "pqThemysAssistantBaseManager.h"

#include <QByteArray> // for QByteArray
#include <QChar>      // for operator!=, QChar
#include <QCharRef>   // for QCharRef
#include <map>        // for operator!=, opera...
#include <regex>
#include <string>
#include <vector>

#include <pqSMAdaptor.h>
#include <pqServer.h>
#include <pqServerResource.h>
#include <vtkCollection.h>
#include <vtkCollectionIterator.h>
#include <vtkLogger.h>
#include <vtkObject.h> // for vtkObject
#include <vtkPVFileInformation.h>
#include <vtkPVFileInformationHelper.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMProxy.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSettings.h>
#include <vtkSmartPointerBase.h> // for operator!=, vtkSm...
#include <vtk_nlohmannjson.h>
#include VTK_NLOHMANN_JSON(json.hpp)
// IWYU pragma: no_include <nlohmann/detail/iterators/iter_impl.hpp>

#include <utility> // for pair, move

#include "pqThemysAssistantStorageModel.h"

// ----------------------------------------------------------------------------
namespace {
std::string SmartJoinDirectoryPath(std::vector<std::string> components)
{
  std::string res;

  for (auto& str : components)
  {
    if (!str.empty())
    {
      res.append(str);
      if (str.back() != '/')
      {
        res.push_back('/');
      }
    }
  }

  return res;
}
}; // namespace

// ----------------------------------------------------------------------------
pqThemysAssistantBaseManager::pqThemysAssistantBaseManager(
    pqServer* server, pqThemysAssistantStorageModel* storageModel,
    QObject* parent)
    : Superclass(parent), StorageModel(storageModel)
{
  const pqServerResource resource =
      server != nullptr ? server->getResource() : pqServerResource("builtin:");
  this->ServerURI = resource.serverName().isEmpty() ? resource.toURI()
                                                    : resource.serverName();

  // Setup filesystem diggers utilities
  if (server != nullptr)
  {
    vtkSMSessionProxyManager* pxm = server->proxyManager();

    vtkSMProxy* helper = pxm->NewProxy("misc", "FileInformationHelper");
    vtkSMPropertyHelper(helper, "ReadDetailedFileInformation").Set(0 /*false*/);
    helper->UpdateVTKObjects();

    this->FileInformationHelperProxy.TakeReference(helper);
  } else
  {
    vtkPVFileInformationHelper* helper = vtkPVFileInformationHelper::New();
    helper->SetReadDetailedFileInformation(false);

    this->FileInformationHelper.TakeReference(helper);
  }

  // Fetch recent bases from settings
  const QString settingKey = "Assistant#" + this->ServerURI +
                             pqThemysAssistantBaseManager::RECENT_BASES_KEY;
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  if (settings->HasSetting(settingKey.toUtf8().data()))
  {
    const std::string basesString =
        settings->GetSettingAsString(settingKey.toUtf8().data(), "");
    if (!basesString.empty())
    {
      auto recentBasesArray = nlohmann::json::parse(basesString);
      for (const auto& baseJson : recentBasesArray)
      {
        assistant::Base base;
        if (pqThemysAssistantBaseManager::ParseBase(baseJson, base))
        {
          this->RecentBases.push_back(base);
        }
      }
    }
  }

  // Fetch bases regex from settings
  constexpr const char* regexSettingKey = ".settings.ThemysSettings.BasesRegex";
  if (settings->HasSetting(regexSettingKey))
  {
    unsigned int nRegex = settings->GetSettingNumberOfElements(regexSettingKey);
    // if nRegex == 0 but HasSetting == true it means that the setting is just a
    // single value so force to fetch the first value.
    if (nRegex == 0)
    {
      nRegex = 1;
    }

    for (unsigned int i = 0; i < nRegex; ++i)
    {
      const std::string regex =
          settings->GetSettingAsString(regexSettingKey, i, "");
      if (!regex.empty())
      {
        this->BasesRegex.push_back(regex.c_str());
      }
    }
  }
}

// ----------------------------------------------------------------------------
// NOLINTNEXTLINE(bugprone-exception-escape)
pqThemysAssistantBaseManager::~pqThemysAssistantBaseManager()
{
  // Store current recent bases in settings
  auto recentBasesArray = nlohmann::json::array();
  for (const auto& base : this->RecentBases)
  {
    nlohmann::json baseJson = pqThemysAssistantBaseManager::SerializeBase(base);
    recentBasesArray.push_back(std::move(baseJson));
  }

  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  const QString settingKey = "Assistant#" + this->ServerURI +
                             pqThemysAssistantBaseManager::RECENT_BASES_KEY;
  settings->SetSetting(settingKey.toUtf8().data(), recentBasesArray.dump());
}

// ----------------------------------------------------------------------------
QVector<assistant::Base> pqThemysAssistantBaseManager::CreateBases(
    const QString& storageName, const QString& user, const QString& curCase,
    const QVector<QString>& regexes) const
{
  const QString basePath = this->GetBasePath(storageName, user, curCase);

  // Create bases recursively into directories. Put recursion limit to 10.
  constexpr int RECURSION_LIMIT{10};
  QVector<assistant::Base> result;
  this->CreateBasesRecursive(basePath, regexes, 0, RECURSION_LIMIT, result);
  for (auto& base : result)
  {
    base.Name = base.Identifier.mid(basePath.size());
    base.StorageName = storageName;
    base.User = user;
    base.Case = curCase;
    vtkLogF(8, "Base Found, Name: %s, StorageName: %s, User: %s, Case: %s.",
            base.Name.toUtf8().data(), base.StorageName.toUtf8().data(),
            base.User.toUtf8().data(), base.Case.toUtf8().data());
  }

  return result;
}

// ----------------------------------------------------------------------------
// NOLINTNEXTLINE(readability-function-cognitive-complexity)
void pqThemysAssistantBaseManager::CreateBasesRecursive(
    QString path, const QVector<QString>& regexes, int depth,
    const int maxDepth, QVector<assistant::Base>& result) const
{
  if (path.isEmpty() || depth == maxDepth)
  {
    return;
  }

  if (path.back() != '/')
  {
    path.push_back('/');
  }
  auto information = this->GetInformations(path, true);

  // Explore current folder, go down if necessary and append all files.
  vtkSmartPointer<vtkCollectionIterator> iter;
  iter.TakeReference(information->GetContents()->NewIterator());
  IndexList fileList;
  for (iter->InitTraversal(); iter->IsDoneWithTraversal() == 0;
       iter->GoToNextItem())
  {
    vtkPVFileInformation* info =
        vtkPVFileInformation::SafeDownCast(iter->GetCurrentObject());
    if (info == nullptr || info->GetHidden())
    {
      continue;
    }

    if (vtkPVFileInformation::IsDirectory(info->GetType()))
    {
      this->CreateBasesRecursive(info->GetFullPath(), regexes, depth + 1,
                                 maxDepth, result);
    } else
    {
      const QDateTime date = QDateTime::fromTime_t(info->GetModificationTime());
      fileList.push_back({info->GetName(), info->GetFullPath(), date});
    }
  }

  // Build all bases for current directory
  for (const auto& regexStr : regexes)
  {
    const std::regex regex{regexStr.toStdString()};
    std::map<std::string, assistant::Base> bases;

    for (const auto& file : fileList)
    {
      std::smatch match;
      const std::string fileName = file.Name.toStdString();
      if (std::regex_search(fileName, match, regex) && match[0].matched &&
          match.size() > 1)
      {
        const std::string identifier =
            path.toStdString() +
            std::string(match[0].first, match[match.size() - 1].first);
        auto findId = bases.find(identifier);
        if (findId == bases.end())
        {
          bases.insert({identifier, assistant::Base{identifier.c_str(),
                                                    "",
                                                    "",
                                                    "",
                                                    identifier.c_str(),
                                                    file.Date,
                                                    {file.FullPath}}});
        } else
        {
          findId->second.FileList.push_back(file.FullPath);
          if (findId->second.Date < file.Date)
          {
            findId->second.Date = file.Date;
          }
        }
      }
    }

    for (auto& base : bases)
    {
      // cppcheck-suppress useStlAlgorithm
      result.push_back(std::move(base.second));
    }
  }
}

// ----------------------------------------------------------------------------
QStringList
pqThemysAssistantBaseManager::GetBaseFileList(const QString& identifier) const
{
  const int separatorIdx = identifier.lastIndexOf('/');
  const QString directory = identifier.left(separatorIdx);
  const QString name = identifier.mid(separatorIdx + 1);
  auto informations = this->GetInformations(directory, false);

  QStringList res;

  const std::regex regex(name.toStdString() + "(.*)");
  auto iter = vtk::TakeSmartPointer(informations->GetContents()->NewIterator());
  for (iter->InitTraversal(); iter->IsDoneWithTraversal() == 0;
       iter->GoToNextItem())
  {
    vtkPVFileInformation* info =
        vtkPVFileInformation::SafeDownCast(iter->GetCurrentObject());
    const bool isFile =
        info != nullptr &&
        (info->GetType() == vtkPVFileInformation::SINGLE_FILE ||
         info->GetType() == vtkPVFileInformation::SINGLE_FILE_LINK);
    if (isFile && !info->GetHidden() &&
        std::regex_match(info->GetName(), regex))
    {
      res.push_back(info->GetFullPath());
    }
  }

  return res;
}

// ----------------------------------------------------------------------------
QString
pqThemysAssistantBaseManager::GetBaseFirstFile(const QString& identifier) const
{
  const int separatorIdx = identifier.lastIndexOf('/');
  const QString directory = identifier.left(separatorIdx);
  const QString name = identifier.mid(separatorIdx + 1);
  auto informations = this->GetInformations(directory, false);

  const std::regex regex(name.toStdString() + "(.*)");
  auto iter = vtk::TakeSmartPointer(informations->GetContents()->NewIterator());
  for (iter->InitTraversal(); iter->IsDoneWithTraversal() == 0;
       iter->GoToNextItem())
  {
    vtkPVFileInformation* info =
        vtkPVFileInformation::SafeDownCast(iter->GetCurrentObject());
    const bool isFile =
        info != nullptr &&
        (info->GetType() == vtkPVFileInformation::SINGLE_FILE ||
         info->GetType() == vtkPVFileInformation::SINGLE_FILE_LINK);
    if (isFile && !info->GetHidden() &&
        std::regex_match(info->GetName(), regex))
    {
      return info->GetFullPath();
    }
  }

  return "";
}

// ----------------------------------------------------------------------------
QString pqThemysAssistantBaseManager::GetBasePath(const QString& storageName,
                                                  const QString& user,
                                                  const QString& curCase) const
{
  const QString storagePath = this->StorageModel->getStoragePath(storageName);
  return ::SmartJoinDirectoryPath({storagePath.toStdString(),
                                   user.toStdString(), curCase.toStdString()})
      .c_str();
}

// ----------------------------------------------------------------------------
vtkSmartPointer<vtkPVFileInformation>
pqThemysAssistantBaseManager::GetInformations(const QString& path,
                                              bool details) const
{
  auto information = vtkSmartPointer<vtkPVFileInformation>::New();

  if (this->FileInformationHelperProxy != nullptr)
  {
    // send data to server
    vtkSMProxy* helper = this->FileInformationHelperProxy;
    pqSMAdaptor::setElementProperty(helper->GetProperty("DirectoryListing"), 1);
    pqSMAdaptor::setElementProperty(helper->GetProperty("Path"), path.toUtf8());
    pqSMAdaptor::setElementProperty(helper->GetProperty("SpecialDirectories"),
                                    0);
    pqSMAdaptor::setElementProperty(helper->GetProperty("GroupFileSequences"),
                                    0);
    pqSMAdaptor::setElementProperty(
        helper->GetProperty("ReadDetailedFileInformation"), details);
    helper->UpdateVTKObjects();

    // get data from server
    information->Initialize();
    helper->GatherInformation(information);
  } else
  {
    vtkPVFileInformationHelper* helper = this->FileInformationHelper;
    helper->SetDirectoryListing(1);
    helper->SetPath(path.toUtf8().data());
    helper->SetSpecialDirectories(0);
    helper->SetGroupFileSequences(false /*0*/);
    helper->SetReadDetailedFileInformation(details);
    information->CopyFromObject(helper);
  }

  return information;
}

// ----------------------------------------------------------------------------
pqThemysAssistantBaseManager::IndexList
pqThemysAssistantBaseManager::GetDirectoryList(const QString& path) const
{
  auto information = this->GetInformations(path, false);

  // Extract directories
  IndexList result;
  vtkSmartPointer<vtkCollectionIterator> iter;
  iter.TakeReference(information->GetContents()->NewIterator());
  for (iter->InitTraversal(); iter->IsDoneWithTraversal() == 0;
       iter->GoToNextItem())
  {
    vtkPVFileInformation* info =
        vtkPVFileInformation::SafeDownCast(iter->GetCurrentObject());
    if (info == nullptr)
    {
      continue;
    }
    if (vtkPVFileInformation::IsDirectory(info->GetType()) &&
        !info->GetHidden())
    {
      result.push_back({info->GetName(), info->GetFullPath(), QDateTime()});
    }
  }

  return result;
}

// ----------------------------------------------------------------------------
nlohmann::json
pqThemysAssistantBaseManager::SerializeBase(const assistant::Base& base)
{
  nlohmann::json baseJson;
  baseJson["name"] = base.Name.toUtf8().data();
  baseJson["storage"] = base.StorageName.toUtf8().data();
  baseJson["user"] = base.User.toUtf8().data();
  baseJson["case"] = base.Case.toUtf8().data();
  baseJson["identifier"] = base.Identifier.toUtf8().data();
  baseJson["date"] = base.Date.toString(Qt::TextDate).toUtf8().data();

  return baseJson;
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantBaseManager::ParseBase(const nlohmann::json& base,
                                             assistant::Base& result)
{
  if (!base.contains("storage") || !base.contains("user") ||
      !base.contains("case") || !base.contains("identifier") ||
      !base.contains("date") || !base.contains("name"))
  {
    return false;
  }

  const QString storageName = base["storage"].get<std::string>().c_str();

  result = assistant::Base{
      base["name"].get<std::string>().c_str(),
      storageName,
      base["user"].get<std::string>().c_str(),
      base["case"].get<std::string>().c_str(),
      base["identifier"].get<std::string>().c_str(),
      QDateTime::fromString(base["date"].get<std::string>().c_str(),
                            Qt::TextDate),
      // We do not try to find the files for the parsed base because the
      // identifier is enough. This gain a lot of time not searching bases
      // through the file system.
      {}};

  return true;
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseManager::AddToRecentBase(assistant::Base base)
{
  if (this->RecentBases.size() > HISTORY_SIZE)
  {
    this->RecentBases.erase(this->RecentBases.begin() + HISTORY_SIZE,
                            this->RecentBases.end());
  }

  for (int i = 0; i < this->RecentBases.size(); ++i)
  {
    if (this->RecentBases[i] == base)
    {
      base = this->RecentBases[i];
      this->RecentBases.removeAt(i);
      break;
    }
  }

  if (this->RecentBases.size() == HISTORY_SIZE)
  {
    this->RecentBases.pop_front();
  }

  base.Date = QDateTime::currentDateTime();
  this->RecentBases.push_back(base);
}
