#include "pqThemysAssistantBaseFilteringWidget.h"

#include <QAbstractItemModel> // for QAbstractItemModel
#include <QByteArray>         // for QByteArray
#include <QComboBox>
#include <QLineEdit>
#include <QList>       // for QList, QVector:...
#include <QModelIndex> // for QModelIndex
#include <QObject>     // for QObject, qobjec...
#include <QPushButton>
#include <QStringList> // for QStringList
#include <QVector>     // for QVector
#include <QtCore>      // for ItemDataRole
#include <algorithm>   // for copy
#include <iterator>    // for back_insert_ite...

#include <QtCore/qglobal.h> // for qCritical
#include <pqApplicationCore.h>
#include <pqServer.h>
#include <pqServerResource.h>
#include <pqSettings.h>
#include <vtkLogger.h>

#include "pqThemysAssistantBaseManager.h" // for Base, pqThemysA...
#include "pqThemysAssistantBaseModel.h"
#include "pqThemysAssistantStorageModel.h"
#include "ui_pqThemysAssistantBaseFilteringWidget.h"

static constexpr const char* SESSION_SETTING_STR = "AssistantBaseFiltering";

//------------------------------------------------------------------------------
struct pqThemysAssistantBaseFilteringWidget::pqInternals {
  Ui::pqThemysAssistantBaseFilteringWidget Ui;
  pqThemysAssistantBaseManager* BaseManager;
};

// ============================================================================
pqThemysAssistantBaseFilteringWidget::pqThemysAssistantBaseFilteringWidget(
    QWidget* parent)
    : Superclass(parent),
      Internals(new pqThemysAssistantBaseFilteringWidget::pqInternals())
{
  auto& intern = *this->Internals;
  intern.Ui.setupUi(this);

  intern.Ui.UserComboBox->setInsertPolicy(QComboBox::NoInsert);
  intern.Ui.CaseComboBox->setInsertPolicy(QComboBox::NoInsert);

  QObject::connect(intern.Ui.StorageComboBox,
                   QOverload<int>::of(&QComboBox::currentIndexChanged), this,
                   &pqThemysAssistantBaseFilteringWidget::onStorageSelected);
  QObject::connect(intern.Ui.UserComboBox,
                   QOverload<int>::of(&QComboBox::currentIndexChanged), this,
                   &pqThemysAssistantBaseFilteringWidget::onUserSelected);
  QObject::connect(
      intern.Ui.UserComboBox->lineEdit(), &QLineEdit::editingFinished, this,
      &pqThemysAssistantBaseFilteringWidget::onUserEditTextChanged);

  QObject::connect(
      intern.Ui.SearchBasesButton, &QPushButton::clicked, this,
      &pqThemysAssistantBaseFilteringWidget::onSearchFilteredBases);
  QObject::connect(intern.Ui.ShowRecentsButton, &QPushButton::clicked, this,
                   &pqThemysAssistantBaseFilteringWidget::onSearchRecentBases);
}

// ----------------------------------------------------------------------------
pqThemysAssistantBaseFilteringWidget::~pqThemysAssistantBaseFilteringWidget()
{
  pqApplicationCore* core = pqApplicationCore::instance();
  pqSettings* settings = core->settings();
  pqServer* server = core->getActiveServer();
  const pqServerResource resource =
      server != nullptr ? server->getResource() : pqServerResource("builtin:");
  const QString uri = resource.serverName().isEmpty() ? resource.toURI()
                                                      : resource.serverName();
  const QString key = QString(SESSION_SETTING_STR).append("/%1").arg(uri);

  auto& intern = *this->Internals;
  if (!intern.Ui.StorageComboBox->currentText().isEmpty() &&
      !intern.Ui.UserComboBox->currentText().isEmpty() &&
      !intern.Ui.CaseComboBox->currentText().isEmpty())
  {
    QStringList value;
    value.push_back(intern.Ui.StorageComboBox->currentText());
    value.push_back(intern.Ui.UserComboBox->currentText());
    value.push_back(intern.Ui.CaseComboBox->currentText());

    settings->setValue(key, value);
  }
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::Initialize(
    pqThemysAssistantBaseManager* manager,
    pqThemysAssistantStorageModel* storageModel,
    pqThemysAssistantBaseModel* baseModel)
{
  this->Internals->BaseManager = manager;

  this->Internals->Ui.StorageComboBox->setModel(storageModel);

  // Connect signals that will update the File view
  QObject::connect(
      this, &pqThemysAssistantBaseFilteringWidget::showFilteredBases,
      [=](const QVector<assistant::Base>& bases) {
        baseModel->setBases(bases, BaseDisplayState::FILTERED_BASE);
      });

  QObject::connect(this, &pqThemysAssistantBaseFilteringWidget::showRecentBases,
                   [=](const QVector<assistant::Base>& bases) {
                     baseModel->setBases(bases, BaseDisplayState::RECENT_BASE);
                   });

  this->loadPreviousSession();
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::loadPreviousSession()
{
  pqApplicationCore* core = pqApplicationCore::instance();
  pqSettings* settings = core->settings();
  pqServer* server = core->getActiveServer();
  const pqServerResource resource =
      server != nullptr ? server->getResource() : pqServerResource("builtin:");
  const QString uri = resource.serverName().isEmpty() ? resource.toURI()
                                                      : resource.serverName();
  const QString key = QString(SESSION_SETTING_STR).append("/%1").arg(uri);

  QStringList previousSessionFiltering;
  if (settings->contains(key))
  {
    previousSessionFiltering = settings->value(key).toStringList();
  }

  if (previousSessionFiltering.size() == 3)
  {
    auto& intern = *this->Internals;
    const int storageIdx =
        intern.Ui.StorageComboBox->findText(previousSessionFiltering[0]);
    if (storageIdx >= 0)
    {
      intern.Ui.StorageComboBox->setCurrentIndex(storageIdx);
      const int userIdx =
          intern.Ui.UserComboBox->findText(previousSessionFiltering[1]);
      if (userIdx >= 0)
      {
        intern.Ui.UserComboBox->setCurrentIndex(userIdx);
        const int caseIdx =
            intern.Ui.CaseComboBox->findText(previousSessionFiltering[2]);
        if (caseIdx >= 0)
        {
          intern.Ui.CaseComboBox->setCurrentIndex(caseIdx);
        }
      }
    }
  }
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::onStorageSelected(int index)
{
  auto& intern = *this->Internals;

  intern.Ui.UserComboBox->clear();
  if (index < 0)
  {
    return;
  }

  const QModelIndex storageIndex =
      intern.Ui.StorageComboBox->model()->index(index, 0);
  if (!storageIndex.isValid())
  {
    qCritical("Invalid Index");
    return;
  }

  const QString storagePath =
      storageIndex.model()->data(storageIndex, Qt::UserRole).toString();

  auto dirList = intern.BaseManager->GetDirectoryList(storagePath);
  for (const auto& info : dirList)
  {
    intern.Ui.UserComboBox->addItem(info.Name, info.FullPath);
    vtkLogF(7, "User added, Name: %s, Path: %s.", info.Name.toUtf8().data(),
            info.FullPath.toUtf8().data());
  }

  auto* storageModel = qobject_cast<pqThemysAssistantStorageModel*>(
      this->Internals->Ui.StorageComboBox->model());
  intern.Ui.UserComboBox->model()->sort(0);
  intern.Ui.UserComboBox->setCurrentIndex(
      intern.Ui.UserComboBox->findText(storageModel->commandArgument()));
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::onUserSelected(int index)
{
  auto& intern = *this->Internals;
  intern.Ui.CaseComboBox->clear();
  if (index < 0)
  {
    return;
  }

  const QString userPath =
      intern.Ui.UserComboBox->itemData(index, Qt::UserRole).toString();
  auto dirList = intern.BaseManager->GetDirectoryList(userPath);
  for (const auto& info : dirList)
  {
    intern.Ui.CaseComboBox->addItem(info.Name, info.FullPath);
    vtkLogF(7, "Case added, Name: %s, Path: %s.", info.Name.toUtf8().data(),
            info.FullPath.toUtf8().data());
  }

  intern.Ui.CaseComboBox->model()->sort(0);
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::onSearchFilteredBases()
{
  Q_EMIT this->preFilterBases();

  QVector<assistant::Base> bases;

  auto& intern = *this->Internals;
  const QString storageName = intern.Ui.StorageComboBox->currentText();
  const QString user = intern.Ui.UserComboBox->currentText();
  const QString curCase = intern.Ui.CaseComboBox->currentText();

  if (!storageName.isEmpty() && !user.isEmpty() && !curCase.isEmpty())
  {
    intern.Ui.CaseComboBox->itemData(intern.Ui.CaseComboBox->currentIndex(),
                                     Qt::UserRole);
    const QVector<QString>& baseRegex = intern.BaseManager->basesRegex();
    const auto& constructed =
        intern.BaseManager->CreateBases(storageName, user, curCase, baseRegex);
    std::copy(constructed.cbegin(), constructed.cend(),
              std::back_inserter(bases));
  }

  Q_EMIT this->showFilteredBases(bases);
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::onSearchRecentBases()
{
  const QList<assistant::Base>& bases =
      this->Internals->BaseManager->recentBases();
  Q_EMIT this->showRecentBases(QVector<assistant::Base>::fromList(bases));
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseFilteringWidget::onUserEditTextChanged()
{
  auto& intern = *this->Internals;
  const QString user = intern.Ui.UserComboBox->currentText();
  if (intern.Ui.UserComboBox->findText(user) == -1)
  {
    auto* storageModel = qobject_cast<pqThemysAssistantStorageModel*>(
        this->Internals->Ui.StorageComboBox->model());
    storageModel->setCommandArgument(user);
  }
}
