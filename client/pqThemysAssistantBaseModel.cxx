#include "pqThemysAssistantBaseModel.h"

#include <QSortFilterProxyModel>

// ----------------------------------------------------------------------------
pqThemysAssistantBaseModel::pqThemysAssistantBaseModel(QObject* parent)
    : Superclass(parent), ProxyModel(new QSortFilterProxyModel(this))
{
  ProxyModel->setSourceModel(this);
}

// ----------------------------------------------------------------------------
int pqThemysAssistantBaseModel::columnCount(const QModelIndex& /*parent*/) const
{
  // Name, Storage, User, Case, Date
  constexpr int COL_COUNT{5};
  return COL_COUNT;
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantBaseModel::setData(const QModelIndex& idx,
                                         const QVariant& value, int role)
{
  Q_UNUSED(role);

  const int row = idx.row();
  if (row >= this->Bases.size() || idx.column() != 0)
  {
    return false;
  }

  const QString newName = value.toString();
  this->Bases[row].Name = newName;
  if (this->CurrentState == BaseDisplayState::STUDY)
  {
    Q_EMIT this->studyBaseNameChanged(row, newName);
  }

  return true;
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantBaseModel::data(const QModelIndex& idx,
                                          int role) const
{
  if (idx.row() >= this->Bases.size())
  {
    return {};
  }

  const assistant::Base& base = this->Bases[idx.row()];
  switch (role)
  {
  case Qt::DisplayRole:
  case Qt::EditRole: {
    switch (idx.column())
    {
    case 0:
      return base.Name;
    case 1:
      return base.StorageName;
    case 2:
      return base.User;
    case 3:
      return base.Case;
    case 4:
      return base.Date;
    default:
      return {};
    }
  }
  case Qt::UserRole:
    return base.FileList;
  case Qt::ToolTipRole:
    return base.Identifier;
  default:
    return {};
  }
}

// ----------------------------------------------------------------------------
QModelIndex
pqThemysAssistantBaseModel::index(int row, int column,
                                  const QModelIndex& /*parent*/) const
{
  return this->createIndex(row, column);
}

// ----------------------------------------------------------------------------
QModelIndex
pqThemysAssistantBaseModel::parent(const QModelIndex& /*child*/) const
{
  return {};
}

// ----------------------------------------------------------------------------
int pqThemysAssistantBaseModel::rowCount(const QModelIndex& /*parent*/) const
{
  return this->Bases.size();
}

// ----------------------------------------------------------------------------
bool pqThemysAssistantBaseModel::hasChildren(const QModelIndex& idx) const
{
  // Bases will never display childrens.
  // XXX It could be possible to display all files that belongs to a base
  // though.
  return !idx.isValid();
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantBaseModel::headerData(int section,
                                                Qt::Orientation /*orientation*/,
                                                int role) const
{
  if (role == Qt::DisplayRole)
  {
    switch (section)
    {
    case 0:
      return tr("Base name");
    case 1:
      return tr("Storage");
    case 2:
      return tr("User");
    case 3:
      return tr("Case");
    case 4:
      return tr("Date");
    default:
      return {};
    }
  }

  return {};
}

// ----------------------------------------------------------------------------
Qt::ItemFlags pqThemysAssistantBaseModel::flags(const QModelIndex& idx) const
{
  Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  if (this->CurrentState != BaseDisplayState::FILTERED_BASE &&
      idx.column() == 0)
  {
    flags |= Qt::ItemIsEditable;
  }
  return flags;
}

// ----------------------------------------------------------------------------
void pqThemysAssistantBaseModel::setBases(const QVector<assistant::Base>& bases,
                                          BaseDisplayState state)
{
  this->beginResetModel();
  this->Bases = bases;
  this->CurrentState = state;
  this->endResetModel();
}
