#ifndef pqThemysViewFrameActionsImplementation_h
#define pqThemysViewFrameActionsImplementation_h

#include <QList>   // for QList
#include <QString> // for QString
#include <QtCore>  // for Q_DISABLE_COPY

#include "pqStandardViewFrameActionsImplementation.h"

class pqThemysViewFrameActionsImplementation
    : public pqStandardViewFrameActionsImplementation
{
  Q_OBJECT
  typedef pqStandardViewFrameActionsImplementation Superclass;

public:
  explicit pqThemysViewFrameActionsImplementation(QObject* parent = 0);
  ~pqThemysViewFrameActionsImplementation() override = default;

protected:
  /**
   * Returns available view types in Themys.
   */
  QList<ViewType> availableViewTypes() override;

private:
  Q_DISABLE_COPY(pqThemysViewFrameActionsImplementation);
};

#endif
