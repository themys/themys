#include "pqThemysInterfaceModeToolbar.h"

#include <QByteArray> // for QByteArray
#include <QComboBox>
#include <QObject> // for QObject
#include <cstring> // for strcmp
#include <map>     // for operator!=, opera...
#include <string>  // for basic_string, string

#include <pqApplicationCore.h>
#include <pqCoreUtilities.h>
#include <pqServer.h> // for pqServer
#include <pqServerManagerModel.h>
#include <pqSettings.h>
#include <pqUndoStack.h>
#include <vtkCommand.h>
#include <vtkNew.h>    // for vtkNew
#include <vtkObject.h> // IWYU pragma: keep
#include <vtkSMProperty.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMProxy.h> // for vtkSMProxy
#include <vtkSMProxyManager.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSettings.h>
#include <vtkSMSettingsProxy.h>
#include <vtk_nlohmannjson.h>

#include "pqThemysSettingsInfo.h"
#include VTK_NLOHMANN_JSON(json.hpp)
// IWYU pragma: no_include <nlohmann/detail/iterators/iter_impl.hpp>

class QWidget;
//-----------------------------------------------------------------------------
class pqThemysInterfaceModeToolbar::vtkInternalObserver : public vtkCommand
{
public:
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  static vtkInternalObserver* New() { return new vtkInternalObserver(); }

  void Execute(vtkObject* /*caller*/, unsigned long /*eventId*/,
               void* callData) override
  {
    auto* info =
        static_cast<vtkSMProxyManager::RegisteredProxyInformation*>(callData);

    if (info != nullptr && info->GroupName != nullptr &&
        info->ProxyName != nullptr &&
        strcmp(info->GroupName, "settings") == 0 &&
        strcmp(info->ProxyName, SETTINGS_GROUP_NAME.toUtf8().data()) == 0)
    {
      self->setupConnections(vtkSMSettingsProxy::SafeDownCast(info->Proxy));
    }
  }

  pqThemysInterfaceModeToolbar* self = nullptr;
};

//-----------------------------------------------------------------------------
pqThemysInterfaceModeToolbar::pqThemysInterfaceModeToolbar(
    const nlohmann::json& interfaces, QWidget* parentW)
    : Superclass(parentW)
{
  this->setWindowTitle("InterfaceMode");
  this->constructor(interfaces);
}

//-----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::constructor(const nlohmann::json& interfaces)
{
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  this->InterfaceOptions = new QComboBox(this);
  for (const nlohmann::json& mode : interfaces)
  {
    this->InterfaceOptions->addItem(
        QString(mode["name"].template get<std::string>().c_str()), 0);
  }

  this->addWidget(InterfaceOptions);

  // Every time the server is changed we add an observer to keep track of when
  // the setting proxy is added. When it is, the observer will call
  // `setupConnections` so everything is in order for the GUI. We use the
  // `preServerAdded` because code path is
  //  - fire pqServerManagerModel::preServerAdded event
  //  - create all necessary proxies, including the pre-loaded plugins proxies
  //  - fire pqServerManagerModel::serverAdded and pqActiveObject::serverChanged
  //  events
  QObject::connect(
      pqApplicationCore::instance()->getServerManagerModel(),
      &pqServerManagerModel::preServerAdded, [this](pqServer* server) {
        if (server != nullptr)
        {
          vtkNew<pqThemysInterfaceModeToolbar::vtkInternalObserver> observer;
          observer->self = this;

          // reset internal proxy so we do no try to call function on the proxy
          // of the former server
          this->SettingsProxy = nullptr;
          server->proxyManager()->AddObserver(vtkCommand::RegisterEvent,
                                              observer);
        }
      });
}

//----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::setupConnections(
    vtkSMSettingsProxy* settingsProxy)
{
  if (settingsProxy != nullptr && settingsProxy != this->SettingsProxy)
  {
    this->SettingsProxy = settingsProxy;

    // Connect on the property
    pqCoreUtilities::connect(
        this->SettingsProxy->GetProperty(INTERFACE_MODE_NAME.toUtf8().data()),
        vtkCommand::ModifiedEvent, this, SLOT(updateInterfaceMode()));

    // Connect the combobox
    this->connect(this->InterfaceOptions, SIGNAL(currentIndexChanged(int)),
                  SLOT(onTriggered(int)));

    // Init the combobox state
    this->updateInterfaceMode();
  }
}

//-----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::onTriggered(int status)
{
  SCOPED_UNDO_EXCLUDE();
  if (this->SettingsProxy == nullptr)
  {
    return;
  }

  // Set the property
  vtkSMPropertyHelper(this->SettingsProxy, INTERFACE_MODE_NAME.toUtf8().data())
      .Set(status);
  this->SettingsProxy->UpdateVTKObjects();

  // Manually save it to user settings files
  pqSettings* qSettings = pqApplicationCore::instance()->settings();
  qSettings->saveInQSettings(
      QString(SETTINGS_GROUP_NAME + "." + INTERFACE_MODE_NAME).toUtf8().data(),
      this->SettingsProxy->GetProperty(INTERFACE_MODE_NAME.toUtf8().data()));
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  settings->SetSetting(
      QString(".settings." + SETTINGS_GROUP_NAME + "." + INTERFACE_MODE_NAME)
          .toUtf8()
          .data(),
      status);
}

//-----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::updateInterfaceMode()
{
  if (this->SettingsProxy == nullptr)
  {
    return;
  }

  // Recover the property value
  const int mode = vtkSMPropertyHelper(this->SettingsProxy,
                                       INTERFACE_MODE_NAME.toUtf8().data())
                       .GetAsInt();

  // Update the combobox status
  this->InterfaceOptions->setCurrentIndex(mode);

  // Inform the application the setting has changed
  emit changeInterfaceMode(mode);
}
