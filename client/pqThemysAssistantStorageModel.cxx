#include "pqThemysAssistantStorageModel.h"

#include <QByteArray> // for QByteArray
#include <QList>      // for QList
#include <Qt>         // for ItemDataRole
#include <string>     // for basic_string, allocator

#include <vtkObject.h>       // for vtkObject
#include <vtkSetGet.h>       // for vtkGenericWarningMacro
#include <vtkSmartPointer.h> // for vtkSmartPointer

#include "pqServer.h"
#include "pqServerResource.h"
#include "vtkLogger.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSettings.h"
#include "vtkSystemIncludes.h" // for vtkOStreamWrapper

class QObject;

// ----------------------------------------------------------------------------
pqThemysAssistantStorageModel::pqThemysAssistantStorageModel(pqServer* server,
                                                             QObject* parent)
    : Superclass(parent), Server(server)
{
  const pqServerResource resource =
      server != nullptr ? server->getResource() : pqServerResource("builtin:");
  this->ServerURI = resource.serverName().isEmpty() ? resource.toURI()
                                                    : resource.serverName();
  this->LoadCommandsFromSettings();
}

// ----------------------------------------------------------------------------
QModelIndex
pqThemysAssistantStorageModel::index(int row, int /*column*/,
                                     const QModelIndex& /*parent*/) const
{
  return this->createIndex(row, 0);
}

// ----------------------------------------------------------------------------
QModelIndex
pqThemysAssistantStorageModel::parent(const QModelIndex& /*child*/) const
{
  return {};
}

// ----------------------------------------------------------------------------
int pqThemysAssistantStorageModel::rowCount(const QModelIndex& /*parent*/) const
{
  return static_cast<int>(this->StorageCommands.count());
}

// ----------------------------------------------------------------------------
int pqThemysAssistantStorageModel::columnCount(
    const QModelIndex& /*parent*/) const
{
  return 1;
}

// ----------------------------------------------------------------------------
QVariant pqThemysAssistantStorageModel::data(const QModelIndex& idx,
                                             int role) const
{
  if (!idx.isValid() || idx.column() != 0 ||
      idx.row() >= this->rowCount(QModelIndex()))
  {
    return {};
  }

  auto keys = this->StorageCommands.keys();
  const QString label = keys[idx.row()];
  switch (role)
  {
  case Qt::DisplayRole:
  case Qt::EditRole:
    return label;
  case Qt::UserRole:
  case Qt::ToolTipRole:
    vtkLogF(5, "Recovering path for storage:`%s`", label.toUtf8().data());
    return this->evaluatePathFromCommand(this->StorageCommands[label],
                                         this->CommandArgument);
  default:
    break;
  }

  return {};
}

// ----------------------------------------------------------------------------
QString pqThemysAssistantStorageModel::evaluatePathFromCommand(
    const QString& command, const QString& argument) const
{
  if (this->Server == nullptr)
  {
    return "";
  }
  auto* pxm = this->Server->proxyManager();
  if (pxm == nullptr)
  {
    return "";
  }

  vtkSmartPointer<vtkSMProxy> commandProxy;
  commandProxy.TakeReference(pxm->NewProxy("misc", "ExecutableRunner"));
  vtkSMPropertyHelper(commandProxy, "Command")
      .Set(command.arg(argument).toStdString().c_str());
  constexpr float TIMEOUT_VALUE{0.3};
  vtkSMPropertyHelper(commandProxy, "Timeout").Set(TIMEOUT_VALUE);
  commandProxy->UpdateVTKObjects();
  commandProxy->InvokeCommand("Execute");
  commandProxy->UpdatePropertyInformation();
  const int ret = vtkSMPropertyHelper(commandProxy, "ReturnValue").GetAsInt();

  QString path{""};
  if (ret > 0)
  {
    vtkGenericWarningMacro(
        "Error when executing the storage command. Error string is :\n"
        << vtkSMPropertyHelper(commandProxy, "StdErr").GetAsString());
  } else if (ret < 0)
  {
    vtkGenericWarningMacro("Error when executing the storage command: `"
                           << command.toStdString() << "`. Error code is :\n"
                           << ret);
  } else
  {
    path = QString(vtkSMPropertyHelper(commandProxy, "StdOut").GetAsString())
               .trimmed();
    vtkLogF(6, "Command evaluated:`%s`", command.toUtf8().data());
    vtkLogF(6, "Command Argument:`%s`", argument.toUtf8().data());
    vtkLogF(6, "Command Resulting path:`%s`", path.toUtf8().data());
  }
  return path;
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStorageModel::LoadCommandsFromSettings()
{
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  if (settings == nullptr)
  {
    return;
  }

  vtkLogF(7, "Fetching storages from settings");
  // Fetch storages from settings
  constexpr const char* storageSettingKey = ".settings.ThemysSettings.Storages";
  if (settings->HasSetting(storageSettingKey))
  {
    vtkLogF(7, "Storages found in settings");
    const unsigned int nStorage =
        settings->GetSettingNumberOfElements(storageSettingKey) / 3;
    for (unsigned int i = 0; i < nStorage; ++i)
    {
      vtkLogF(7, "Fetching settings for storage n°\%d", i);
      const QString serverURI =
          settings->GetSettingAsString(storageSettingKey, i * 3, "").c_str();
      vtkLogF(7, "Server URI is : %s", serverURI.toStdString().data());
      vtkLogF(7, "Current server URI is : %s",
              this->ServerURI.toStdString().data());
      if (serverURI == this->ServerURI)
      {
        const QString label =
            settings->GetSettingAsString(storageSettingKey, i * 3 + 1, "")
                .c_str();
        const QString command =
            settings->GetSettingAsString(storageSettingKey, i * 3 + 2, "")
                .c_str();
        vtkLogF(7, "Label for storage n°\%d is %s", i,
                label.toStdString().data());
        vtkLogF(7, "Command for storage n°\%d is %s", i,
                command.toStdString().data());
        if (!serverURI.isEmpty() && !label.isEmpty() && !command.isEmpty())
        {
          this->StorageCommands[label] = command;
        }
      }
    }
  }
}

// ----------------------------------------------------------------------------
QString
pqThemysAssistantStorageModel::getStoragePath(const QString& storageName) const
{
  vtkLogF(5, "Recovering path for storage:`%s`", storageName.toUtf8().data());
  return this->evaluatePathFromCommand(this->StorageCommands[storageName],
                                       this->CommandArgument);
}

// ----------------------------------------------------------------------------
void pqThemysAssistantStorageModel::setCommandArgument(const QString& argument)
{
  this->beginResetModel();
  this->CommandArgument = argument;
  this->endResetModel();
}

// ----------------------------------------------------------------------------
QString pqThemysAssistantStorageModel::commandArgument()
{
  return this->CommandArgument;
}
