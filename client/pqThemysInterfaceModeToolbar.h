#include <QString> // for QString
#include <QToolBar>
#include <QtCore> // for Q_DISABLE_COPY, Q_OBJECT, Q_SIGNALS

#include <vtk_nlohmannjson.h>
#include VTK_NLOHMANN_JSON(json.hpp) // IWYU pragma: keep
// IWYU pragma: no_include <nlohmann/json_fwd.hpp>

class vtkSMSettingsProxy;
class QComboBox;
class QWidget;

/**
 * Toolbar associated to a ComboBox widget, from which the user can select the
 * application interface mode. A signal is emitted when the mode is changed by
 * the user. This component also reacts to a change made in the settings. The
 * different interface modes are described by the file `interface_modes.json`,
 * copied to the build and install folders by CMake. Refer to the Themys
 * documentation to get an extensive overview of the configuration file.
 */
class pqThemysInterfaceModeToolbar : public QToolBar
{
  Q_OBJECT;
  using Superclass = QToolBar;

public:
  explicit pqThemysInterfaceModeToolbar(const nlohmann::json& interfaces,
                                        QWidget* parent = nullptr);
  ~pqThemysInterfaceModeToolbar() override = default;

Q_SIGNALS:
  /**
   * Signal emitted when the interface mode is changed.
   */
  void changeInterfaceMode(int mode);

protected Q_SLOTS:
  /**
   * Slot called when the interface mode setting is changed,
   * to update the combo state and emit the updateInterfaceMode signal.
   */
  void updateInterfaceMode();

  /**
   * Slot called when the interface mode combobox selected index changes.
   * Sets the property and the corresponding setting.
   */
  void onTriggered(int status);

private:
  Q_DISABLE_COPY(pqThemysInterfaceModeToolbar);

  /**
   * Create the combobox item from the interfaces json list,
   * and create an observer to monitor external interface mode change.
   */
  void constructor(const nlohmann::json& interfaces);

  /**
   * Call to set up the signal/slot connection of the toolbar
   * The given proxy should be the proxy of the themysSettings plugin
   */
  void setupConnections(vtkSMSettingsProxy* settingsProxy);
  class vtkInternalObserver;

  QComboBox* InterfaceOptions = nullptr;
  vtkSMSettingsProxy* SettingsProxy = nullptr;
};
