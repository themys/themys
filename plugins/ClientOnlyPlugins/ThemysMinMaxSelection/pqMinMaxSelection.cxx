#include "pqMinMaxSelection.h"

#include <QAction>           // for QAction
#include <QIcon>             // for QIcon
#include <QNonConstOverload> // for QNonConstOverload, QOverload
#include <QObject>           // for qobject_cast, QObject
#include <string>            // for operator+, allocator, char_tr...

#include <QtCore/qglobal.h> // for qCritical, qWarning

// PQ headers
#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqDataRepresentation.h>
#include <pqPipelineSource.h> // for pqPipelineSource
#include <pqSelectionManager.h>
#include <pqServer.h>

// VTK SM headers
#include <vtkSMColorMapEditorHelper.h>
#include <vtkSMCoreUtilities.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMRepresentationProxy.h> // for vtkSMRepresentationProxy
#include <vtkSMSelectionHelper.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSourceProxy.h>

// VTK headers
#include <vtkPVArrayInformation.h>
#include <vtkSMProxy.h> // for vtkSMProxy
#include <vtkSmartPointer.h>
#include <vtkSmartPointerBase.h> // for operator==, vtkSmartPointerBase

class pqOutputPort;
//-----------------------------------------------------------------------------
void pqMinMaxSelection::findData()
{
  // Retrieve the active server and associated proxy manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  vtkSMSessionProxyManager* pxm = server->proxyManager();

  // Retrieve the active source proxy
  pqPipelineSource* activeSource = pqActiveObjects::instance().activeSource();
  if (activeSource == nullptr)
  {
    qCritical("Failed to retrieve the active source.");
    return;
  }
  vtkSMSourceProxy* activeSourceProxy =
      vtkSMSourceProxy::SafeDownCast(activeSource->getProxy());
  if (activeSourceProxy == nullptr)
  {
    qCritical("Failed to retrieve the active source proxy.");
    return;
  }

  // Retrieve the active representation proxy
  pqDataRepresentation* activeRepr =
      pqActiveObjects::instance().activeRepresentation();
  if (activeRepr == nullptr)
  {
    qCritical("Failed to retrieve the active representation.");
    return;
  }
  vtkSMRepresentationProxy* activeReprProxy =
      vtkSMRepresentationProxy::SafeDownCast(activeRepr->getProxy());
  if (activeReprProxy == nullptr)
  {
    qCritical("Failed to retrieve the active representation proxy.");
    return;
  }

  // Retrieve the selection manager
  auto* selectionManager = qobject_cast<pqSelectionManager*>(
      pqApplicationCore::instance()->manager("SELECTION_MANAGER"));
  if (selectionManager == nullptr)
  {
    qCritical("Failed to retrieve the selection manager.");
    return;
  }

  // Retrieve the active array info and the LUT
  vtkPVArrayInformation* activeArrayInfo =
      vtkSMColorMapEditorHelper::GetArrayInformationForColorArray(
          activeReprProxy);
  if (activeArrayInfo == nullptr)
  {
    selectionManager->select(nullptr);
    return;
  }
  vtkSMProxy* lutProxy = activeRepr->getLookupTableProxy();

  // Retrieve the mode - "Magnitude" or "Component" - and the associated
  // component in the second case.
  const std::string mode =
      vtkSMPropertyHelper(lutProxy, "VectorMode").GetAsString();
  const std::string component = std::to_string(
      vtkSMPropertyHelper(lutProxy, "VectorComponent").GetAsInt());

  // Create a new selection source proxy
  vtkSmartPointer<vtkSMSourceProxy> selectionSource;
  selectionSource.TakeReference(vtkSMSourceProxy::SafeDownCast(
      pxm->NewProxy("sources", "SelectionQuerySource")));
  if (selectionSource == nullptr)
  {
    qCritical("Failed to create the selection source proxy.");
    return;
  }

  // Set the element type associated to the color array
  const int elementType = vtkSMPropertyHelper(activeReprProxy, "ColorArrayName")
                              .GetInputArrayAssociation();
  if (elementType > 1) // Support only POINT and CELL
  {
    qWarning("Only point and cell arrays are supported - skipping selection.");
    selectionManager->select(nullptr);
    return;
  }
  vtkSMPropertyHelper(selectionSource, "ElementType").Set(elementType);

  // Set the query
  const std::string arrayName =
      vtkSMCoreUtilities::SanitizeName(activeArrayInfo->GetName());
  const int nbOfComponents = activeArrayInfo->GetNumberOfComponents();
  std::string query;

  if (nbOfComponents == 1)
  {
    // (array == max(array)) | (array == min(array))
    query = "(" + arrayName + " == max(" + arrayName +
            "))"
            "|(" +
            arrayName + " == min(" + arrayName + "))";
  } else
  {
    if (mode == "Magnitude")
    {
      // (mag(array) == max(mag(array))) | (mag(array) == min(mag(array)))
      query = "(mag(" + arrayName + ") == max(mag(" + arrayName +
              ")))"
              "|(mag(" +
              arrayName + ") == min(mag(" + arrayName + ")))";
    } else
    {
      // (array[:,component] == max(array[:,component])) | (array[:,component]
      // == min(array[:,component]))
      query = "(" + arrayName + "[:," + component + "] == max(" + arrayName +
              "[:," + component +
              "]))"
              "|(" +
              arrayName + "[:," + component + "] == min(" + arrayName + "[:," +
              component + "]))";
    }
  }

  vtkSMPropertyHelper(selectionSource, "QueryString").Set(query.c_str());
  selectionSource->UpdateVTKObjects();

  // Set the selection to the active source
  vtkSmartPointer<vtkSMSourceProxy> appendSelections;
  appendSelections.TakeReference(vtkSMSourceProxy::SafeDownCast(
      vtkSMSelectionHelper::NewAppendSelectionsFromSelectionSource(
          selectionSource)));

  activeSourceProxy->SetSelectionInput(0, appendSelections,
                                       0); // Assume portIndex = 0

  pqOutputPort* outputPort =
      activeSource->getOutputPort(0); // Assume outputport = 0
  selectionManager->select(outputPort);
}

//-----------------------------------------------------------------------------
pqMinMaxSelection::pqMinMaxSelection(QObject* parent) : QActionGroup(parent)
{
  auto* action = new QAction(QIcon(":/Themys/pqMinMax.svg"),
                             tr("Select Min and Max values"), this);
  action->setEnabled(false);
  this->addAction(action);

  QObject::connect(action, &QAction::triggered, this,
                   &pqMinMaxSelection::findData);

  QObject::connect(
      &pqActiveObjects::instance(),
      qOverload<pqDataRepresentation*>(&pqActiveObjects::representationChanged),
      [=](const pqDataRepresentation* repr) {
        action->setEnabled(repr != nullptr);
      });
}
