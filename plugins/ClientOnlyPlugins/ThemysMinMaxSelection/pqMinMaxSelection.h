#ifndef pqMinMaxSelection_h
#define pqMinMaxSelection_h

#include <QActionGroup>
#include <QString> // for QString
#include <QtCore>  // for Q_DISABLE_COPY, Q_OBJECT, Q_SLOTS

/**
 * @class pqMinMaxSelection
 * @brief Select points or cells with the min or max value of the active array.
 *
 * This class allows to quickly select points or cells with the min or max value
 * of the active array by adding a new button in the Themys toolbar.
 * The selection is done on the current active source, taking acount of
 * the active array, it's association (points or cells) and its current selected
 * component (or magnitude) in case of multidimentional data.
 */

class pqMinMaxSelection : public QActionGroup
{
  Q_OBJECT

public:
  explicit pqMinMaxSelection(QObject* p);
  ~pqMinMaxSelection() override = default;

public Q_SLOTS:
  static void findData();

private:
  Q_DISABLE_COPY(pqMinMaxSelection);
};

#endif // pqMinMaxSelection_h
